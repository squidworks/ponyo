/*
isrcpp.h

reconfigurable interface to hardware interrupts 

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#ifndef ISRCCP_H_
#define ISRCPP_H_

#include <arduino.h>
#include "hunks/hunks.h"

// it's a singlet, like our trunk (debug) tool

class ISRCPP {
private:
  static ISRCPP* instance;
public:
  ISRCPP();
  static ISRCPP* getInstance(void);

  /*
  // since moved this to static implementation,
  boolean register_for_tc0(uint16_t hunkIndice, uint8_t handlerIndice);
  uint16_t tc0_hunkIndice = 0;
  uint8_t tc0_handlerIndice = HUNK_HANDLER_NOHANDLER; // ... a/b/c switch, so bad
  boolean register_for_tc5(uint16_t hunkIndice, uint8_t handlerIndice);
  uint16_t tc5_hunkIndice = 0;
  uint8_t tc5_handlerIndice = HUNK_HANDLER_NOHANDLER;
  */

  boolean register_for_ser0_rx(uint16_t hunkIndice, uint8_t handlerIndice);
  uint16_t ser0_rx_hunkIndice = 0;
  uint8_t ser0_rx_handlerIndice = HUNK_HANDLER_NOHANDLER;
  boolean register_for_ser0_tx(uint16_t hunkIndice, uint8_t handlerIndice);
  uint16_t ser0_tx_hunkIndice = 0;
  uint8_t ser0_tx_handlerIndice = HUNK_HANDLER_NOHANDLER;

  boolean register_for_ser1_rx(uint16_t hunkIndice, uint8_t handlerIndice);
  uint16_t ser1_rx_hunkIndice = 0;
  uint8_t ser1_rx_handlerIndice = HUNK_HANDLER_NOHANDLER;
  boolean register_for_ser1_tx(uint16_t hunkIndice, uint8_t handlerIndice);
  uint16_t ser1_tx_hunkIndice = 0;
  uint8_t ser1_tx_handlerIndice = HUNK_HANDLER_NOHANDLER;

  boolean register_for_ser2_rx(uint16_t hunkIndice, uint8_t handlerIndice);
  uint16_t ser2_rx_hunkIndice = 0;
  uint8_t ser2_rx_handlerIndice = HUNK_HANDLER_NOHANDLER;
  boolean register_for_ser2_rx_on_one(uint16_t hunkIndice, uint8_t handlerIndice);
  uint16_t ser2_rx_one_hunkIndice = 0;
  uint8_t ser2_rx_one_handlerIndice = HUNK_HANDLER_NOHANDLER;
  boolean register_for_ser2_tx(uint16_t hunkIndice, uint8_t handlerIndice);
  uint16_t ser2_tx_hunkIndice = 0;
  uint8_t ser2_tx_handlerIndice = HUNK_HANDLER_NOHANDLER;

  boolean register_for_ser3_rx(uint16_t hunkIndice, uint8_t handlerIndice);
  uint16_t ser3_rx_hunkIndice = 0;
  uint8_t ser3_rx_handlerIndice = HUNK_HANDLER_NOHANDLER;
  boolean register_for_ser3_tx(uint16_t hunkIndice, uint8_t handlerIndice);
  uint16_t ser3_tx_hunkIndice = 0;
  uint8_t ser3_tx_handlerIndice = HUNK_HANDLER_NOHANDLER;

  boolean register_for_ser4_rx(uint16_t hunkIndice, uint8_t handlerIndice);
  uint16_t ser4_rx_hunkIndice = 0;
  uint8_t ser4_rx_handlerIndice = HUNK_HANDLER_NOHANDLER;
  boolean register_for_ser4_tx(uint16_t hunkIndice, uint8_t handlerIndice);
  uint16_t ser4_tx_hunkIndice = 0;
  uint8_t ser4_tx_handlerIndice = HUNK_HANDLER_NOHANDLER;

  boolean register_for_ser5_rx(uint16_t hunkIndice, uint8_t handlerIndice);
  uint16_t ser5_rx_hunkIndice = 0;
  uint8_t ser5_rx_handlerIndice = HUNK_HANDLER_NOHANDLER;
  boolean register_for_ser5_tx(uint16_t hunkIndice, uint8_t handlerIndice);
  uint16_t ser5_tx_hunkIndice = 0;
  uint8_t ser5_tx_handlerIndice = HUNK_HANDLER_NOHANDLER;
};

extern ISRCPP* isrcpp;

#endif

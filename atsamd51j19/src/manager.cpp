/*
manager.cpp

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#include "manager.h"
#include "trunk.h"
#include "build_bootstrap.h"

Manager* Manager::instance = 0;

Manager* Manager::getInstance(void){
  if(instance == 0){
    instance = new Manager();
  }
  return instance;
}

Manager::Manager(){
  type_ = "manager";
  // ?
  numInputs = 1;
  inputs[0] = inMsgs;
  numOutputs = 1;
  outputs[0] = outMsgs;
}

Manager* ponyo = Manager::getInstance();

void Manager::init(void){
  // fill in hunks w/ nullptrs: we use
  for(uint16_t i = 0; i < HUNK_LIST_MAX_LENGTH; i ++){
    hunks[i] = nullptr;
  }
  // init self,
  indice_ = 0;
  hunks[indice_] = this;
  hunkCount_ ++;
  name_ = "ponyo_one";
  // since we init ourselves,
  setChildrenIndices();
  // say hello, ok
  debug("ponyo inits");
};

// calls bootstrap, from circuit.h, is our startup config essentially...
void Manager::callBootstrap(void){
  bootstrap(this);
}

void Manager::loop(void){
  /* -------------------------------- ---------------------------------- */
  /* ---------------------- recv manager messages ---------------------- */
  /* -------------------------------- ---------------------------------- */
  if(inMsgs->io()){
    // TODO: NEXT: get here,
    // finish link (connects, hello-indice, acks ok)
    // any js changes? spec? writing?
    // then handle some mgr msgs, starting w/ brief
    debug("lp: msg 2 get");
    inBufferLen_ = inMsgs->get(inBuffer_);
    dbs();
    dbn("MGR RECIEVETH the message: len: ");
    dbf(inBufferLen_);
    // tracking,
    uint16_t mp = 0;
    uint8_t msgid = 0;
    // output,
    uint16_t op = 0;
    boolean hasMsgId = false;
    // find and pull msgid,
    if(inBuffer_[0] == MK_MSGID){
      // take the id,
      msgid = inBuffer_[1];
      // inc by two: [0] was the id flag, [1] was the id,
      mp = 2;
      // and we're going to reply to this thing,
      outBuffer_[op ++] = MK_MSGID;
      outBuffer_[op ++] = msgid;
      hasMsgId = true;
    }
    //
    dbs();
    dbn("MGR RECIEVETH key byte: ");
    dbf(inBuffer_[mp]);
    // continue,
    switch(inBuffer_[mp]){
      case MK_HELLO:
        // straightforward
        outBuffer_[op ++] = MK_HELLO;
        mmd_->put(outBuffer_, op);
        debug("MGR reply hello");
        break;
      case MK_QUERY:
        mp ++;
        if(op + 1 < inBufferLen_){
          // read indice,
          uint16_t hi = readUint16(inBuffer_, &mp);
          if(hi >= hunkCount_){
            error("query for hunk is out of range");
            dbs();
            dbn("indice requested: ");
            dbf(hi);
          } else {
            dbs();
            dbn("SERIALIZING HNK AT INDICE: ");
            dbf(hi);
            // work thru errors: cannot match to this # args, (anneal)
            // then: strings, writing out? how find void* equiv? where bytes?
            serializeAndSendHunk(hunks[hi], false, op);
          }
        } else {
          // toplevel query,
          debug("MGR reply for brief");
          outBuffer_[op++] = MK_BRIEF;
          directWrite(outBuffer_, &op, interpreterName_.c_str(), TS_STRING_KEY, interpreterName_.length(), true);
          directWrite(outBuffer_, &op, interpreterVersion_.c_str(), TS_STRING_KEY, interpreterVersion_.length(), true);
          // so uint16 comes out 3 0 not 0 3 ... agree on a way, spec?
          // wikipedia: big eadianess is 'network byte order'
          // looks like arms are little eadian ?
          // for tricks,
          directWrite(outBuffer_, &op, (void*)&hunkCount_, TS_UINT16_KEY, 2, false);
          // once you know about those lengths, this should do it...
          mmd_->put(outBuffer_, op);
        }// end query
        break;
      case MK_REQLISTAVAIL:
        debug("MGR to write list...");
        if(!serializeAndSendAvailable(op)){
          // opportunity for error catching, but
        }
        break;
      case MK_REQADDHUNK:
        // increment past the key,
        mp ++;
        // string hunktype, (opt) chars for state deserialization, (opt) (string) name
        if(addHunkFromSerial(mp)){
          // it's the last one,
          serializeAndSendHunk(hunks[hunkCount_ - 1], false, op);
        } else {
          replyError("couldn't add this hunk from serial...", mp);
          error("failure on adding hunk from serial...");
        }
        break;
      case MK_REQRMHUNK:{
          // uint16 hunk indice,
          mp ++;
          uint16_t rmHunkIndice = readUint16(inBuffer_, &mp);
          if(rmHunk(rmHunkIndice)){
            // rm ok, something like ...
            outBuffer_[op++] = MK_HUNKREMOVED;
            writeUint16(outBuffer_, &op, rmHunkIndice);
            mmd_->put(outBuffer_, op);
          } else {
            replyError("botched rm hunk, undefined behaviour probably awaits", mp);
            // this is a low level hook str8 thru the usb port, if it's available
            error("botched rm hunk, undefined behaviour probably awaits");
          }
        }
        break;
      case MK_REQSTATECHANGE: {
          // hunk indice, state indice, new value
          mp ++;
          uint16_t stChHnkInd = readUint16(inBuffer_, &mp);
          uint8_t stChInd = readUint8(inBuffer_, &mp);
          // ok, and recall that tryChange doesn't increment mp! (but could?)
          // but this is in any case the EOM, so http://shrugman.com
          if(hunks[stChHnkInd]->tryStateChange(stChInd, inBuffer_, mp)){
            // worked, ok,
          } else {
            // a todo, for resilience and completeness, is to error reply with msg ids and
            // handle them well upstream
            // this replies w/ msg id associated,
            // replyError("cannot update this state", mp);
            // this is a low level hook str8 thru the usb port, if it's available
            error("could not update this state, hunk: " + String(stChHnkInd) + " state: " + String(stChInd));
          }
          // but can only reply if the state exists:
          if(stChInd >= hunks[stChHnkInd]->numStates){
            // trouble,
          } else {
            // ... write back state either way:
            op = 0;
            if(hasMsgId){
              outBuffer_[op ++] = MK_MSGID;
              outBuffer_[op ++] = msgid;
            }
            writeStateUpdateMessage(stChHnkInd, stChInd, op);
            hasStateUpdatesToMake_ = false;
            hunks[stChHnkInd]->states[stChInd]->hasPendingStateChangeReport = false;
          }
        }
        break;
      case MK_REQADDLINK: { // http://www.cplusplus.com/forum/beginner/85367/ ?
          // past the key,
          mp ++;
          // coupla bangers
          uint16_t linkMsgStart = mp;
          uint16_t outHunkIndice = readUint16(inBuffer_, &mp);
          uint8_t outputIndice = readUint8(inBuffer_, &mp);
          uint16_t inHunkIndice = readUint16(inBuffer_, &mp);
          uint8_t inputIndice = readUint8(inBuffer_, &mp);
          //adebug("hookup: " + String(outHunkIndice) + " " + String(outputIndice) + " " + String(inHunkIndice) + " " +  String(inputIndice));
          if(addLink(outHunkIndice, outputIndice, inHunkIndice, inputIndice)){
            outBuffer_[op++] = MK_LINKALIVE;
            memcpy(&outBuffer_[op], &inBuffer_[linkMsgStart], 10);
            op += 10;
            mmd_->put(outBuffer_, op);
          } else {
            replyError("cannot add this link", mp);
            error("failure adding link...");
          }
        }
        break;
      case MK_REQRMLINK: {
          // past the key,
          mp ++;
          uint16_t linkMsgStart = mp;
          uint16_t outHunkIndice = readUint16(inBuffer_, &mp);
          uint8_t outputIndice = readUint8(inBuffer_, &mp);
          uint16_t inHunkIndice = readUint16(inBuffer_, &mp);
          uint8_t inputIndice = readUint8(inBuffer_, &mp);
          if(rmLink(outHunkIndice, outputIndice, inHunkIndice, inputIndice)){
            outBuffer_[op++] = MK_LINKREMOVED;
            memcpy(&outBuffer_[op], &inBuffer_[linkMsgStart], 10);
            op += 10;
            mmd_->put(outBuffer_, op);
          } else {
            replyError("cannot remove this link", mp);
            error("failure removing link...")
          }
        }
        break;
      default:
        error("MGR MSG w bad key");
        dbs();
        dbn("key is: ");
        dbf(inBuffer_[mp]);
        break;
    }
  } // end if-inMsgs io

  if(!(outMsgs->io()) && mmd_->getNumWaiting() > 0){
    debug("MGR releases from MMD");
    mmd_->releaseTo(outMsgs);
  }

  // remember to *not* run own loop, (h=1, != 0)
  for (uint16_t h = 1; h < hunkCount_; h ++) {
    hunks[h]->loop();
  }

  // finally (kind of upsetting to have to check this every loop, but at least
  // it's minimized to this one if-at-all)
  if(hasStateUpdatesToMake_){
    // look through our hunks to figure out which one it was,
    for(uint16_t h = 0; h < hunkCount_; h ++){
      Hunk* hnk = hunks[h];
      for(uint8_t s = 0; s < hnk->numStates; s ++){
        State* st = hnk->states[s];
        if(st->hasPendingStateChangeReport){
          // well, that's an explicit flag!
          // writing from zero, so it looks like we are always assuming
          // outbuffer_ is empty, that might be dangerous but I
          // can't think of any cases where it will not have
          // been put to mmd already...
          debug("one state to upd8");
          writeStateUpdateMessage(h, s, 0);
          st->hasPendingStateChangeReport = false;
        }
      }
    }
    // our global,
    hasStateUpdatesToMake_ = false;
  }

  // debug delay,
  #if LOOP_DELAY
    CLKLIGHT_TOGGLE;
    delay(LOOP_DELAY);
  #else
    loop_tick_tracker_ ++;
    if(loop_tick_tracker_ > LOOP_TICKS_PER_BLINK){
      CLKLIGHT_TOGGLE;
      loop_tick_tracker_ = 0;
    }
  #endif
}

/* -------------------------------- ---------------------------------- */
/* ------------------------ manager workhorses ----------------------- */
/* -------------------------------- ---------------------------------- */

boolean Manager::serializeAndSendAvailable(uint16_t bptr){
  outBuffer_[bptr ++] = MK_LISTOFAVAIL;
  // that hlc is from build_bootstrap.h ...
  for(uint16_t i = 0; i < hlc; i ++){
    directWrite(outBuffer_, &bptr, hunklist[i].c_str(), TS_STRING_KEY, hunklist[i].length(), true);
  }
  // send like,
  mmd_->put(outBuffer_, bptr);
  return true;
}

/* -------------------------------- ---------------------------------- */
/* -------------------------- ADDING HUNKS --------------------------- */
/* -------------------------------- ---------------------------------- */

Hunk* Manager::addHunkFromSerial(uint16_t bptr){
  // going to do by type, (opt) name, (opt) states
  String type = readString(inBuffer_, &bptr);
  debug("READS STRING: " + type);
  String name;
  if(inBufferLen_ > bptr){
    bptr ++;
    name = readString(inBuffer_, &bptr);
  } else {
    name = type + "_" + hunkCount_;
  }
  // guarantee we have a name and type now, can do:
  Hunk* hnk = allocateHunkByType(type);
  if(!hnk){
    return nullptr;
  }
  hnk->name_ = name;
  hnk->indice_ = hunkCount_;
  hunks[hunkCount_] = hnk;
  hunkCount_ ++;
  // is there more?
  if(inBufferLen_ > bptr){
    debug("INTO STATE SETTINGS ON ADD, FOR: " + String(hnk->numStates));
    // these should be in order, right ?
    // and there should always be all of them, but we can err-catch ...
    for(uint8_t s = 0; s < hnk->numStates; s ++){
      if(inBuffer_[bptr] == HK_STATE){
        // doin' it for
        State* st = hnk->states[s];
        // there's a name, and a type here, and then a typed value
        // we would rather check by type key, but we can use these to inc. the bufptr along
        bptr ++;
        String stName = readString(inBuffer_, &bptr);
        String stType = readString(inBuffer_, &bptr);
        // now we have the $$
        if(inBuffer_[bptr] == st->chunk->key){
          debug("KEY IS GOOD");
          bptr ++; // past the key,
          uint16_t stDataLen = 0;
          if(st->chunk->isVarLen){
            stDataLen = (inBuffer_[bptr + 1] << 8) | inBuffer_[bptr];
            bptr += 2;
            // to hack, we put ah zero at the end of the data as well, and set (this for arduino string class reading out)
            /*
            if(st->chunk->key == TS_STRING_KEY){
              bughuntr remnants,
              adebug("writing string st change bytes, len is " + String(stDataLen));
              adebug("hunk name is " + hnk->name_);
              adebug("state name is " + stName);
              for(int16_t i = - 3; i < stDataLen; i ++){
                // on inspection, these look sound... so it's the exit
                adbs();
                adbn(String(i) + ": ");
                adbf(String(inBuffer_[bptr + i]));
              }
            }
            */
            st->chunk->len = stDataLen;
            memcpy(st->chunk->data, &inBuffer_[bptr], stDataLen);
            // well! there's almost no way it's not this
            void* eop = st->chunk->data;
            *((char*)eop + stDataLen) = 0;
          } else {
            stDataLen = st->chunk->len;
            memcpy(st->chunk->data, &inBuffer_[bptr], stDataLen);
          }
          // and bptr past the data, for ntext round
          bptr += stDataLen;
        } else {
          debug("KEY IS BAD: position " + String(s) + " wanted " + String(st->chunk->key) + " but had " + String(inBuffer_[bptr]));
        }
      } else {
        debug("1st key != HK_STATE");
      }
    }
  }
  // hunks need access to their indicies (sometimes)
  // before they startup.. so,
  hnk->setChildrenIndices();
  // startup,
  hnk->init();
  // hello,
  debug("added hunk " + hnk->name_ + " at indice " + hnk->indice_);
  return hnk;
}

// function overloading ... same fn name different args
Hunk* Manager::addHunk(String type, String name, unsigned char* states, uint16_t statesLen){
  Hunk* hnk = allocateHunkByType(type);
  // ok, get one:
  // ok, should have type:
  if (hnk) {
    // name it,
    hnk->name_ = name;
    hnk->indice_ = hunkCount_;
    // if we have state objects,
    if(statesLen > 0){
      /*
      not yet, but would
      write state objects into place,
      */
      debug("ERR: not writing states during load yet");
    }
    // get it in there
    hunks[hunkCount_] = hnk;
    hunkCount_ ++;
    // start it up,
    hnk->init();
    // only do this apres add via msg, then have handle on start point, know we are repl.
    // has indices now, so
    hnk->setChildrenIndices();
    // serializeAndSendHunk(hnk, false, 0);
    debug("added hunk " + hnk->name_ + " at indice " + hnk->indice_);
    return hnk;
  } else {
    error("couldn't add this hunk: " + name);
    return nullptr;
  }
}

Hunk* Manager::addHunk(String type, String name){
  return addHunk(type, name, 0, 0);
}

Hunk* Manager::addHunk(String type){
  return addHunk(type, type + "_" + hunkCount_);
}

// or,
Hunk* Manager::includeHunk(Hunk* hnk){
  hnk->indice_ = hunkCount_;
  hunks[hunkCount_] = hnk;
  hunkCount_ ++;
  hnk->setChildrenIndices();
  hnk->init();
  return hnk;
}

// rm,
boolean Manager::rmHunk(uint16_t indice){
  // could totally just remove them from the array,
  // but this will leave them floating around in memory
  // ... not a gargantuan effort, but for false completion, could do
  // ok f it, let's party:
  if(indice < hunkCount_ && indice != 0){
    // first, we need to rm this hunk from all other connections ...
    // dead hunk walking:
    Hunk* hnk2rm = hunks[indice];
    // for each of its inputs, we rm them from others' list:
    for(uint8_t oi = 0; oi < hnk2rm->numInputs; oi ++){
      Input* ip2rm = hnk2rm->inputs[oi];
      // scroll thru all others,
      for(uint16_t h = 0; h < hunkCount_; h ++){
        Hunk* hnk = hunks[h];
        for(uint8_t o = 0; o < hnk->numOutputs; o ++){
          Output* op = hnk->outputs[o];
          for(uint8_t i = 0; i < op->numConnections; i ++){
            Input* ip = op->connections[i].ip;
            // ok, if this ip (from *some* hunk) is the same as the ip
            if(ip == ip2rm){
              if(op->remove(ip2rm)){
                debug("for hunk deletion, rmd one connection");
              } else {
                error("during a hunk deletion, an error appears removing a connection");
                return false;
              }
            }
          } // end loop over that outputs' connections
        } // end loop over that hunks' outputs
      } // complete per-input loop over all hunks,
    } // complete deadhunk-inputs loop

    // done rm'ing links, we can wash over the global handle:
    // *this doesn't actually rm any memory!! an incomplete implemetation, for sure!*
    hunks[indice] = nullptr;
    for(uint16_t i = indice; i < hunkCount_; i ++){
      hunks[i] = hunks[i + 1];
    }
    hunkCount_ --;
    return true;
  } else {
    return false;
  }
}

boolean Manager::serializeAndSendHunk(Hunk* hnk, boolean replace, uint16_t bptr){
  // writing into outBuffer_,
  if(replace){
    outBuffer_[bptr++] = MK_HUNKREPLACE;
  } else {
    outBuffer_[bptr++] = MK_HUNKALIVE;
  }
  // continue: the indice of the hunk we will pass,
  outBuffer_[bptr++] = HK_INDICE;
  writeUint16(outBuffer_, &bptr, hnk->indice_);
  // its type:
  outBuffer_[bptr++] = HK_TYPE;
  // so, evidently, a stringWrite method would be cool
  directWrite(outBuffer_, &bptr, hnk->type_.c_str(), TS_STRING_KEY, hnk->type_.length(), true);
  // its name:
  outBuffer_[bptr++] = HK_NAME;
  directWrite(outBuffer_, &bptr, hnk->name_.c_str(), TS_STRING_KEY, hnk->name_.length(), true);
  // inputs,
  for(uint8_t ip = 0; ip < hnk->numInputs; ip ++){
    dbs();
    dbn("to write input: " + hnk->inputs[ip]->name);
    dbf();
    outBuffer_[bptr++] = HK_INPUT;
    directWrite(outBuffer_, &bptr, hnk->inputs[ip]->name.c_str(), TS_STRING_KEY, hnk->inputs[ip]->name.length(), true);
    directWrite(outBuffer_, &bptr, hnk->inputs[ip]->typeName.c_str(), TS_STRING_KEY, hnk->inputs[ip]->typeName.length(), true);
  }
  // outputs
  for(uint8_t op = 0; op < hnk->numOutputs; op ++){
    Output* outp = hnk->outputs[op];
    dbs();
    dbn("to write output: " + outp->name);
    dbf();
    outBuffer_[bptr++] = HK_OUTPUT;
    directWrite(outBuffer_, &bptr, outp->name.c_str(), TS_STRING_KEY, outp->name.length(), true);
    directWrite(outBuffer_, &bptr, outp->chunk->type.c_str(), TS_STRING_KEY, outp->chunk->type.length(), true);
    // do connections,
    // ok,
    /*
    - re-writing js for
    - hunk indice in connection having potentially 16b,
    - js, not writing in HK_CONNECTIONS for each hunk output,
    */
    if(outp->numConnections > 0){
      outBuffer_[bptr++] = HK_CONNECTIONS;
      for(uint8_t cn = 0; cn < outp->numConnections; cn ++){
        // write conn like,
        outBuffer_[bptr++] = HK_CONNECTION;
        dbs();
        dbn("parent indice for " + hnk->name_ + "'s connection is apparently: ");
        dbf(outp->connections[cn].ip->parentIndice);
        // we dont' key these, just nums,
        outBuffer_[bptr++] = (outp->connections[cn].ip->parentIndice) & 255;  // indice of connection's parent,
        outBuffer_[bptr++] = ((outp->connections[cn].ip->parentIndice) >> 8) & 255;       // lb of above,
        outBuffer_[bptr++] = outp->connections[cn].ip->indice; // indice (8) of connection's own indice
      }
    }
  }
  // states
  // ok, we know that the view doesn't actually get to the state loop.. view 5 not printing...
  // so ?
  for(uint8_t st = 0; st < hnk->numStates; st ++){
    dbs();
    dbn("STATE: " + hnk->states[st]->name);
    dbf();
    if(false){ //replace && hnk->states[st]->chunk->key == TS_STRING_KEY){
      debug("skipping...");
    } else {
      outBuffer_[bptr++] = HK_STATE;
      directWrite(outBuffer_, &bptr, hnk->states[st]->name.c_str(), TS_STRING_KEY, hnk->states[st]->name.length(), true);
      directWrite(outBuffer_, &bptr, hnk->states[st]->chunk->type.c_str(), TS_STRING_KEY, hnk->states[st]->chunk->type.length(), true);
      typeWrite(outBuffer_, &bptr, hnk->states[st]->chunk);
    }
  }
  // send like,
  mmd_->put(outBuffer_, bptr);
  // ok ok ok
  return true;
}


boolean Manager::evaluateHunk(uint16_t ind){
  // serial upd8, then walk all links for ...
  Hunk* hnk2eval = hunks[ind];
  // do this!
  hnk2eval->setChildrenIndices();
  serializeAndSendHunk(hnk2eval, true, 0);
  // walk outputs for connections to our inputs...
  for(uint8_t oi = 0; oi < hnk2eval->numInputs; oi ++){
    Input* ip2inspect = hnk2eval->inputs[oi];
    // scroll thru all others,
    for(uint16_t h = 0; h < hunkCount_; h ++){
      Hunk* hnk = hunks[h];
      for(uint8_t o = 0; o < hnk->numOutputs; o ++){
        Output* op = hnk->outputs[o];
        for(uint8_t i = 0; i < op->numConnections; i ++){
          Input* ip = op->connections[i].ip;
          // ok, if this ip (from *some* hunk) is the same as the ip
          // we need to send it upstream,
          if(ip == ip2inspect){
            sendLinkAlive(h, o, hnk2eval->indice_, oi, 0);
          }
        } // end loop over that outputs' connections
      } // end loop over that hunks' outputs
    } // complete per-input loop over all hunks,
  } // complete evalhunk-inputs loop

  // walk our outputs for connections
  for(uint8_t o = 0; o < hnk2eval->numOutputs; o ++){
    Output* op = hnk2eval->outputs[o];
    for(uint8_t i = 0; i < op->numConnections; i ++){
      Input* ip = op->connections[i].ip;
      sendLinkAlive(hnk2eval->indice_, o, ip->parentIndice, ip->indice, 0);
    }
  }

  // ?
  return true;
}

// add a link,
boolean Manager::addLink(uint16_t outHunkIndice, uint8_t outputIndice, uint16_t inHunkIndice, uint8_t inputIndice){
  // this should be easier now, try
  debug("going to add link between " + hunks[outHunkIndice]->name_ + " and " + hunks[inHunkIndice]->name_);
  if((hunks[outHunkIndice] != nullptr) && (hunks[inHunkIndice] != nullptr)){
    Output* op = hunks[outHunkIndice]->outputs[outputIndice];
    Input* ip = hunks[inHunkIndice]->inputs[inputIndice];
    if((op != nullptr) && (ip != nullptr)){
      debug("input " + ip->name + " and output " + op->name);
      if(op->attach(ip)){
        return true;
      } else {
        return false;
      }
    } else {
      error("bad input or output for link hookup");
      return false;
    }
  } else {
    error("bad output hunk or input hunk for link hookup");
    return false;
  }
}

// remove a link,
boolean Manager::rmLink(uint16_t outHunkIndice, uint8_t outputIndice, uint16_t inHunkIndice, uint8_t inputIndice){
  debug("going to rm link between " + hunks[outHunkIndice]->name_ + " and " + hunks[inHunkIndice]->name_);
  Output* op = hunks[outHunkIndice]->outputs[outputIndice];
  Input* ip = hunks[inHunkIndice]->inputs[inputIndice];
  if(op && ip){
    debug("input " + ip->name + " and output " + op->name);
    if(op->remove(ip)){
      return true;
    } else {
      error("link rm failure: output's remove function returned false");
      adebug("was for links between input " + ip->name + " and output " + op->name);
      return false;
    }
  } else {
    error("on link remove, requested output or input does not exist");
    return false;
  }
}

void Manager::writeStateUpdateMessage(uint16_t stChHunkIndice, uint8_t stIndice, uint16_t bptr){
  outBuffer_[bptr ++] = MK_HUNKSTATECHANGE;
  writeUint16(outBuffer_, &bptr, stChHunkIndice);
  writeUint8(outBuffer_, &bptr, stIndice);
  typeWrite(outBuffer_, &bptr, hunks[stChHunkIndice]->states[stIndice]->chunk);
  mmd_->put(outBuffer_, bptr);
}

void Manager::sendLinkAlive(uint16_t outHunkIndice, uint8_t outputIndice, uint16_t inHunkIndice, uint8_t inputIndice, uint16_t bptr){
  outBuffer_[bptr++] = MK_LINKALIVE;
  writeUint16(outBuffer_, &bptr, outHunkIndice);
  writeUint8(outBuffer_, &bptr, outputIndice);
  writeUint16(outBuffer_, &bptr, inHunkIndice);
  writeUint8(outBuffer_, &bptr, inputIndice);
  mmd_->put(outBuffer_, bptr);
}

void Manager::registerStateChangeForNextLoop(void){
  hasStateUpdatesToMake_ = true;
}

void Manager::replyError(String msg, uint16_t bptr){
  outBuffer_[bptr ++] = MK_ERR;
  writeString(outBuffer_, &bptr, msg);
}

/*
states/states.h

hunk state objects 

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#ifndef STATES_H_
#define STATES_H_

#include <arduino.h>
#include "../transports/typeset.h"

class State {
public:
  String name;
  // another chunk, and a swap (to buffer change requests)
  Chunk* chunk;
  Chunk* swap;
  // nice 2 know about our
  uint16_t parentIndice;
  uint8_t indice;

  /*
  // ok, this is the manager's handle into requesting a state change
  // it's a direct ptr from the msg buffer,
  boolean tryChange(unsigned char* addr, uint16_t* dptr);
  */

  // individuals write interfaces to this memory:
  // Type value(void);
  // boolean set(Type arg);
  // set, calls this, which sets a flag for the manager to bubble through
  boolean hasPendingStateChangeReport = false;
  void registerChangeWithManager(void);
};

#endif

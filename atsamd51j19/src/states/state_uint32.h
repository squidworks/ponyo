/*
states/state_uint32.h

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#ifndef STATE_UINT32_H_
#define STATE_UINT32_H_

#include <arduino.h>
#include "states.h"

class State_uint32 : public State {
public:
  State_uint32(String nm, uint32_t initial);
  // a getter and setter, and one for the swap
  uint32_t value(void);
  uint32_t swapValue(void);
  boolean set(uint32_t val);
};

#endif

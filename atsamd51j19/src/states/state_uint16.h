/*
states/state_uint16.h

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#ifndef STATE_UINT16_H_
#define STATE_UINT16_H_

#include <arduino.h>
#include "states.h"

class State_uint16 : public State {
public:
  State_uint16(String nm, uint16_t initial);
  // the business - these are interfaces to our memory hole
  uint16_t value(void);
  uint16_t swapValue(void);
  boolean set(uint16_t val);
};

#endif

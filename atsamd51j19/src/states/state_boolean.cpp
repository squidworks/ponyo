/*
states/state_boolean.cpp

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#include "state_boolean.h"

// not a c string, an arduino string (I know...)

State_boolean::State_boolean(String nm, boolean initial){
  name = nm;
  chunk = new Chunk_boolean();
  swap = new Chunk_boolean();
  memcpy(chunk->data, (void*)&initial, chunk->len);
}

boolean State_boolean::value(void){
  return *(boolean*)(chunk->data);
}

boolean State_boolean::swapValue(void){
  return *(boolean*)(swap->data);
}

boolean State_boolean::set(boolean val){
  if(val){
    *(boolean*)(chunk->data) = 1;
  } else {
    *(boolean*)(chunk->data) = 0;
  }
  registerChangeWithManager();
  return true;
}

/*
states/state_uint32.cpp

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#include "state_uint32.h"

State_uint32::State_uint32(String nm, uint32_t initial){
  name = nm;
  chunk = new Chunk_uint32();
  swap = new Chunk_uint32();
  memcpy(chunk->data, (void*)&initial, chunk->len);
}

uint32_t State_uint32::value(void){
  return *(uint32_t*)(chunk->data);
}

uint32_t State_uint32::swapValue(void){
  return *(uint32_t*)(swap->data);
}

boolean State_uint32::set(uint32_t val){
  memcpy(chunk->data, (void*)&val, chunk->len);
  registerChangeWithManager();
  return true;
}

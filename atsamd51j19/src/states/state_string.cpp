/*
states/state_string.cpp

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#include "state_string.h"

// not a c string, an arduino string (I know...)

State_string::State_string(String nm, String initial, size_t dimension){
  name = nm;
  chunk = new Chunk_string(dimension);
  swap = new Chunk_string(dimension);
  chunk->len = initial.length();
  initial.getBytes((unsigned char*)(chunk->data), chunk->size);
}

String State_string::value(void){
  // bc the string is null terminated, this works. string constr.
  // watches for that 0 -> so don't loose it
  return String((const char*)chunk->data);
}

String State_string::swapValue(void){
  return String((const char*)swap->data);
}

boolean State_string::set(String str){
  str.getBytes((unsigned char*)(chunk->data), str.length() + 1);
  chunk->len = str.length();
  registerChangeWithManager();
  return true;
}

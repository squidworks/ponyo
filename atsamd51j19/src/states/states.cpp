/*
states/states.cpp

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#include "states.h"
#include "../manager.h"

void State::registerChangeWithManager(void){
  // manager has one to set,
  ponyo->registerStateChangeForNextLoop();
  // and ours is it,
  hasPendingStateChangeReport = true;
}

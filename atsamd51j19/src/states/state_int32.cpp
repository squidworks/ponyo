/*
states/state_int32.cpp

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#include "state_int32.h"

// not a c string, an arduino string (I know...)

State_int32::State_int32(String nm, int32_t initial){
  // it's a:
  name = nm;
  // comprising of a wrapper on one,
  chunk = new Chunk_int32();
  swap = new Chunk_int32();
  // set startup value,
  memcpy(chunk->data, (void*)&initial, chunk->len);
}

int32_t State_int32::value(void){
  return *(int32_t*)(chunk->data);
}

int32_t State_int32::swapValue(void){
  return *(int32_t*)(swap->data);
}

boolean State_int32::set(int32_t val){
  memcpy(chunk->data, (void*)&val, chunk->len);
  registerChangeWithManager();
  return true;
}

/*
states/state_int32.h

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#ifndef STATE_INT32_H_
#define STATE_INT32_H_

#include <arduino.h>
#include "states.h"

class State_int32 : public State {
public:
  State_int32(String nm, int32_t initial);
  // the business - these are interfaces to our memory hole
  int32_t value(void);
  int32_t swapValue(void);
  boolean set(int32_t val);
};

#endif

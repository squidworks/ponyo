/*
states/state_boolean.h

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#ifndef STATE_BOOLEAN_H_
#define STATE_BOOLEAN_H_

#include <arduino.h>
#include "states.h"

class State_boolean : public State {
public:
  State_boolean(String nm, boolean initial);
  // the business - these are interfaces to our memory hole
  boolean value(void);
  boolean swapValue(void);
  boolean set(boolean val);
};

#endif

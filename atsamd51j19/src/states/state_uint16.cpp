/*
states/state_uint16.cpp

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#include "state_uint16.h"

// not a c string, an arduino string (I know...)

State_uint16::State_uint16(String nm, uint16_t initial){
  // it's a:
  name = nm;
  // comprising of a wrapper on one,
  chunk = new Chunk_uint16();
  swap = new Chunk_uint16();
  // set startup value,
  memcpy(chunk->data, (void*)&initial, chunk->len);
}

uint16_t State_uint16::value(void){
  return *(uint16_t*)(chunk->data);
}

uint16_t State_uint16::swapValue(void){
  return *(uint16_t*)(swap->data);
}

boolean State_uint16::set(uint16_t val){
  memcpy(chunk->data, (void*)&val, chunk->len);
  registerChangeWithManager();
  return true;
}

/*
states/state_string.h

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#ifndef STATE_STRING_H_
#define STATE_STRING_H_

#include <arduino.h>
#include "states.h"

class State_string : public State {
public:
  State_string(String nm, String initial, size_t dimension);
  String value(void);
  String swapValue(void);
  boolean set(String str);
};

#endif

/*
manager.h

dataflow environment overlord 

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#ifndef MANAGER_H_
#define MANAGER_H_

#include <arduino.h>
#include "hunks/hunks.h"
#include "transports/multimessagedevice.h"
#include "transports/net_byteArray.h"

// ???? infinite
#define MGRMSG_BUFFER_LEN 1024

// msgs,
#define MK_MSGID 235
#define MK_HELLO 231
#define MK_ERR 254
#define MK_QUERY 251 // () reply brief or  (uint16 indice) reply hunk serialized
#define MK_BRIEF 250
#define MK_REQLISTAVAIL 249   // (eom)
#define MK_LISTOFAVAIL 248  // (list)(str) names 'dirs/like/this' (includes programs ?) (this might be multiple packets?)
#define MK_REQADDHUNK 247
#define MK_REQNAMECHANGE 246
#define MK_HUNKALIVE 245
#define MK_HUNKREPLACE 244
#define MK_REQSTATECHANGE 243 // (uint16 indice)(uint8 stindice) (type)
#define MK_HUNKSTATECHANGE 242
#define MK_REQRMHUNK 241 // (indice)
#define MK_HUNKREMOVED 240
#define MK_REQADDLINK 239
#define MK_LINKALIVE 238
#define MK_REQRMLINK 237
#define MK_LINKREMOVED 236

//
#define HK_NAME 253
#define HK_TYPE 252
#define HK_INDICE 251
#define HK_DESCR 250
#define HK_INPUT 249
#define HK_OUTPUT 247
#define HK_CONNECTIONS 246
#define HK_CONNECTION 245
#define HK_STATE 244

// us
#define HUNK_LIST_MAX_LENGTH 256

class Manager : public Hunk{
  private:
    // we are singleton,
    static Manager* instance;
    // that's a big bag, m8
    uint16_t hunkCount_ = 0;
    // local msgsbuffer
    unsigned char inBuffer_[MGRMSG_BUFFER_LEN];
    uint16_t inBufferLen_ = 0;
    // outgoing msg, builds as we reply,
    unsigned char outBuffer_[MGRMSG_BUFFER_LEN];
    // and puts to,
    MultiMessageDevice* mmd_ = new MultiMessageDevice();
    // us
    String interpreterName_ = "ponyo";
    String interpreterVersion_ = "v0.1";
    // to blink,
    uint16_t loop_tick_tracker_ = 0;
    // to register state changes,
    boolean hasStateUpdatesToMake_ = false;
    // ok,
  public:
    Manager();
    static Manager* getInstance(void);
    // we share, we trust
    Hunk* hunks[HUNK_LIST_MAX_LENGTH];
    // every1 has a name
    // ever1 has one
    void init(void);
    void loop(void);
    // in out for messages
    Inp_byteArray* inMsgs = new Inp_byteArray("mgrMsgs", MGRMSG_BUFFER_LEN);
    Outp_byteArray* outMsgs = new Outp_byteArray("mgrMsgs", MGRMSG_BUFFER_LEN);
    // we have some public fn's that the bootloop can use to setup programs,
    // these will be our friends, they are sucess / fail (?)
    void callBootstrap(void);
    // add one -> would like to do this with init state, how to abstract that?
    // parse bytes at the scene? go as string?
    Hunk* addHunkFromSerial(uint16_t bptr);
    Hunk* addHunk(String type, String name, unsigned char* states, uint16_t statesLen);
    Hunk* addHunk(String type, String name);
    Hunk* addHunk(String type);
    // for the compile time specialists,
    Hunk* includeHunk(Hunk* hnk);

    boolean rmHunk(uint16_t indice);
    // need this to add/rm links
    boolean addLink(uint16_t outHunkIndice, uint8_t outputIndice, uint16_t inHunkIndice, uint8_t inputIndice);
    boolean rmLink(uint16_t outHunkIndice, uint8_t outputIndice, uint16_t inHunkIndice, uint8_t inputIndice);

    // nice to send things upstream
    void sendLinkAlive(uint16_t outHunkIndice, uint8_t outputIndice, uint16_t inHunkIndice, uint8_t inputIndice, uint16_t bptr);

    // to change state variables,
    boolean changeState(uint8_t stateIndice, void* data, void* datalen);

    // the list,
    boolean serializeAndSendAvailable(uint16_t bptr);

    // a tricky pickle, this: replace flag to tell view we r updating, not a new bb
    boolean serializeAndSendHunk(Hunk* hnk, boolean replace, uint16_t bptr);

    // hunks will get a ptr to us, they will want to call this occasinally (on new inputs / outputs)
    boolean evaluateHunk(uint16_t ind);

    // us, doing work
    void writeStateUpdateMessage(uint16_t stChHunkIndice, uint8_t stIndice, uint16_t bptr);
    // fns for hunks (or others) to access manager global singleton: this just sets a flag,
    void registerStateChangeForNextLoop(void);

    // err pathways,
    void replyError(String msg, uint16_t bptr);
};

extern Manager* ponyo;

#endif

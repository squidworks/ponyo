/*
transports/typeset.cpp

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#include "typeset.h"

/*
if(typeCheck(inBuffer[1], outputs[pnum]->typekey, &dstart; &increment)){
  // that's right lads we r type blind here,
  // good faith on typeCheck (not blind)
  memcpy(outputs[pnum]->dataPtr, inBuffer[dstart], increment);
  outputs[pnum]->wasWritten(increment);
}
*/

Chunk_boolean::Chunk_boolean(void){
  type = TS_BOOLEAN_STRING;
  key = TS_BOOLEAN_KEY;
  isVarLen = false;
  len = 1;
  size = len;
  data = malloc(size);
}

Chunk_uint16::Chunk_uint16(void){
  type = TS_UINT16_STRING;
  key = TS_UINT16_KEY;
  isVarLen = false;
  len = TS_UINT16_DBYTES;
  size = len;
  data = malloc(size);
}

Chunk_uint32::Chunk_uint32(void){
  type = TS_UINT32_STRING;
  key = TS_UINT32_KEY;
  isVarLen = false;
  len = TS_UINT32_DBYTES;
  size = len;
  data = malloc(size);
}

Chunk_int32::Chunk_int32(void){
  type = TS_INT32_STRING;
  key = TS_INT32_KEY;
  isVarLen = false;
  len = TS_INT32_DBYTES;
  size = len;
  data = malloc(size);
}

Chunk_byteArray::Chunk_byteArray(size_t reserved){
  type = TS_BYTEARRAY_STRING;
  key = TS_BYTEARRAY_KEY;
  isVarLen = true;
  len = 0;
  size = reserved;
  data = malloc(size);
}

Chunk_MDmseg::Chunk_MDmseg(void){
  type = TS_MDMSEG_STRING;
  key = TS_MDMSEG_KEY;
  isVarLen = false;
  len = TS_MDMSEG_DBYTES;
  size = len;
  data = malloc(size);
}

Chunk_mseg::Chunk_mseg(void){
  type = TS_MSEG_STRING;
  key = TS_MSEG_KEY;
  isVarLen = false;
  len = TS_MSEG_DBYTES;
  size = len;
  data = malloc(size);
}

Chunk_string::Chunk_string(size_t reserved){
  type = TS_STRING_STRING;
  key = TS_STRING_KEY;
  isVarLen = true;
  len = 0;
  size = reserved;
  data = malloc(size);
}

boolean typeCheck(unsigned char* addr, Chunk* chunk, uint16_t* dstart, uint16_t* dlength){  // typkey is an enum (?) probably, or we run a big switch
  // addr, typkey, start, len
  // we use dstart to find what should be the type key,
  // we also increment it to point to the start of the actual data bytes, 0th msb,
  // and set len to show where end of data bytes should live
  // 1st, there should be at addr[1]
  if(addr[*dstart] == chunk->key){
    // it's good keywise, we can:
    if(chunk->isVarLen){
      // we get the length: todo: repl with readLenBytes
      (*dlength) = (addr[(*dstart) + 2] << 8) | addr[(*dstart) + 1];
      // for a type w/ length bytes, the data bits start 3 places from the key (1 key, 2 len bytes)
      (*dstart) += 3;
    } else {
      (*dlength) = chunk->len;
      (*dstart) += 1;
    }
    return true;
  } else {
    return false;
  }
}

boolean directWrite(unsigned char* dest, uint16_t* dptr, const void* src, uint8_t typekey, uint16_t len, boolean varlen){
  dest[(*dptr) ++] = typekey;
  if(varlen){
    writeLenBytes(dest, dptr, len);
  }
  memcpy(&dest[*dptr], src, len);
  (*dptr) += len;
  //dbs();
  //dbn("directWrite puts, buf ptr at: ");
  //dbf(*dptr);
  return true;
}


boolean typeWrite(unsigned char* dest, uint16_t* dptr, Chunk* chunk){
  dest[(*dptr) ++] = chunk->key;
  if(chunk->isVarLen){
    writeLenBytes(dest, dptr, chunk->len);
  }
  memcpy(&dest[*dptr], chunk->data, chunk->len);
  (*dptr) += chunk->len;
  // end debug
  //dbn("typeWrite puts, buf ptr at: ");
  //dbf(*dptr);
  return true;
}

// https://stackoverflow.com/questions/859770/post-increment-on-a-dereferenced-pointer
boolean writeBoolean(unsigned char* dest, uint16_t* dptr, boolean val){
  // key in front,
  dest[(*dptr) ++] = TS_BOOLEAN_KEY;
  if(val){
    dest[(*dptr) ++] = 1;
  } else {
    dest[(*dptr) ++] = 0;
  }
  dbs();
  dbn("dptr, apres writeboolean: ");
  dbf(*dptr);
  return true;
}

boolean readBoolean(unsigned char* addr, uint16_t* dptr){
  if(addr[*dptr] == TS_BOOLEAN_KEY){
    // bytes, and the key
    boolean ret = (boolean)addr[(*dptr) + 1];
    (*dptr) += TS_BOOLEAN_DBYTES + 1;
    return ret;
  } else {
    error("bad key on boolean read");
    (*dptr) += TS_BOOLEAN_DBYTES + 1;
    return false; // ??
  }
}

// this one is headless,
boolean writeLenBytes(unsigned char* dest, uint16_t* dptr, uint16_t len){
  dest[(*dptr) ++] = len;
  dest[(*dptr) ++] = (len >> 8) & 255;
  return true;
}

boolean writeUint16(unsigned char* dest, uint16_t* dptr, uint16_t val){
  dest[(*dptr) ++] = TS_UINT16_KEY;
  dest[(*dptr) ++] = val;
  dest[(*dptr) ++] = (val >> 8) & 255;
  return true;
}

uint16_t readUint16(unsigned char* addr, uint16_t* dptr){
  // this is cool as long as we're reading it off of the network,
  // where bytes are big eadian,
  // if we read out of memory they are likely to be little eadian
  if(addr[*dptr] == TS_UINT16_KEY){
    uint16_t ret = (uint8_t)addr[(*dptr) + 2] << 8 | (uint8_t)addr[(*dptr) + 1];
    (*dptr) += TS_UINT16_DBYTES + 1;
    return ret;
  } else {
    error("bad key on uint16 read");
    return 0;
  }
}

boolean writeUint8(unsigned char* dest, uint16_t* dptr, uint8_t val){
  dest[(*dptr) ++] = TS_UINT8_KEY;
  dest[(*dptr) ++] = val;
  return true;
}

uint8_t readUint8(unsigned char* addr, uint16_t* dptr){
  if(addr[*dptr] == TS_UINT8_KEY){
    uint8_t ret = (uint8_t)addr[(*dptr) + 1];
    (*dptr) += TS_UINT8_BYTES + 1;
    return ret;
  } else {
    error("bad key on uint8 read");
    return 0;
  }
}

boolean writeString(unsigned char* dest, uint16_t* dptr, String msg){
  uint16_t len = msg.length();
  dest[(*dptr) ++] = TS_STRING_KEY;
  writeLenBytes(dest, dptr, len);
  msg.getBytes(dest, len + 1);
  return true;
}

// this is a nice handle,
String readString(unsigned char* addr, uint16_t* dptr){
  if(addr[*dptr] == TS_STRING_KEY){
    // get the len,
    uint16_t blen = (uint8_t)addr[(*dptr) + 2] << 8 | (uint8_t)addr[(*dptr) + 1];
    char strChars[blen + 1];
    (*dptr) += 3; // inc for len and flag,
    memcpy(strChars, &addr[*dptr], blen);
    strChars[blen] = '\0';
    (*dptr) += blen;
    String str = String(strChars);
    return str;
  } else {
    error("bad key on string read");
    // haha,
    return "null";
  }
}

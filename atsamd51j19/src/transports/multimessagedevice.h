/*
transports/net_multimessagedevice.h

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#ifndef MULTIMESSAGEDEVICE_H_
#define MULTIMESSAGEDEVICE_H_

#include <arduino.h>
#include "../trunk.h"
#include "net_byteArray.h"

// have up to this many bytes, that many diff. msgs
#define MMD_BIG_BOI 2048//8192
#define MMD_MAX_MSGS 32
#define MMD_MEMBER_WRAP MMD_MAX_MSGS - 1

class MultiMessageDevice {
private:
  unsigned char bigbuf_[MMD_BIG_BOI];
  // I get that I should be using size_t to write a size,
  // however, convention be damned...
  // as brad leone says: "the thing about making things
  // yourself, vinny, is that you get to make them
  // the way you want"
  // altho, in this case, paying attention to standards
  // is just out of my bandwidth
  uint16_t size_ = MMD_BIG_BOI;
  // read ptrs, write ptrs
  uint16_t heads_[MMD_MAX_MSGS];
  uint16_t lens_[MMD_MAX_MSGS];
  // read member (smallest) and write member (position to write into)
  uint8_t rm_;
  uint8_t wm_;
  // hmmm ....
public:
  MultiMessageDevice(void);
  // to put to,
  boolean put(unsigned char* data, uint16_t len);
  // to check if has anything
  uint8_t getNumWaiting(void);
  // drop in to this thing,
  boolean releaseTo(Outp_byteArray* op);
};

#endif

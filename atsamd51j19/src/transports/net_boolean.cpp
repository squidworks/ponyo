/*
transports/net_boolean.cpp

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#include "net_boolean.h"

// set class variables in the constructor?
Inp_boolean::Inp_boolean(String nm){
  name = nm;
  typeKey = TS_BOOLEAN_KEY;
  typeName = TS_BOOLEAN_STRING;
}

// inputs just need to have get() typed,
boolean Inp_boolean::get(void){
  // getNextWireIndice will throw the err, if need
  uint8_t i = getNextWireIndice();
  // copies out of upstream outputs' data chunk
  boolean dt = *((boolean*)(connections[i]->op->chunk->data));
  manualReadFrom(i);
  return dt; // (uint32_t)dataPtr;
}

Outp_boolean::Outp_boolean(String nm){
  name = nm;
  chunk = new Chunk_boolean();
}

// and outputs just need to type input ...
boolean Outp_boolean::put(boolean data){
  if(io()){
    return false;
  } else {
    /*
    trunk->print("uint32_t puts: ");
    trunk->println(data);
    void* result = */
    memcpy(chunk->data, (void *)&data, chunk->size);
    manualSetIo(0);
    /*
    uint32_t rst = *(uint32_t*)result;
    trunk->print("resulting copy and readout: ");
    trunk->println(rst);
    */
    return true;
  }
}

/*
transports/net_bytearray.h

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#ifndef NET_BYTEARRAY_H_
#define NET_BYTEARRAY_H_
#include <arduino.h>
#include "nets.h"
#include "trunk.h"

// INPUT
class Inp_byteArray : public Input{
public:
  Inp_byteArray(String nm, size_t dimension);
  uint16_t get(unsigned char* dest);
};

// OUTPUT
class Outp_byteArray : public Output{
public:
  Outp_byteArray(String nm, size_t dimension);
  boolean put(unsigned char* data, uint16_t len);
};

#endif

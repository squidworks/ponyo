/*
transports/net_boolean.h

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#ifndef NET_BOOLEAN_H_
#define NET_BOOLEAN_H_
#include <arduino.h>
#include "nets.h"
#include "trunk.h"

// INPUT
class Inp_boolean : public Input{
public:
  Inp_boolean(String nm);
  boolean get(void);
};

// OUTPUT
class Outp_boolean : public Output{
public:
  Outp_boolean(String nm);
  boolean put(boolean);
};

#endif

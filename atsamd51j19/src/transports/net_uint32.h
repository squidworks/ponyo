/*
transports/net_uint32.h

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#ifndef NET_UINT32_H_
#define NET_UINT32_H_
#include <arduino.h>
#include "nets.h"
#include "trunk.h"

// INPUT
class Inp_uint32 : public Input{
public:
  Inp_uint32(String nm);
  uint32_t get(void);
};

// OUTPUT
class Outp_uint32 : public Output{
public:
  Outp_uint32(String nm);
  boolean put(uint32_t);
};

#endif

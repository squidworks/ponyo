/*
transports/net_mseg.cpp

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#include "net_mseg.h"

Inp_mseg::Inp_mseg(String nm){
  name = nm;
  typeKey = TS_MSEG_KEY;
  typeName = TS_MSEG_STRING;
}

void Inp_mseg::get(float* duration, float* displacement, float* v0){
  uint8_t i = getNextWireIndice();
  *duration = *((float*)(connections[i]->op->chunk->data));
  *displacement = *((float*)((char*)connections[i]->op->chunk->data + 4));
  *v0 = *((float*)((char*)connections[i]->op->chunk->data + 8));
  manualReadFrom(i);
}

Outp_mseg::Outp_mseg(String nm){
  name = nm;
  chunk = new Chunk_mseg();
}

boolean Outp_mseg::put(float duration, float displacement, float v0){
  if(io()){
    return false;
  } else {
    memcpy(chunk->data, (void*)&duration, 4);
    memcpy((char*)chunk->data + 4, (void*)&displacement, 4);
    memcpy((char*)chunk->data + 8, (void*)&v0, 4);
    manualSetIo(0);
    return true;
  }
}

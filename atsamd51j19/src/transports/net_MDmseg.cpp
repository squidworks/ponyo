/*
transports/net_mdmseg.cpp

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#include "net_MDmseg.h"

Inp_MDmseg::Inp_MDmseg(String nm){
  name = nm;
  typeKey = TS_MDMSEG_KEY;
  typeName = TS_MDMSEG_STRING;
}

void Inp_MDmseg::get(float* p0, float* p1, float* t, float* v0, float* a){
  uint8_t i = getNextWireIndice();
  p0[0] = *((float*)(connections[i]->op->chunk->data));
  p0[1] = *((float*)((char*)connections[i]->op->chunk->data + 4));
  p0[2] = *((float*)((char*)connections[i]->op->chunk->data + 8));

  p1[0] = *((float*)((char*)connections[i]->op->chunk->data + 12));
  p1[1] = *((float*)((char*)connections[i]->op->chunk->data + 16));
  p1[2] = *((float*)((char*)connections[i]->op->chunk->data + 20));

  *t = *((float*)((char*)connections[i]->op->chunk->data + 24));
  *v0 = *((float*)((char*)connections[i]->op->chunk->data + 28));
  *a = *((float*)((char*)connections[i]->op->chunk->data + 32));
  // to note that we have read this thing,
  manualReadFrom(i);
}

Outp_MDmseg::Outp_MDmseg(String nm){
  name = nm;
  chunk = new Chunk_MDmseg();
}

boolean Outp_MDmseg::put(float* p0, float* p1, float t, float v0, float a){
  error("MDmseg doesn't have a put call (!)");
  if(io()){
    return false;
  } else {
    // and then will,
    manualSetIo(0);
    return true;
  }
}

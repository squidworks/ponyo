/*
transports/net_uint32.cpp

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#include "net_uint32.h"

// set class variables in the constructor?
Inp_uint32::Inp_uint32(String nm){
  name = nm;
  typeKey = TS_UINT32_KEY;
  typeName = TS_UINT32_STRING;
}

// inputs just need to have get() typed,
uint32_t Inp_uint32::get(void){
  uint8_t i = getNextWireIndice();
  uint32_t dt = *((uint32_t*)(connections[i]->op->chunk->data));
  manualReadFrom(i);
  return dt;
}

Outp_uint32::Outp_uint32(String nm){
  name = nm;
  chunk = new Chunk_uint32();
}

// and outputs just need to type input ...
boolean Outp_uint32::put(uint32_t data){
  if(io()){
    return false;
  } else {
    /*
    trunk->print("uint32_t puts: ");
    trunk->println(data);
    void* result = */
    memcpy(chunk->data, (void *)&data, chunk->size);
    manualSetIo(0);
    /*
    uint32_t rst = *(uint32_t*)result;
    trunk->print("resulting copy and readout: ");
    trunk->println(rst);
    */
    return true;
  }
}

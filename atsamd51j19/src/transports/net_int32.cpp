/*
transports/net_int32.cpp

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#include "net_int32.h"

// set class variables in the constructor?
Inp_int32::Inp_int32(String nm){
  name = nm;
  typeKey = TS_INT32_KEY;
  typeName = TS_INT32_STRING;
}

// inputs just need to have get() typed,
int32_t Inp_int32::get(void){
  uint8_t i = getNextWireIndice();
  int32_t dt = *((int32_t*)(connections[i]->op->chunk->data));
  manualReadFrom(i);
  return dt;
}

Outp_int32::Outp_int32(String nm){
  name = nm;
  chunk = new Chunk_int32();
}

// and outputs just need to type input ...
boolean Outp_int32::put(int32_t data){
  if(io()){
    return false;
  } else {
    /*
    trunk->print("int32_t puts: ");
    trunk->println(data);
    void* result = */
    memcpy(chunk->data, (void *)&data, chunk->size);
    manualSetIo(0);
    /*
    int32_t rst = *(int32_t*)result;
    trunk->print("resulting copy and readout: ");
    trunk->println(rst);
    */
    return true;
  }
}

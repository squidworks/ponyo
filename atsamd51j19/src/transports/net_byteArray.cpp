/*
transports/net_byteArray.cpp

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#include "net_byteArray.h"
#include "trunk.h"

Inp_byteArray::Inp_byteArray(String nm, size_t dimension){
  name = nm + "_" + dimension;
  typeKey = TS_BYTEARRAY_KEY;
  typeName = TS_BYTEARRAY_STRING;
}

uint16_t Inp_byteArray::get(unsigned char* dest){
  uint8_t i = getNextWireIndice();
  // copy out of here into hunk-local memory
  size_t len = connections[i]->op->chunk->len;
  memcpy(dest, connections[i]->op->chunk->data, len);
  // sys
  manualReadFrom(i);
  return len;
}

Outp_byteArray::Outp_byteArray(String nm, size_t dimension){
  name = nm + "_" + dimension;
  chunk = new Chunk_byteArray(dimension);
}

boolean Outp_byteArray::put(unsigned char* data, uint16_t length){
  if(io()){
    return false;
    debug("bad byteArray put call");
  } else {
    memcpy(chunk->data, data, length);
    manualSetIo(length);
    return true;
  }
}

/*
transports/typeset.h

serialization routines for ~ t h e i n t e r o p n e t ~ 

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#ifndef TYPESET_H_
#define TYPESET_H_

#include <arduino.h>
#include "trunk.h"

#define TS_DBYTES_UNKNOWN 244

// this is gonna get hefty, here we go
// should these be structs?
#define TS_BOOLEAN_KEY 32
#define TS_BOOLEAN_STRING "boolean"
#define TS_BOOLEAN_DBYTES 1

#define TS_BYTEARRAY_KEY 35
#define TS_BYTEARRAY_STRING "byteArray"
// #define TS_BYTEARRAY_DBYTES TS_DBYTES_UNKNOWN

#define TS_STRING_KEY 37
#define TS_STRING_STRING "string"

#define TS_UINT8_KEY 38
#define TS_UINT8_STRING "uint8"
#define TS_UINT8_BYTES 1

#define TS_UINT16_KEY 40
#define TS_UINT16_STRING "uint16"
#define TS_UINT16_DBYTES 2

#define TS_UINT32_KEY 42
#define TS_UINT32_STRING "uint32"
#define TS_UINT32_DBYTES 4

#define TS_INT32_KEY 50
#define TS_INT32_STRING "int32"
#define TS_INT32_DBYTES 4

#define TS_MDMSEG_KEY 88
#define TS_MDMSEG_STRING "MDmseg"
#define TS_MDMSEG_DBYTES 36

#define TS_MSEG_KEY 89
#define TS_MSEG_STRING "mseg"
#define TS_MSEG_DBYTES 12

// link codes
#define LK_ACK 254
#define LK_HELLO 253
#define LK_LLDEBUG 252

// base class, ah chunk of memory
class Chunk{
public:
  // naming, tracking
  String type;
  uint8_t key;
  boolean isVarLen;
  // size is malloc'd, len is currently set (for varlen)
  // for not-varlen, size always == len
  size_t len;
  size_t size;
  // the ptr to our data
  void* data;
};

class Chunk_boolean : public Chunk{
public:
  Chunk_boolean(void);
};

class Chunk_uint16 : public Chunk{
public:
  Chunk_uint16(void);
};

class Chunk_uint32 : public Chunk{
public:
  Chunk_uint32(void);
};

class Chunk_int32 : public Chunk{
public:
  Chunk_int32(void);
};

class Chunk_byteArray : public Chunk{
public:
  Chunk_byteArray(size_t reserved);
};

class Chunk_MDmseg : public Chunk{
public:
  Chunk_MDmseg(void);
};

class Chunk_mseg : public Chunk{
public:
  Chunk_mseg(void);
};

class Chunk_string : public Chunk{
public:
  Chunk_string(size_t reserved);
};

// to direct write,
boolean directWrite(unsigned char* dest, uint16_t* dptr, const void* src, uint8_t typekey, uint16_t len, boolean varlen);

boolean typeCheck(unsigned char* addr, Chunk* chunk, uint16_t* dstart, uint16_t* dlength);
boolean typeWrite(unsigned char* dest, uint16_t* dptr, Chunk* chunk);

// one per type, yikes, lots to write
boolean writeBoolean(unsigned char* dest, uint16_t* dptr, boolean val);
boolean readBoolean(unsigned char* addr, uint16_t* dptr);

// doublets
boolean writeLenBytes(unsigned char* dest, uint16_t* dptr, uint16_t len);
boolean writeUint16(unsigned char* dest, uint16_t* dptr, uint16_t val);
uint16_t readUint16(unsigned char* addr, uint16_t* dptr);

// and also
boolean writeUint8(unsigned char* dest, uint16_t* dptr, uint8_t val);
uint8_t readUint8(unsigned char* addr, uint16_t* dptr);

// and also
boolean writeString(unsigned char* dest, uint16_t* dptr, String msg);
String readString(unsigned char* addr, uint16_t* dptr);

#endif

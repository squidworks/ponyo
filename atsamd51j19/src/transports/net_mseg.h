/*
transports/net_mseg.h

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#ifndef NET_MSEG_H_
#define NET_MSEG_H_

#include <arduino.h>
#include "nets.h"
#include "trunk.h"

class Inp_mseg : public Input {
public:
  Inp_mseg(String nm);
  void get(float* duration, float* displacement, float* v0);
};

class Outp_mseg : public Output {
public:
  Outp_mseg(String nm);
  boolean put(float duration, float displacement, float v0);
};

#endif

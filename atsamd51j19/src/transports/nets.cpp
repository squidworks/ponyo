/*
transports/nets.cpp

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#include "nets.h"
#include "trunk.h"

boolean Input::io(void){
  for(uint8_t i = 0; i < numConnections; i ++){
    if(connections[i]->io){
      return true;
    }
  }
  return false;
}

uint8_t Input::getNextWireIndice(void){
  uint8_t i = lastGet;
  for(uint8_t u = 0; u < numConnections; u ++){
    i ++;
    if(i >= numConnections){
      i = 0;
    }
    if(connections[i]->io){
      return i;
    }
  }
  // THROW ERR, returning nullptr is going to just convert to a number ...
  // should fail in some other way ..
  error("RETURN NULL");
  // halting
  digitalWrite(13, HIGH);
  while(1);
  return 255;
}

void Input::manualReadFrom(uint8_t wireIndice){
  connections[wireIndice]->io = false;
  lastGet = wireIndice;
}

boolean Input::disconnect(Output* op){
  // find it in our array, rm, shift array...
  for(uint8_t i = 0; i < numConnections; i ++){
    if(connections[i]->op == op){
      // write over by backfilling
      for(uint8_t u = i + 1; u < numConnections; u ++){
        connections[i] = connections[u];
        i ++;
      }
      numConnections --;
      return true;
    }
  }
  return false;
}

boolean Output::io(void){
  for(uint8_t i = 0; i < numConnections; i ++){
    if(connections[i].io){
      return true;
    }
  }
  return false;
}

// we can actually do a lot of this non-specifically
boolean Output::attach(Input* ip){
  if(numConnections < MAX_TRANSPORT_MULTIPLEX && ip->numConnections < MAX_TRANSPORT_MULTIPLEX){
    // we can typecheck at attach
    if(ip->typeKey != chunk->key){
      error("attach: type mismatch, output is: " + chunk->type + "input is: " + ip->typeName);
      return false;
    }
    // ok then, add to our list of conn ...
    connections[numConnections].op = this;
    connections[numConnections].ip = ip;
    connections[numConnections].io = false;
    // ptr to that in the input
    ip->connections[ip->numConnections] = &(connections[numConnections]);
    numConnections ++;
    ip->numConnections ++;
    return true;
  } else {
    error("attach: maxed out transport multiplex");
    return false;
  }
}

boolean Output::remove(Input* ip){
  // find it in our array, rm, shift array...
  for(uint8_t i = 0; i < numConnections; i ++){
    if(connections[i].ip == ip){
      // partner deletes,
      if(!(ip->disconnect(this))){
        return false;
      }
      // write over by backfilling
      for(uint8_t u = i + 1; u < numConnections; u ++){
        connections[i] = connections[u];
        i ++;
      }
      numConnections --;
      return true;
    }
  }
  return false;
}

void Output::manualSetIo(size_t increment){
  if(chunk->isVarLen){
    chunk->len = increment;
  }
  for(uint8_t i = 0; i < numConnections; i ++){
    connections[i].io = true;
  }
}

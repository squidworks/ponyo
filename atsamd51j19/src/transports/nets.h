/*
transports/nets.h

virtual wires for virtual dataflow environment 

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#ifndef NETS_H_
#define NETS_H_

// because we are including a parent
// for each input, output, and state,
// the compiler would otherwise see a cyclic redundancy,
// so we pull a 'forward declaration', like this:
class Hunk;
// https://stackoverflow.com/questions/625799/resolve-build-errors-due-to-circular-dependency-amongst-classes

#include <arduino.h>
#include "hunks/hunks.h"
#include "typeset.h"

#define MAX_TRANSPORT_MULTIPLEX 16

typedef struct {
  Output* op;
  Input* ip;
  boolean io;
} Wire;

class Input{
public:
  // ref for sys and humans
  String name;
  String typeName;
  uint8_t typeKey;
  // sys (manager) needs to track
  uint16_t parentIndice;
  uint8_t indice;
  // state
  uint16_t numConnections = 0;
  Wire* connections[MAX_TRANSPORT_MULTIPLEX];
  boolean io(void);
  uint8_t lastGet = 0;
  uint8_t getNextWireIndice(void);
  void manualReadFrom(uint8_t wireIndice);
  boolean disconnect(Output* op);
  // actions
  // pls write 'type get()' as an interface to those bytes
};

class Output{
public:
  // naming, typing, data
  String name;
  Chunk* chunk;
  // sys (manager) needs to track
  uint16_t parentIndice;
  uint8_t indice;
  // the business
  // built in to output
  uint16_t numConnections = 0;
  Wire connections[MAX_TRANSPORT_MULTIPLEX];
  boolean io(void);
  // could do: make attach, remove, transport virtual: then can 'attach' with DMA lines, etc...
  boolean attach(Input* ip);
  boolean remove(Input* ip);
  void manualSetIo(size_t increment);
  // *does* have to have connections ...
  // pls write the interface; boolean put(type);
};

#endif

/*
transports/net_int32.h

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#ifndef NET_int32_H_
#define NET_int32_H_
#include <arduino.h>
#include "nets.h"
#include "trunk.h"

// INPUT
class Inp_int32 : public Input{
public:
  Inp_int32(String nm);
  int32_t get(void);
};

// OUTPUT
class Outp_int32 : public Output{
public:
  Outp_int32(String nm);
  boolean put(int32_t);
};

#endif

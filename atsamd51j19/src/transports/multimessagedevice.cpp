/*
transports/net_multimessagedevice.cpp

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#include "multimessagedevice.h"

// a tool, offering a rekluse of roughly 10kb to those
// in need of storing multiple messages, dishing them to sys one at
// a time, i.e. link, i.e. manager

MultiMessageDevice::MultiMessageDevice(void){
  // set zeroes yeh?
  heads_[0] = 0;
  lens_[0] = 0;
  // and we have
  rm_ = 0;
  wm_ = 0;
}

boolean MultiMessageDevice::put(unsigned char* data, uint16_t len){
  dbs();
  dbn("MMD to put for len: ");
  dbf(len);

  // (1) does the head need a wrap?
  if(heads_[wm_] + len + 1 > MMD_BIG_BOI){
    if(len > heads_[rm_]){
      // we full up
      error("MMD FULL 2 GILLZ");
      return false;
    } else {
      // simple simple, wrap heads not chunks, stay contiguous, stay memcpy
      heads_[wm_] = 0;
    }
  }

  // (2) now we r guarantor'd this is ok,
  memcpy(&bigbuf_[heads_[wm_]], data, len);

  // (3) so, for this member at wm_, we'll read this many bytes out:
  lens_[wm_] = len;

  // (4) now that we've done that, the member to write to is the next one, but track...
  uint8_t lastmem = wm_;
  wm_ ++;
  if(wm_ > MMD_MEMBER_WRAP){
    debug("HOT WARNING: -------> MMD WRAPS ON PUT");
    wm_ = 0;
  }
  // and the write ptr there is the end of this thing,
  heads_[wm_] = heads_[lastmem] + len + 1;
  // ostensibly this went ok,
  return true;
}

uint8_t MultiMessageDevice::getNumWaiting(void){
  // ok, if they are the same, it's none:
  if(rm_ == wm_){
    return 0;
  } else {
    // actually, we don't GAF, it's not none, so
    return 1;
  }
}

boolean MultiMessageDevice::releaseTo(Outp_byteArray* op){
  // only bad case is:
  if(rm_ == wm_ || lens_[rm_] == 0){
    error("bad call to mmd releaseTo");
    return false;
  }
  // otherwise do,
  dbs();
  dbn("MMD to release for len: ");
  dbf(lens_[rm_]);
  // so this is easy,
  op->put(&bigbuf_[heads_[rm_]], lens_[rm_]);
  // to err check, we can do:
  lens_[rm_] = 0;
  // now we just need to increment that read ptr,
  rm_ ++;
  if(rm_ > MMD_MEMBER_WRAP){
    debug("HOT WARNING: -------> MMD WRAPS ON READ");
    rm_ = 0;
  }
  return true;
}

/*
transports/net_mdmseg.h

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#ifndef NET_MDMSEG_H_
#define NET_MDMSEG_H_

#include <arduino.h>
#include "nets.h"
#include "trunk.h"

// INPUT
class Inp_MDmseg : public Input{
public:
  Inp_MDmseg(String nm);
  void get(float* p0, float* p1, float* t, float* v0, float* a);
};

class Outp_MDmseg : public Output{
public:
  Outp_MDmseg(String nm);
  boolean put(float* p0, float* p1, float t, float v0, float a);
};

#endif

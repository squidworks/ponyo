/*
trunk.h

low-level debug interface

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#ifndef TRUNK_H_
#define TRUNK_H_
#include <arduino.h>
#include "build_config.h"
#include "debugpins.h"
#include "transports/typeset.h"
#include "utils/cobs.h"
// we can do this by extending Print

#define TRUNK_OUTBUF_LEN 1024

// -> 1 to get debug messages
#ifdef DEBUG_MSGS
#define debug(msg) if(Serial){trunk->print("DBG: "); trunk->println(msg);}
#define dbs() if(Serial){trunk->print("DBG: ");}
#define dbn(msg) if(Serial){trunk->print(msg);}
#define dbf(msg) if(Serial){trunk->println(msg);}
#else
#define debug(msg)
#define dbs()
#define dbn(msg)
#define dbf(msg)
#endif

// always
#define adbs() if(Serial){trunk->print("DBG: ");}
#define adbn(msg) if(Serial){trunk->print(msg);}
#define adbf(msg) if(Serial){trunk->println(msg);}

// always
#define error(msg) if(Serial){trunk->print("ERR: "); trunk->println(msg);}

// also always (semantics!)
#define adebug(msg) if(Serial){trunk->print("DBG: "); trunk->println(msg);}

class Trunk : public Print{
private:
  // one buffer for internal (writes to before COBS)
  uint8_t buf_[TRUNK_OUTBUF_LEN];
  size_t ps_ = 0;
  // on buffer to add err-is flag to front of,
  // yes yes, messy, yes
  uint8_t errbuf_[TRUNK_OUTBUF_LEN];
  // one buffer for copying-into as we write COBS packet
  uint8_t cbuf_[TRUNK_OUTBUF_LEN];
  static Trunk* instance;
public:
  Trunk();
  static Trunk* getInstance(void);
  virtual size_t write(uint8_t bt);
  // writes 2 serial port,
  // should be abstracted to write to a buffer, however
  boolean writeCOBS_USBPacket(uint8_t* buf, size_t len);
};

extern Trunk* trunk;

#endif

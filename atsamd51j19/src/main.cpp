/*
main.cpp

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#include <arduino.h>
// useful to know: https://github.com/adafruit/ArduinoCore-samd/tree/master/variants/feather_m4
// in particular, https://github.com/adafruit/ArduinoCore-samd/blob/master/variants/feather_m4/variant.cpp
// for platformio source, to modify (for SERCOM5_2_Handler issue)
// users/<me>/.platformio/packages/framework-arduinosam/variants/feather_m4/variant.cpp

// two singletons,
#include "manager.h"
#include "trunk.h"

void setup() {
  // lights!
  // low level!
  CLKLIGHT_SETUP;
  ERRLIGHT_SETUP;
  CLKLIGHT_ON;
  ERRLIGHT_ON;
  // and maybe,
  DEBUGPIN_A_SETUP;
  #ifdef BOARD_IS_ROUTER
    // turn this light off... not sure where it gets set low, but it does
    PORT->Group[0].DIRSET.reg |= (uint32_t)(1 << 6);
    PORT->Group[0].OUTSET.reg |= (uint32_t)(1 << 6);
  #endif
  // hmmm
  #ifdef BOOT_USB
    Serial.begin(9600);
    while(!Serial);
    debug("boostrap startup, serial hello");
  #else
    // ?
  #endif
  // setting up au-manuel for now
  __enable_irq();
  // indice (0)
  ponyo->init();
  ERRLIGHT_OFF;
  // remainder of the bootstrap,
  ponyo->callBootstrap();
}

// because we rely on arduino / adafruit USB implementation,
// we have to (if using usb) run inside of the arduino's loop,
// where some work goes on in-between. otherwise, we just lock into a while(1)
void loop() {
  ponyo->loop();
}
/*
#ifdef BOOT_USB
  void loop() {
    ponyo->loop();
  }
#else
  void loop() {
    while(1){
      ponyo->loop();
    }
  }
#endif
*/

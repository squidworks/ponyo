/*
isrcpp.cpp

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#include "isrcpp.h"
#include "debugpins.h"
#include "manager.h"

ISRCPP* ISRCPP::instance = 0;

ISRCPP* ISRCPP::getInstance(void){
  if(instance == 0){
    instance = new ISRCPP();
  }
  return instance;
}

ISRCPP::ISRCPP(){
  // nothing, right?
}

ISRCPP* isrcpp = ISRCPP::getInstance();

/*

boolean ISRCPP::register_for_tc0(uint16_t hunkIndice, uint8_t handlerIndice){
  // store that, call it later...
  tc0_hunkIndice = hunkIndice;
  tc0_handlerIndice = handlerIndice;
  return true;
}

// dangit https://www.embedded.com/design/prototyping-and-development/4023817/Interrupts-in-C-
void TC0_Handler(void){
  // DEBUG_PIN_A_SET;
  // ptr ptr ptr ptr yikes
  if(isrcpp->tc0_hunkIndice){
    switch (isrcpp->tc0_handlerIndice){
      case HUNK_HANDLER_A:
        ponyo->hunks[isrcpp->tc0_hunkIndice]->isr_handler_a();
        break;
      case HUNK_HANDLER_B:
        ponyo->hunks[isrcpp->tc0_hunkIndice]->isr_handler_b();
        break;
      case HUNK_HANDLER_NOHANDLER:
        // noooop
        break;
      default:
        // noooop
        break;
    }
  }
  // DEBUG_PIN_A_CLEAR;
}

boolean ISRCPP::register_for_tc5(uint16_t hunkIndice, uint8_t handlerIndice){
  // store that, call it later...
  tc5_hunkIndice = hunkIndice;
  tc5_handlerIndice = handlerIndice;
  return true;
}

// dangit https://www.embedded.com/design/prototyping-and-development/4023817/Interrupts-in-C-
// this interrupt takes ~ 800ns to complete ...
// for serving (at the isr_handler_x) two register access lines
void TC5_Handler(void){
  //DEBUG_PIN_A_SET;
  // ptr ptr ptr ptr yikes
  if(isrcpp->tc5_hunkIndice){
    switch (isrcpp->tc5_handlerIndice){
      case HUNK_HANDLER_A:
        ponyo->hunks[isrcpp->tc5_hunkIndice]->isr_handler_a();
        break;
      case HUNK_HANDLER_B:
        ponyo->hunks[isrcpp->tc5_hunkIndice]->isr_handler_b();
        break;
      case HUNK_HANDLER_NOHANDLER:
        // noooop
        break;
      default:
        // noooop
        break;
    }
  }
  //DEBUG_PIN_A_CLEAR;
}

*/

/* --------------------------- */
/* ---------- SER0 ----------- */
/* --------------------------- */

boolean ISRCPP::register_for_ser0_rx(uint16_t hunkIndice, uint8_t handlerIndice){
  ser0_rx_hunkIndice = hunkIndice;
  ser0_rx_handlerIndice = handlerIndice;
  return true;
}

void SERCOM0_2_Handler(void){
  if(isrcpp->ser0_rx_hunkIndice){
    switch (isrcpp->ser0_rx_handlerIndice){
      case HUNK_HANDLER_A:
        ponyo->hunks[isrcpp->ser0_rx_hunkIndice]->isr_handler_a();
        break;
      case HUNK_HANDLER_B:
        ponyo->hunks[isrcpp->ser0_rx_hunkIndice]->isr_handler_b();
        break;
      case HUNK_HANDLER_NOHANDLER:
        // noooop
        break;
      default:
        // noooop
        break;
    }
  }
}

boolean ISRCPP::register_for_ser0_tx(uint16_t hunkIndice, uint8_t handlerIndice){
  ser0_tx_hunkIndice = hunkIndice;
  ser0_tx_handlerIndice = handlerIndice;
  return true;
}

void SERCOM0_0_Handler(void){
  if(isrcpp->ser0_tx_hunkIndice){
    switch (isrcpp->ser0_tx_handlerIndice){
      case HUNK_HANDLER_A:
        ponyo->hunks[isrcpp->ser0_tx_hunkIndice]->isr_handler_a();
        break;
      case HUNK_HANDLER_B:
        ponyo->hunks[isrcpp->ser0_tx_hunkIndice]->isr_handler_b();
        break;
      case HUNK_HANDLER_NOHANDLER:
        // noooop
        break;
      default:
        // noooop
        break;
    }
  }
}

/* --------------------------- */
/* ---------- SER1 ----------- */
/* --------------------------- */

boolean ISRCPP::register_for_ser1_rx(uint16_t hunkIndice, uint8_t handlerIndice){
  ser1_rx_hunkIndice = hunkIndice;
  ser1_rx_handlerIndice = handlerIndice;
  return true;
}

void SERCOM1_2_Handler(void){
  if(isrcpp->ser1_rx_hunkIndice){
    switch (isrcpp->ser1_rx_handlerIndice){
      case HUNK_HANDLER_A:
        ponyo->hunks[isrcpp->ser1_rx_hunkIndice]->isr_handler_a();
        break;
      case HUNK_HANDLER_B:
        ponyo->hunks[isrcpp->ser1_rx_hunkIndice]->isr_handler_b();
        break;
      case HUNK_HANDLER_NOHANDLER:
        // noooop
        break;
      default:
        // noooop
        break;
    }
  }
}

boolean ISRCPP::register_for_ser1_tx(uint16_t hunkIndice, uint8_t handlerIndice){
  ser1_tx_hunkIndice = hunkIndice;
  ser1_tx_handlerIndice = handlerIndice;
  return true;
}

void SERCOM1_0_Handler(void){
  if(isrcpp->ser1_tx_hunkIndice){
    switch (isrcpp->ser1_tx_handlerIndice){
      case HUNK_HANDLER_A:
        ponyo->hunks[isrcpp->ser1_tx_hunkIndice]->isr_handler_a();
        break;
      case HUNK_HANDLER_B:
        ponyo->hunks[isrcpp->ser1_tx_hunkIndice]->isr_handler_b();
        break;
      case HUNK_HANDLER_NOHANDLER:
        // noooop
        break;
      default:
        // noooop
        break;
    }
  }
}

/* --------------------------- */
/* ---------- SER2 ----------- */
/* --------------------------- */

boolean ISRCPP::register_for_ser2_rx(uint16_t hunkIndice, uint8_t handlerIndice){
  ser2_rx_hunkIndice = hunkIndice;
  ser2_rx_handlerIndice = handlerIndice;
  return true;
}

void SERCOM2_2_Handler(void){
  if(isrcpp->ser2_rx_hunkIndice){
    switch (isrcpp->ser2_rx_handlerIndice){
      case HUNK_HANDLER_A:
        ponyo->hunks[isrcpp->ser2_rx_hunkIndice]->isr_handler_a();
        break;
      case HUNK_HANDLER_B:
        ponyo->hunks[isrcpp->ser2_rx_hunkIndice]->isr_handler_b();
        break;
      case HUNK_HANDLER_NOHANDLER:
        // noooop
        break;
      default:
        // noooop
        break;
    }
  }
}

boolean ISRCPP::register_for_ser2_rx_on_one(uint16_t hunkIndice, uint8_t handlerIndice){
  ser2_rx_one_hunkIndice = hunkIndice;
  ser2_rx_one_handlerIndice = handlerIndice;
  return true;
}

void SERCOM2_1_Handler(void){
  if(isrcpp->ser2_rx_one_hunkIndice){
    switch (isrcpp->ser2_rx_one_handlerIndice){
      case HUNK_HANDLER_A:
        ponyo->hunks[isrcpp->ser2_rx_one_hunkIndice]->isr_handler_a();
        break;
      case HUNK_HANDLER_B:
        ponyo->hunks[isrcpp->ser2_rx_one_hunkIndice]->isr_handler_b();
        break;
      case HUNK_HANDLER_NOHANDLER:
        // noooop
        break;
      default:
        // noooop
        break;
    }
  }
}

boolean ISRCPP::register_for_ser2_tx(uint16_t hunkIndice, uint8_t handlerIndice){
  ser2_tx_hunkIndice = hunkIndice;
  ser2_tx_handlerIndice = handlerIndice;
  return true;
}

void SERCOM2_0_Handler(void){
  if(isrcpp->ser2_tx_hunkIndice){
    switch (isrcpp->ser2_tx_handlerIndice){
      case HUNK_HANDLER_A:
        ponyo->hunks[isrcpp->ser2_tx_hunkIndice]->isr_handler_a();
        break;
      case HUNK_HANDLER_B:
        ponyo->hunks[isrcpp->ser2_tx_hunkIndice]->isr_handler_b();
        break;
      case HUNK_HANDLER_NOHANDLER:
        // noooop
        break;
      default:
        // noooop
        break;
    }
  }
}

/* --------------------------- */
/* ---------- SER3 ----------- */
/* --------------------------- */

boolean ISRCPP::register_for_ser3_rx(uint16_t hunkIndice, uint8_t handlerIndice){
  ser3_rx_hunkIndice = hunkIndice;
  ser3_rx_handlerIndice = handlerIndice;
  return true;
}

void SERCOM3_2_Handler(void){
  if(isrcpp->ser3_rx_hunkIndice){
    switch (isrcpp->ser3_rx_handlerIndice){
      case HUNK_HANDLER_A:
        ponyo->hunks[isrcpp->ser3_rx_hunkIndice]->isr_handler_a();
        break;
      case HUNK_HANDLER_B:
        ponyo->hunks[isrcpp->ser3_rx_hunkIndice]->isr_handler_b();
        break;
      case HUNK_HANDLER_NOHANDLER:
        // noooop
        break;
      default:
        // noooop
        break;
    }
  }
}

boolean ISRCPP::register_for_ser3_tx(uint16_t hunkIndice, uint8_t handlerIndice){
  ser3_tx_hunkIndice = hunkIndice;
  ser3_tx_handlerIndice = handlerIndice;
  return true;
}

void SERCOM3_0_Handler(void){
  if(isrcpp->ser3_tx_hunkIndice){
    switch (isrcpp->ser3_tx_handlerIndice){
      case HUNK_HANDLER_A:
        ponyo->hunks[isrcpp->ser3_tx_hunkIndice]->isr_handler_a();
        break;
      case HUNK_HANDLER_B:
        ponyo->hunks[isrcpp->ser3_tx_hunkIndice]->isr_handler_b();
        break;
      case HUNK_HANDLER_NOHANDLER:
        // noooop
        break;
      default:
        // noooop
        break;
    }
  }
}

/* --------------------------- */
/* ---------- SER4 ----------- */
/* --------------------------- */

boolean ISRCPP::register_for_ser4_rx(uint16_t hunkIndice, uint8_t handlerIndice){
  ser4_rx_hunkIndice = hunkIndice;
  ser4_rx_handlerIndice = handlerIndice;
  return true;
}

void SERCOM4_2_Handler(void){
  if(isrcpp->ser4_rx_hunkIndice){
    switch (isrcpp->ser4_rx_handlerIndice){
      case HUNK_HANDLER_A:
        ponyo->hunks[isrcpp->ser4_rx_hunkIndice]->isr_handler_a();
        break;
      case HUNK_HANDLER_B:
        ponyo->hunks[isrcpp->ser4_rx_hunkIndice]->isr_handler_b();
        break;
      case HUNK_HANDLER_NOHANDLER:
        // noooop
        break;
      default:
        // noooop
        break;
    }
  }
}

boolean ISRCPP::register_for_ser4_tx(uint16_t hunkIndice, uint8_t handlerIndice){
  ser4_tx_hunkIndice = hunkIndice;
  ser4_tx_handlerIndice = handlerIndice;
  return true;
}

void SERCOM4_0_Handler(void){
  if(isrcpp->ser4_tx_hunkIndice){
    switch (isrcpp->ser4_tx_handlerIndice){
      case HUNK_HANDLER_A:
        ponyo->hunks[isrcpp->ser4_tx_hunkIndice]->isr_handler_a();
        break;
      case HUNK_HANDLER_B:
        ponyo->hunks[isrcpp->ser4_tx_hunkIndice]->isr_handler_b();
        break;
      case HUNK_HANDLER_NOHANDLER:
        // noooop
        break;
      default:
        // noooop
        break;
    }
  }
}

/* --------------------------- */
/* ---------- SER5 ----------- */
/* --------------------------- */

boolean ISRCPP::register_for_ser5_rx(uint16_t hunkIndice, uint8_t handlerIndice){
  ser5_rx_hunkIndice = hunkIndice;
  ser5_rx_handlerIndice = handlerIndice;
  return true;
}

// need to comment out arduino's instantiation of this,
// platformio keeps these things elsewhere,
// for (windows) this was (the .- proceeding the folder name means it's hidden)
// you'll have to make sure hidden files are exposed in your finder
// users/<me>/.platformio/packages/framework-arduinosam/variants/feature_m4/variant.cpp

void SERCOM5_2_Handler(void){
  if(isrcpp->ser5_rx_hunkIndice){
    switch (isrcpp->ser5_rx_handlerIndice){
      case HUNK_HANDLER_A:
        ponyo->hunks[isrcpp->ser5_rx_hunkIndice]->isr_handler_a();
        break;
      case HUNK_HANDLER_B:
        ponyo->hunks[isrcpp->ser5_rx_hunkIndice]->isr_handler_b();
        break;
      case HUNK_HANDLER_NOHANDLER:
        // noooop
        break;
      default:
        // noooop
        break;
    }
  }
}

boolean ISRCPP::register_for_ser5_tx(uint16_t hunkIndice, uint8_t handlerIndice){
  ser5_tx_hunkIndice = hunkIndice;
  ser5_tx_handlerIndice = handlerIndice;
  return true;
}

void SERCOM5_0_Handler(void){
  if(isrcpp->ser5_tx_hunkIndice){
    switch (isrcpp->ser5_tx_handlerIndice){
      case HUNK_HANDLER_A:
        ponyo->hunks[isrcpp->ser5_tx_hunkIndice]->isr_handler_a();
        break;
      case HUNK_HANDLER_B:
        ponyo->hunks[isrcpp->ser5_tx_hunkIndice]->isr_handler_b();
        break;
      case HUNK_HANDLER_NOHANDLER:
        // noooop
        break;
      default:
        // noooop
        break;
    }
  }
}

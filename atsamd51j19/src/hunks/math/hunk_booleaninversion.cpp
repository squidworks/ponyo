/*
hunks/math/hunk_booleaninversion.h

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#include "hunk_booleaninversion.h"

#define PIN_NUM 12
#define PIN_BM (uint32_t)(1 << PIN_NUM)
#define PIN_PORT PORT->Group[0]

BooleanInversion::BooleanInversion(){
  type_ = "math/booleaninversion";
  // physical input is software output
  numInputs = 1;
  inputs[0] = inp;
  numOutputs = 1;
  outputs[0] = out;
}

void BooleanInversion::init(void){
  // set otp
}

void BooleanInversion::loop(void){
  if(inp->io() && !(out->io())){
    out->put(!(inp->get()));
  }
}

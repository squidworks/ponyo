/*
hunks/math/hunk_adder.h

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/



#include "hunk_adder.h"
#include "trunk.h"

Adder::Adder(){
  // we have some setup to do,
  type_ = "math/adder";
  // inputs,
  inputs[0] = numA;
  inputs[1] = numB;
  numInputs = 2;
  // outputs,
  outputs[0] = numC;
  numOutputs = 1;
}

void Adder::init(void){
  // this has no setup desires
}

void Adder::loop(void){
  // we check flow control: we can only take data
  // from inputs that are occupied, [->io() is true]
  // and we can only put to ports that are not occupied
  if(numA->io() && numB->io() && !(numC->io())){
    uint32_t result = numA->get() + numB->get();
    numC->put(result);
  }
}

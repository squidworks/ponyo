/*
hunks/math/hunk_modulo.cpp

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/



#include "hunk_modulo.h"
#include "trunk.h"

Modulo::Modulo(){
  // pls name self,
  type_ = "math/modulo";
  // inputs,
  inputs[0] = numA;
  numInputs = 1;
  // outputs,
  outputs[0] = numC;
  numOutputs = 1;
  // we'll modulo against this state item,
  states[0] = modulator;
  numStates = 1;
}

void Modulo::init(void){
  // nothing to setup here,
}

void Modulo::loop(void){
  // check flowcontrol conditions, then op
  if(numA->io() && !(numC->io())){
    uint32_t result = numA->get() % modulator->value();
    numC->put(result);
  }
}

// these are virtual functions that we override, so
// they're just indexed by the state's location in states[i] ...
boolean Modulo::stateChangeCallback_0(void){
  // when we define a state change callback,
  // the requested value appears in a swap,
  uint32_t option = modulator->swapValue();
  // if we like it, we can set:
  if(option < 1200){
    modulator->set(option);
    return true;
  } else {
    // or we reject it, and walk on
    return false;
  }
}

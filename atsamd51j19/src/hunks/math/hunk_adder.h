/*
hunks/math/hunk_adder.h

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#ifndef HUNK_ADDER_H_
#define HUNK_ADDER_H_

#include <arduino.h>
#include "hunks/hunks.h"
#include "states/state_uint32.h"
#include "transports/net_uint32.h"

class Adder : public Hunk {
private:
public:
  Adder();
  void init(void);
  void loop(void);
  // in, out
  Inp_uint32* numA = new Inp_uint32("numA");
  Inp_uint32* numB = new Inp_uint32("numB");
  Outp_uint32* numC = new Outp_uint32("numC");
};

#endif

/*
hunks/math/hunk_booleaninversion.h

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#ifndef HUNK_BOOLEAN_INVERSION_H_
#define HUNK_BOOLEAN_INVERSION_H_

#include <arduino.h>
#include "hunks/hunks.h"
#include "transports/net_boolean.h"

class BooleanInversion : public Hunk {
private:
public:
  BooleanInversion();
  //
  void init(void);
  void loop(void);
  //
  Inp_boolean* inp = new Inp_boolean("state");
  Outp_boolean* out = new Outp_boolean("inverted");
};

#endif

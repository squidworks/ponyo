/*
hunks/math/hunk_modulo.h

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#ifndef HUNK_MODULO_H_
#define HUNK_MODULO_H_

#include <arduino.h>
#include "hunks/hunks.h"
#include "states/state_uint32.h"
#include "transports/net_uint32.h"

class Modulo : public Hunk {
private:
public:
  Modulo();
  void init(void);
  void loop(void);
  // in, out
  Inp_uint32* numA = new Inp_uint32("numA");
  Outp_uint32* numC = new Outp_uint32("numC");
  // ah change w/ override callback,
  State_uint32* modulator = new State_uint32("modulator", 64);
  boolean stateChangeCallback_0(void) override;
  // and a plain change
  State_uint32* numAdd = new State_uint32("numAdd", 64);
};

#endif

/*
hunks/driver/hunk_errlight.h

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#ifndef HUNK_DRIVER_ERRLIGHT_H_
#define HUNK_DRIVER_ERRLIGHT_H_

#include <arduino.h>
#include "hunks/hunks.h"
#include "transports/net_boolean.h"
#include "states/state_boolean.h"
#include "debugpins.h"

class Hunk_ErrLight : public Hunk {
private:
public:
  Hunk_ErrLight();
  //
  void init(void);
  void loop(void);
  Inp_boolean* inp = new Inp_boolean("set");
  State_boolean* state = new State_boolean("on?", false);
  boolean stateChangeCallback_0(void) override;
};

#endif

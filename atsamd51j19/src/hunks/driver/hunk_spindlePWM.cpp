/*
hunks/driver/hunk_spindlePWM.cpp

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#include "hunk_spindlePWM.h"

Hunk_SpindlePWM::Hunk_SpindlePWM(){
  type_ = "driver/spindlePWM";
  // physical input is software output
  numInputs = 1;
  inputs[0] = inp;
  numStates = 2;
  states[0] = state;
  states[1] = periodState;
}

void Hunk_SpindlePWM::init(void){
  // always assert start to 1k, so that we boot the esc
  //state->set(SPINDLE_STARTUP_PWM);
  // set pin to output:
  PWM_PORT.DIRSET.reg = PWM_BM;
  //PWM_PORT.OUTSET.reg = PWM_BM;
  PWM_PORT.PINCFG[PWM_PIN].bit.PMUXEN = 1;
  //PWM_PORT.PMUX[PWM_PIN>>1].reg |= PORT_PMUX_PMUXE(0x4);
  PWM_PORT.PMUX[PWM_PIN>>1].reg |= PORT_PMUX_PMUXO(0x4);
  // you'll just have to write your own TC here,
  PWM_TIMER->COUNT16.CTRLA.bit.ENABLE = 0;
  MCLK->APBCMASK.reg |= MCLK_APBCMASK_TC5;
  GCLK->GENCTRL[PWM_GCLKNUM].reg = GCLK_GENCTRL_SRC(GCLK_GENCTRL_SRC_DFLL) | GCLK_GENCTRL_GENEN;
  while(GCLK->SYNCBUSY.reg & GCLK_SYNCBUSY_GENCTRL(PWM_GCLKNUM));
  GCLK->PCHCTRL[PWM_GCLK_ID].reg = GCLK_PCHCTRL_CHEN | GCLK_PCHCTRL_GEN(PWM_GCLKNUM);
  // ok,
  PWM_TIMER->COUNT16.CTRLA.reg = TC_CTRLA_MODE_COUNT16 | TC_CTRLA_PRESCSYNC_PRESC | TC_CTRLA_PRESCALER_DIV16;// | TC_CTRLA_CAPTEN0;
  PWM_TIMER->COUNT16.WAVE.reg = TC_WAVE_WAVEGEN_MPWM;
  // and then,
  PWM_TIMER->COUNT16.CTRLA.bit.ENABLE = 1;

  PWM_TIMER->COUNT16.CCBUF[0].reg = SPINDLE_STARTUP_PWM_PERIOD * PWM_TICKS_PER_US ; // this is the period - this should be ~ 16ms (48000),
  PWM_TIMER->COUNT16.CCBUF[1].reg = SPINDLE_STARTUP_PWM * PWM_TICKS_PER_US; // so, 1ms, and in our world 1ms = 3000 ticks, nice. (measured this, it's just about spot on)
  setPWM(SPINDLE_STARTUP_PWM);
}

void Hunk_SpindlePWM::setPWM(uint32_t val){
  if(val > SPINDLE_UPPERBOUND_PWM){
    state->set(SPINDLE_UPPERBOUND_PWM);
  } else if (val < SPINDLE_LOWERBOUND_PWM) {
    state->set(SPINDLE_LOWERBOUND_PWM);
  } else {
    state->set(val);
  }
  uint32_t current = state->value();
  PWM_TIMER->COUNT16.CCBUF[1].reg = val * PWM_TICKS_PER_US;
  // do the write...
}

boolean Hunk_SpindlePWM::stateChangeCallback_0(void){
  uint32_t requestedVal = state->swapValue();
  setPWM(requestedVal);
  return true;
}

boolean Hunk_SpindlePWM::stateChangeCallback_1(void){
  uint32_t requestedVal = periodState->swapValue();
  PWM_TIMER->COUNT16.CCBUF[0].reg = requestedVal * PWM_TICKS_PER_US;
  periodState->set(requestedVal);
  return true;
}

void Hunk_SpindlePWM::loop(void){
  if(inp->io()){
    setPWM(inp->get());
  }
}

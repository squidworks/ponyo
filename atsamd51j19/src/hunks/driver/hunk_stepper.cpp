/*
hunks/driver/hunk_stepper.cpp

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#include "hunk_stepper.h"
#ifdef BUILD_INCLUDES_HUNK_STEPPER

// the repo you want: https://gitlab.cba.mit.edu/jakeread/atkstepper23/tree/master/embedded/atkstepper23/atkstepper23
// and also, https://gitlab.cba.mit.edu/jakeread/atkstepper17/tree/master/embedded/mkstepper17
// isr device,
#include "isrcpp.h"

#ifdef IS_OLD_STEPPER
  // spi pins...
  #define SPI_PIN_MISO 18
  #define SPI_PIN_MOSI 16
  #define SPI_PIN_SCK 17
  #define SPI_PIN_CSN 19

  // coupla pins
  #define GPIO_PORT PORT->Group[1]
  #define TMC_PIN_EN 11
  #define DIR_PIN 17
  #define STEP_PIN 0

  // it's port a:
  #define SPI_PORT PORT->Group[0]
  // sercom1, BFPP 'DCA'
  #define SPI_SERCOM SERCOM1
  // peripheral A: 0, B: 1, etc.
  #define SPI_PERIPHERAL 2
  #define SPI_GCLK_ID_CORE SERCOM1_GCLK_ID_CORE
  #define SPI_GCLK_NUM 8
#else
  // spi pins...
  #define SPI_PIN_MISO 6
  #define SPI_PIN_MOSI 4
  #define SPI_PIN_SCK 5
  #define SPI_PIN_CSN 7

  // coupla pins
  #define GPIO_PORT PORT->Group[0]
  #define TMC_PIN_EN 22
  #define DIR_PIN 20
  #define STEP_PIN 21

  // it's port a:
  #define SPI_PORT PORT->Group[0]
  // sercom1, BFPP 'DCA'
  #define SPI_SERCOM SERCOM0
  // peripheral A: 0, B: 1, etc.
  #define SPI_PERIPHERAL 3
  #define SPI_GCLK_ID_CORE SERCOM0_GCLK_ID_CORE
  #define SPI_GCLK_NUM 8
#endif

#define SPI_BM_MISO (uint32_t)(1 << SPI_PIN_MISO)
#define SPI_BM_MOSI (uint32_t)(1 << SPI_PIN_MOSI)
#define SPI_BM_SCK (uint32_t)(1 << SPI_PIN_SCK)
#define SPI_BM_CSN (uint32_t)(1 << SPI_PIN_CSN)

#define TMC_BM_EN (uint32_t)(1 << TMC_PIN_EN)
#define DIR_BM (uint32_t)(1 << DIR_PIN)
#define STEP_BM (uint32_t)(1 << STEP_PIN)

#define STEP_TIMER_PERIPHERAL 4

#define TIMER_A_GCLK_NUM 9
#define TIMER_B_GCLK_NUM 10

Stepper::Stepper(){
  // input: int16's -> moves-to-make buffer ... append to end, timer handles
  type_ = "driver/stepper";
  // output: flow controlled int16's out of a ringbuffer ... timer puts in, we remove (interrupt)
  numInputs = 3;
  inputs[0] = msegIn;
  inputs[1] = enableIn;
  inputs[2] = speedIn;
  numOutputs = 2;
  outputs[0] = incOut;
  outputs[1] = sgOut;
  //
  numStates = 4;
  states[0] = enableState;
  states[3] = modeState;
  states[1] = stepsPerUnitState;
  states[2] = cscaleState;
}

// ------------------------------------------------------- //
// ----------------------- SPI for TMC  ------------------ //
// ------------------------------------------------------- //

void init_spi(void){
  // first up, a clock...
  MCLK->APBAMASK.bit.SERCOM1_ = 1;
  // want to startup our hardware,
  // here's the relevant pin setups
  SPI_PORT.DIRCLR.reg = SPI_BM_MISO;
  SPI_PORT.PINCFG[SPI_PIN_MISO].bit.PMUXEN = 1;
  SPI_PORT.DIRSET.reg = SPI_BM_MOSI | SPI_BM_SCK | SPI_BM_CSN;
  SPI_PORT.PINCFG[SPI_PIN_MOSI].bit.PMUXEN = 1;
  SPI_PORT.PINCFG[SPI_PIN_SCK].bit.PMUXEN = 1;
  //  csn,
  SPI_PORT.OUTSET.reg = SPI_BM_CSN;
  // activate the peripheral, ecah has this odd / even business
  if(SPI_PIN_MISO % 2){ // true when odd,
    SPI_PORT.PMUX[SPI_PIN_MISO >> 1].reg |= PORT_PMUX_PMUXO(SPI_PERIPHERAL);
  } else {
    SPI_PORT.PMUX[SPI_PIN_MISO >> 1].reg |= PORT_PMUX_PMUXE(SPI_PERIPHERAL);
  }
  if(SPI_PIN_MOSI % 2){ // true when odd,
    SPI_PORT.PMUX[SPI_PIN_MOSI >> 1].reg |= PORT_PMUX_PMUXO(SPI_PERIPHERAL);
  } else {
    SPI_PORT.PMUX[SPI_PIN_MOSI >> 1].reg |= PORT_PMUX_PMUXE(SPI_PERIPHERAL);
  }
  if(SPI_PIN_SCK % 2){ // true when odd,
    SPI_PORT.PMUX[SPI_PIN_SCK >> 1].reg |= PORT_PMUX_PMUXO(SPI_PERIPHERAL);
  } else {
    SPI_PORT.PMUX[SPI_PIN_SCK >> 1].reg |= PORT_PMUX_PMUXE(SPI_PERIPHERAL);
  }
  // each peripherla gets a clock...
  GCLK->GENCTRL[SPI_GCLK_NUM].reg = GCLK_GENCTRL_SRC(GCLK_GENCTRL_SRC_DFLL) | GCLK_GENCTRL_GENEN;
  while(GCLK->SYNCBUSY.reg & GCLK_SYNCBUSY_GENCTRL(SPI_GCLK_NUM));
  GCLK->PCHCTRL[SPI_GCLK_ID_CORE].reg = GCLK_PCHCTRL_CHEN | GCLK_PCHCTRL_GEN(SPI_GCLK_NUM);
  // the actual sercom init,
  SPI_SERCOM->SPI.CTRLA.bit.ENABLE = 0;
  SPI_SERCOM->SPI.CTRLA.reg |= SERCOM_SPI_CTRLA_MODE(0x3);
  // ok, SERCOM_SPI_CTRLA_CPHA = 1 -> data valid on clock leading edge,
  // SERCOM_SPI_DORD (bit order, 1 for msbfirst, 0 lsbfirst)
  // MODE 0x03 sets both of these up to 1, 1 ... as per TMC262 datasheet...
  SPI_SERCOM->SPI.CTRLA.reg |= SERCOM_SPI_CTRLA_CPOL | SERCOM_SPI_CTRLA_CPHA; // ?
  // bits for data in pin out, data out pin out
  SPI_SERCOM->SPI.CTRLA.reg |= SERCOM_SPI_CTRLA_DIPO(2) | SERCOM_SPI_CTRLA_DOPO(0);
  // f_baud = f_ref / (2 * (BAUD +1)) so BAUD = f_ref / (2 * f_baud) - 1
  // at baud(8), clock pulses are 200ns apart -> about 3mhz, for a 32bit op of 9us
  SPI_SERCOM->SPI.BAUD.reg |= SERCOM_SPI_BAUD_BAUD(8);
  // use hardware slave select, enable rx (but do we?)
  SPI_SERCOM->SPI.CTRLB.reg |= SERCOM_SPI_CTRLB_RXEN;
  // then on,
  SPI_SERCOM->SPI.CTRLA.bit.ENABLE = 1;
}

void write_byte(uint8_t byte){
  while(!(SPI_SERCOM->SPI.INTFLAG.bit.DRE));
  SPI_SERCOM->SPI.DATA.reg = SERCOM_SPI_DATA_DATA(byte);
}

void write_bytes(uint8_t* bytes, size_t len){
  SPI_PORT.OUTCLR.reg = SPI_BM_CSN;
  for(size_t i = 0; i < len; i ++){
    write_byte(bytes[i]);
  }
  while(!(SPI_SERCOM->SPI.INTFLAG.bit.TXC));
  SPI_PORT.OUTSET.reg = SPI_BM_CSN;
}
// actually, these are 20 bit words... 3 bytes...
void write_tmc262(uint32_t word){
  uint8_t bytes[3];
  bytes[0] = word >> 16 | 0b11010000;
  bytes[1] = word >> 8;
  bytes[2] = word;
  write_bytes(bytes, 3);
}

uint32_t write_read_tmc262(uint32_t word){
  uint8_t bytes[3];
  bytes[0] = word >> 16 | 0b11010000;
  bytes[1] = word >> 8;
  bytes[2] = word;
  //
  uint32_t repl = 0;
  uint8_t td = 0;
  SPI_PORT.OUTCLR.reg = SPI_BM_CSN;
  for(size_t i = 0; i < 3; i ++){
    // send when ready,
    while(!(SPI_SERCOM->SPI.INTFLAG.bit.DRE));
    SPI_SERCOM->SPI.DATA.reg = SERCOM_SPI_DATA_DATA(bytes[i]);
    // read in when ready
    while(!(SPI_SERCOM->SPI.INTFLAG.bit.RXC));
    td = SPI_SERCOM->SPI.DATA.reg;
    repl = repl | (td << ((2-i) * 8));
  }
  while(!(SPI_SERCOM->SPI.INTFLAG.bit.TXC));
  SPI_PORT.OUTSET.reg = SPI_BM_CSN;
  return repl;
}

void enable_tmc262(void){
  GPIO_PORT.OUTCLR.reg = TMC_BM_EN;
}

void disable_tmc262(void){
  GPIO_PORT.OUTSET.reg = TMC_BM_EN;
}

void init_gpio(void){
  GPIO_PORT.DIRSET.reg = TMC_BM_EN;
  disable_tmc262();
}

void startup_default(void){
  // these are examples from the TMC datasheet... not used,
  write_tmc262(0x901B4);
  write_tmc262(0x94557);
  write_tmc262(0xD001F);
  write_tmc262(0xEF010);
  write_tmc262(0x00000);
}

// ------------------------------------------------------- //
// --------------------- STATE CHANGES  ------------------ //
// ------------------------------------------------------- //

// for enable,
boolean Stepper::stateChangeCallback_0(void){
  boolean reqState = enableState->swapValue();
  set_enable_state(reqState);
  return true;
}

// for cscale,
boolean Stepper::stateChangeCallback_2(void){
  uint16_t cscale = cscaleState->swapValue();
  if(cscale > 24){
    cscale = 24;
  } else if (cscale < 6) {
    cscale = 6;
  }
  set_cscale(cscale);
  cscaleState->set(cscale);
  return true;
}

// ------------------------------------------------------- //
// --------------------- TMC OPERATION  ------------------ //
// ------------------------------------------------------- //

void Stepper::set_enable_state(boolean newValue){
  if(newValue){
    disable_tmc262();
    startup_tmc262();
    enable_tmc262();
  } else {
    disable_tmc262();
  }
  enableState->set(newValue);
}

void Stepper::set_cscale(uint16_t cscale){
  uint32_t cscale_mask = 0b00000000000000000000000000011111;
  if(cscale > 24){
    cscale = 24;
  } else if (cscale < 6) {
    cscale = 6;
  }
  // assuming this has been written at least once before, gotta wipe that old cscale:
  sgsconf_ = sgsconf_ & (~cscale_mask);
  sgsconf_ = sgsconf_ | (cscale & cscale_mask);
  write_tmc262(sgsconf_);
}

void Stepper::startup_tmc262(void){
  // address, slope control hi and lo to minimum, short to ground protection on, short to gnd timer 3.2us,
  // enable step/dir, sense resistor full scale current voltage is 0.16mv, readback stallguard2 data, reserved
  // this we define as a
  write_tmc262(drvconf_);
  // address, sgfilt off, threshold value, current scaling (5-bit value appended)
  uint32_t sgthresh_mask = 0b00000111111100000000;
  // sgthres... more notes in setup_to_read_sg
  // there's some bug here at the moment: any value that are non-zero here cause motor hiccups and either
  // 0 or 1024 readings from stall guard... probably you are writing to a bad bit somewhere? http://shrugguy.com
  int32_t sgthres_val = 0;
  sgsconf_ = 0b11010000000000000000 | ((sgthres_val << 8) & sgthresh_mask);
  uint16_t initCScale = cscaleState->value();
  set_cscale(initCScale);
  // turning coolstep off
  uint32_t smarten = 0b10100000000000000000;
  write_tmc262(smarten);
  // times, delays, cycle mode
  uint32_t chopconf = 0b10011000001000010011;
  write_tmc262(chopconf);
  // 9th bit is intpol, 8th is dedge, last 4 are microstepping
  // 0101 8
  // 0100 16
  // 0011 32
  // 0010 64
  // 0001 128
  // 0000 256
  uint32_t drvctrl = 0b00000000001100000100;
  write_tmc262(drvctrl);
}

void Stepper::setup_to_read_sg(void){
  // to read, we push DRVCONF register and read ...
  // I'm assuming that this will be a DRVCONF with RDSEL = 01
  // for 10 (0-9) bits of SG data back
  // ok,
  // stallguard reads 10 bits 0-1024: higher values indicate lower mechanical load,
  // and when the value reaches 0 the motor has likely stalled, so we can think of this
  // as mechanical affordance.
  uint32_t sg = write_read_tmc262(drvconf_);
  // the word is just 3 bytes, and we want the top 10 of those //
  sg = (sg >> 14) & 0b00000000000000000000001111111111;
  // put 'er in there
  the_made_sg = sg;
}

// ------------------------------------------------------- //
// ----------------- STEP TIMER OPERATION  --------------- //
// ------------------------------------------------------- //

// timer A runs the ~100hz update loop that we hope to
// close at toplevel, pushing new 16-bit step-counts to make per cycle
// hoping arduino doesn't use these...
void startup_timer_a(void){
  // let's make a clock w/ that xtal:
  OSCCTRL->XOSCCTRL[0].bit.RUNSTDBY = 0;
  OSCCTRL->XOSCCTRL[0].bit.XTALEN = 1;
  // set oscillator current..
  OSCCTRL->XOSCCTRL[0].reg |= OSCCTRL_XOSCCTRL_IMULT(4) | OSCCTRL_XOSCCTRL_IPTAT(3);
  OSCCTRL->XOSCCTRL[0].reg |= OSCCTRL_XOSCCTRL_STARTUP(5);
  OSCCTRL->XOSCCTRL[0].bit.ENALC = 1;
  OSCCTRL->XOSCCTRL[0].bit.ENABLE = 1;
  // wait 4 boot (?) apparently uneccessary
  /*
  while(!(OSCCTRL->STATUS.bit.XOSCRDY0)){
    ERRLIGHT_ON;
  }
  ERRLIGHT_OFF;
  */
  // ok
  TC0->COUNT32.CTRLA.bit.ENABLE = 0;
  TC1->COUNT32.CTRLA.bit.ENABLE = 0;
  // unmask clocks
  MCLK->APBAMASK.reg |= MCLK_APBAMASK_TC0 | MCLK_APBAMASK_TC1;
  // make one clk,
  GCLK->GENCTRL[TIMER_A_GCLK_NUM].reg = GCLK_GENCTRL_SRC(GCLK_GENCTRL_SRC_XOSC0) | GCLK_GENCTRL_GENEN; // GCLK_GENCTRL_SRC_DFLL
  while(GCLK->SYNCBUSY.reg & GCLK_SYNCBUSY_GENCTRL(TIMER_A_GCLK_NUM));
  // ok, clock to these channels...
  GCLK->PCHCTRL[TC0_GCLK_ID].reg = GCLK_PCHCTRL_CHEN | GCLK_PCHCTRL_GEN(TIMER_A_GCLK_NUM);
  GCLK->PCHCTRL[TC1_GCLK_ID].reg = GCLK_PCHCTRL_CHEN | GCLK_PCHCTRL_GEN(TIMER_A_GCLK_NUM);
  // turn them ooon...
  TC0->COUNT32.CTRLA.reg = TC_CTRLA_MODE_COUNT32 | TC_CTRLA_PRESCSYNC_PRESC | TC_CTRLA_PRESCALER_DIV2 | TC_CTRLA_CAPTEN0;
	TC1->COUNT32.CTRLA.reg = TC_CTRLA_MODE_COUNT32 | TC_CTRLA_PRESCSYNC_PRESC | TC_CTRLA_PRESCALER_DIV2 | TC_CTRLA_CAPTEN0;
  // going to set this up to count at some time, we will tune
  // that freq. with
  TC0->COUNT32.WAVE.reg = TC_WAVE_WAVEGEN_MFRQ;
  TC1->COUNT32.WAVE.reg = TC_WAVE_WAVEGEN_MFRQ;
  // allow interrupt to trigger on this event (overflow)
	TC0->COUNT32.INTENSET.bit.MC0 = 1;
  TC0->COUNT32.INTENSET.bit.MC1 = 1;
  // set the period,
  while(TC0->COUNT32.SYNCBUSY.bit.CC0);
	TC0->COUNT32.CC[0].reg = 80; // at DIV2, 240 for 10us ('realtime') (with DFLL), 80 for 10us (with XTAL 16MHZ)
	// enable, sync for enable write
	while(TC0->COUNT32.SYNCBUSY.bit.ENABLE);
	TC0->COUNT32.CTRLA.bit.ENABLE = 1;
  while(TC0->COUNT32.SYNCBUSY.bit.ENABLE);
	TC1->COUNT32.CTRLA.bit.ENABLE = 1;
	// enable the IRQ
	NVIC_EnableIRQ(TC0_IRQn);
}

// https://www.embedded.com/generate-stepper-motor-speed-profiles-in-real-time/
// for timer count c:
//

// runs every 10us,
void Stepper::isr_handler_a(void){
  TC0->COUNT32.INTFLAG.bit.MC0 = 1;
  TC0->COUNT32.INTFLAG.bit.MC1 = 1;
  if(modeState->value()){
    // speed control,
    // do current speed,

  } else {
    // mseg control
    // inc, halt if halting
    _currentMoveTime ++;
    if((_currentMoveTime >= _targetMoveTime) && _insideBlock){//} || _currentStepCount == _targetStepCount){
      ERRLIGHT_TOGGLE;
      PA23_TOGGLE;
      _insideBlock = false;
      if(abs(_currentStepCount) < abs(_targetStepCount)){
        STEP;
        (_dir) ? _currentStepCount ++ : _currentStepCount --;
      }
      if(_currentStepCount != _targetStepCount){
        int64_t error = _currentStepCount - _targetStepCount;
        _cumulativeError += error;
        adebug("bs\t" + String((long)error) + "\t" + String((long)_cumulativeError));
        //adebug("BAD steps, vdelta: " + String(_v_delta, 4) + " counted " + String((long)_currentStepCount) + " for target: " + String((long)_targetStepCount));
        //adebug("WANT vi: " + String(_move_vi) + " and vf " + String(_move_vf) + " and finished with " + String(_v_current));
      } else if (_targetStepCount == 0) {
        //adebug("OK wait, t: " + String((unsigned long)_targetMoveTime));
      } else {
        //adebug("OK move, vdelta, steps: " + String(_v_delta, 4) + " " + String((long)_currentStepCount));
      }
    }
    // if we have new moves available, and aren't inside of one, load it
    if((_rb_tail != _rb_head) && !_insideBlock){
      // inc, wrap pointer
      _rb_tail ++;
      if(_rb_tail >= RB_SIZE) _rb_tail = 0;
      // reset state
      _stepRemainder = 0;
      _currentStepCount = 0;
      _currentMoveTime = 0;
      // load new values,
      _v_current = _rb_blocks[_rb_tail].vi;
      _move_vi = _rb_blocks[_rb_tail].vi;
      _move_v_delta = _rb_blocks[_rb_tail].total_v_delta;
      _v_delta = _rb_blocks[_rb_tail].v_delta;
      _move_vf = _rb_blocks[_rb_tail].vf;
      _targetStepCount = _rb_blocks[_rb_tail].targetStepCount;
      _targetMoveTime = _rb_blocks[_rb_tail].targetMoveTime;
      _dir = _rb_blocks[_rb_tail].dir;
      _dir ? DIR_SET : DIR_CLEAR;
      // now we're in it
      _insideBlock = true;
    }

    if(_insideBlock && _currentStepCount != _targetStepCount){
      // ffast-math _v_current += _v_delta;
      _v_current = _move_vi + (float)((float)_currentMoveTime / (float)_targetMoveTime) * _move_v_delta;
      // could probably ditch the abs() call pretty easily
      _stepsInThisPeriod = abs(_v_current) * _secondsPerTick;
      if(_stepsInThisPeriod > 1){ // catch these,
        error(String(_stepsInThisPeriod));
        adebug("above is the steps in this period, ? 1");
        adebug("_v_current: " + String(_v_current));
        adebug("_v_delta: " + String(_v_delta));
        while(1);
      }
      _stepRemainder += _stepsInThisPeriod;
      if(_stepRemainder > 1){
        STEP;
        (_dir) ? _currentStepCount ++ : _currentStepCount --;
        _stepRemainder -= 1;
      }
    }
  }
}

// #define BIG_STEP_DEBUG

// really want to make some known count of steps, across some known time duration ...
// ratiooooos
void Stepper::loadMove(float duration, float displacement, float v0){
  // check 4 baddies
  if((v0 < 0 && displacement > 0) || (v0 > 0 && displacement < 0)){
    error("bad dir / speed pair at the stepper");
    adebug("displacement:\t" + String(displacement, 6));
    adebug("v0:\t\t" + String(v0, 6));
    return;
  }

  // convert displacements, speeds, by SPU, stop thinking about spu.
  v0 = v0 * (float)(stepsPerUnitState->value());
  displacement = displacement * (float)(stepsPerUnitState->value());

  // what-we-will-set
  float v_delta, vf, total_v_delta;
  int64_t targetStepCount;
  uint64_t targetMoveTime = abs(duration * (float)TICKS_PER_S);
  boolean dir = false;

  if(displacement == 0){
    // not a move,
    v0 = 0; // to be sure ... should just make zero steps each time !
    vf = 0;
    v_delta = 0; // don't increment speed,
    total_v_delta = 0;
    targetStepCount = 0;
  } else {
    // ok, displacement nonzero,
    // let's calculate the projected vf, and our acceleration:
    float twoDoverD = (2.0 * displacement) / duration;
    vf = twoDoverD - v0;
    float accel = (twoDoverD - 2.0 * v0) / duration;
    // set dirction
    (displacement > 0) ? dir = true : dir = false;
    // cruise / not cruise
    if(accel > -0.001 && accel < 0.001){
      v_delta = 0;
      total_v_delta = 0;
    } else {
      // v delta / ticks / s, not move time (?)
      v_delta = accel / (float)TICKS_PER_S;
      total_v_delta = vf - v0;
    }
    // for the new move:
    targetStepCount = (int64_t)displacement;
  }
  // ok, we have a move, increment our move-block counter:
  _rb_head ++;
  if(_rb_head >= RB_SIZE){
    _rb_head = 0;
  }
  // write to it:
  _rb_blocks[_rb_head].vi = v0; // float, steps / s
  _rb_blocks[_rb_head].v_delta = v_delta; // float, steps / s / tick
  _rb_blocks[_rb_head].total_v_delta = total_v_delta;
  _rb_blocks[_rb_head].vf = vf; // also float, steps / s
  _rb_blocks[_rb_head].targetStepCount = targetStepCount; // int64_t
  _rb_blocks[_rb_head].targetMoveTime = targetMoveTime; // uint64_t (count in ticks)
  _rb_blocks[_rb_head].dir = dir; // boolean
  float initialPeriod = abs(v0) * _secondsPerTick;
  #if (0)
    adebug("-----");
    adebug("vi: " + String(v0));
    adebug("vf: " + String(vf))
    adebug("t: " + String(duration));
    adebug("a: " + String(accel));
    adebug("moveTime: " + String((unsigned long)targetMoveTime));
    adebug("v_delta: " + String(v_delta,6));
    adebug("pi: " + String(initialPeriod));
    delay(250);
  #endif
}

void Stepper::init(void){
  init_spi();
  init_gpio();
  //startup_default();
  disable_tmc262();
  // below ~6 here and we get some shakyness: the TMC262 has trouble with small current signals
  // to adjust, set the vsense bit in the driver config
  // '12' here is about 0.4 amp at 24v for a nema17 with 50mOhm sense r, ~ just shy of 10W
  // this keeps motors medium cool enough to run on plastic mounts...
  startup_tmc262();
  //enable_tmc262();
  // ok,
  // setup PA20 (dir), PA21 (step), PA23 (debug);
  PORT->Group[0].DIRSET.reg = (uint32_t)(1 << 20) | (uint32_t)(1 << 21) | (uint32_t)(1 << 23);
  startup_timer_a();
}

// if adding one to the head hits the tail, it's full...
boolean Stepper::rbFull(void){
  uint8_t test = _rb_head + 1;
  if(test >= RB_SIZE){
    test = 0;
  }
  if(test == _rb_tail){
    return true;
  } else {
    return false;
  }
}

void Stepper::loop(void){
  // one at a time to start,
  // HOT WARNING: if you send one while the other
  // is full, u r hooped - hotfix.com
  if(modeState->value()){
    if(speedIn->io()){
      // is RPM ? idk
      targetSpeed = speedIn->get();
      // ok, this isn't done, but here's the ticket that you want to look at
      /*
      // ffast-math _v_current += _v_delta;
      _v_current = _move_vi + (float)((float)_currentMoveTime / (float)_targetMoveTime) * _move_v_delta;
      // could probably ditch the abs() call pretty easily
      _stepsInThisPeriod = abs(_v_current) * _secondsPerTick;
      if(_stepsInThisPeriod > 1){ // catch these,
        error(String(_stepsInThisPeriod));
        adebug("above is the steps in this period, ? 1");
        adebug("_v_current: " + String(_v_current));
        adebug("_v_delta: " + String(_v_delta));
        while(1);
      }
      _stepRemainder += _stepsInThisPeriod;
      if(_stepRemainder > 1){
        STEP;
        (_dir) ? _currentStepCount ++ : _currentStepCount --;
        _stepRemainder -= 1;
      }
      */
    }
  } else {
    if(!rbFull() && msegIn->io()){
      float duration, displacement, v0;
      msegIn->get(&duration, &displacement, &v0);
      loadMove(duration, displacement, v0);
    }
  }
  // handle enable moves:
  if(enableIn->io()){
    set_enable_state(enableIn->get());
  }
}

#endif

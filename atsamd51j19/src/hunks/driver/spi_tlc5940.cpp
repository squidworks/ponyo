/*
hunks/driver/spi_tlc5940.cpp

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/#include "spi_tlc5940.h"

#ifdef BUILD_INCLUDES_HUNK_DRIVER_SPI_TLC5940

// same
SPI_TLC5940::SPI_TLC5940(){
  type_ = "spi_tlc5940";
  numInputs = 1;
  inputs[0] = brightnessIn;
}

void SPI_TLC5940::init(void){
  // setup SPI lines,
}

void SPI_TLC5940::loop(void){
  // get brightess if io and send to spi
}

#endif

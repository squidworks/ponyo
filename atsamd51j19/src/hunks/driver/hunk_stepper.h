/*
hunks/driver/hunk_stepper.h

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#ifndef STEPPER_H_
#define STEPPER_H_

#include "build_config.h"

#ifdef BUILD_INCLUDES_HUNK_STEPPER

#include <arduino.h>
#include "hunks/hunks.h"
#include "transports/net_int32.h"
#include "transports/net_uint32.h"
#include "transports/net_mseg.h"
#include "transports/net_boolean.h"
#include "states/state_uint16.h"
#include "states/state_int32.h"
#include "states/state_boolean.h"

// pin action:
#define STEP PORT->Group[0].OUTTGL.reg = (uint32_t)(1 << 21)
#define DIR_SET PORT->Group[0].OUTSET.reg = (uint32_t)(1 << 20)
#define DIR_CLEAR PORT->Group[0].OUTCLR.reg = (uint32_t)(1 << 20)
#define PA23_TOGGLE PORT->Group[0].OUTTGL.reg = (uint32_t)(1 << 23);

// timer info:
#define TICKS_PER_S 10000 // hz !

// rb
#define RB_SIZE 8

// start here, look at execution. clarify types.
struct block {
  float vi;
  float v_delta;
  float total_v_delta;
  float vf; // we don't operate on this, I just want to check it
  int64_t targetStepCount;
  uint64_t targetMoveTime; // in ticks!
  boolean dir;
};

class Stepper : public Hunk {
private:
  // always:
  float _secondsPerTick = 1 / (float)TICKS_PER_S; // init once in constructor (?)
  // move buffering:
  volatile uint8_t _rb_tail = 0;
  volatile uint8_t _rb_head = 0;
  volatile block _rb_blocks[RB_SIZE];
  // move state
  volatile boolean _insideBlock = false;
  volatile float _stepRemainder;
  volatile int64_t _currentStepCount;
  volatile uint64_t _currentMoveTime;
  volatile float _v_current;
  // scary shit
  volatile uint64_t _cumulativeError = 0;
  // move numbers
  volatile float _v_delta;
  volatile float _move_vf;
  volatile float _move_vi;
  volatile float _move_v_delta;
  volatile int64_t _targetStepCount;
  volatile uint64_t _targetMoveTime; // in ticks!
  volatile boolean _dir;

  // temp, used in interrupt:
  volatile float _stepsWithRemainder;
  volatile float _stepsInThisPeriod;

  // ok, some TMC items,
  const uint32_t drvconf_ = 0b11100000000001010000;
  uint32_t sgsconf_ = 0;
  volatile uint32_t the_made_sg = 1024;
public:
  Stepper();
  // we all:
  void init(void);
  void loop(void);
  // io, want signed ints? for now.. nothing
  Inp_mseg* msegIn = new Inp_mseg("increment");
  Inp_boolean* enableIn = new Inp_boolean("enable");
  Inp_int32* speedIn = new Inp_int32("speed");
  // out,
  Outp_int32* incOut = new Outp_int32("increment");
  Outp_uint32* sgOut = new Outp_uint32("stallGuard");
  // current, enable
  State_boolean* enableState = new State_boolean("enable", false);
  boolean stateChangeCallback_0(void) override;
  State_boolean* modeState = new State_boolean("speed?", false);
  int32_t targetSpeed = 0;
  float currentStep = 0;
  State_int32* stepsPerUnitState = new State_int32("steps per unit", 80);
  State_uint16* cscaleState = new State_uint16("current (6-24)", 16);
  boolean stateChangeCallback_2(void) override;
  // TMC Driver Fn's
  void startup_tmc262(void);
  void set_enable_state(boolean newValue);
  void set_cscale(uint16_t cscale);
  void setup_to_read_sg(void);
  // rb management,
  boolean rbFull(void);
  // Stepping Fn's
  void isr_handler_a(void) override;
  void loadMove(float duration, float displacement, float v0);
  void setStepInterval(void);
};

#endif
#endif

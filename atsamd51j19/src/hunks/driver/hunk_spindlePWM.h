/*
hunks/driver/hunk_spindlePWM.h

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#ifndef HUNK_DRIVER_SPINDLEPWM_H_
#define HUNK_DRIVER_SPINDLEPWM_H_

#include <arduino.h>
#include "hunks/hunks.h"
#include "transports/net_uint32.h"
#include "states/state_uint32.h"
#include "debugpins.h"

#define SPINDLE_LOWERBOUND_PWM 900
#define SPINDLE_UPPERBOUND_PWM 2200
#define SPINDLE_STARTUP_PWM SPINDLE_LOWERBOUND_PWM
#define SPINDLE_STARTUP_PWM_PERIOD 20000 //60000 // for 20ms / 50hz
#define PWM_TICKS_PER_US 3
// for simple TCs, output to WO[1] - ! pwm period uses CC0 (oddly)
// and the actual capture / compare output is on to CC1 !
#define PWM_PORT PORT->Group[1]
#define PWM_PIN 11
#define PWM_BM (uint32_t)(1 << PWM_PIN)

// atm we are on PB10, TC5-0
#define PWM_TIMER TC5
#define PWM_GCLKNUM 11
#define PWM_GCLK_ID TC5_GCLK_ID

class Hunk_SpindlePWM : public Hunk {
public:
  Hunk_SpindlePWM();
  //
  void init(void);
  void loop(void);
  void setPWM(uint32_t val);
  Inp_uint32* inp = new Inp_uint32("pwm");
  State_uint32* state = new State_uint32("pwm", SPINDLE_STARTUP_PWM);
  State_uint32* periodState = new State_uint32("pwm period", SPINDLE_STARTUP_PWM_PERIOD);
  boolean stateChangeCallback_0(void) override;
  boolean stateChangeCallback_1(void) override;
};

#endif

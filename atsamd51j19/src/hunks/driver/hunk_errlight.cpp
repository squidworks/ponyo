/*
hunks/driver/hunk_errlight.cpp

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#include "hunk_errlight.h"

Hunk_ErrLight::Hunk_ErrLight(){
  type_ = "driver/errlight";
  // physical input is software output
  numInputs = 1;
  inputs[0] = inp;
  numStates = 1;
  states[0] = state;
}

void Hunk_ErrLight::init(void){
  // set otp
  ERRLIGHT_SETUP;
}

boolean Hunk_ErrLight::stateChangeCallback_0(void){
  boolean requestedVal = state->swapValue();
  if(requestedVal){
    ERRLIGHT_ON;
  } else {
    ERRLIGHT_OFF;
  }
  state->set(requestedVal);
  return true;
}

void Hunk_ErrLight::loop(void){
  if(inp->io()){
    if(inp->get()){
      ERRLIGHT_ON;
    } else {
      ERRLIGHT_OFF;
    }
  }
}

/*
hunks/driver/spi_tlc5940.h

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#ifndef SPI_TLC5940_H_
#define SPI_TLC5940_H_

#include "build_config.h"

#ifdef BUILD_INCLUDES_HUNK_DRIVER_SPI_TLC5940

#include <arduino.h>
#include "hunks/hunks.h"
#include "transports/net_uint32.h"

class SPI_TLC5940 : public Hunk {
public:
  SPI_TLC5940();
  void init(void);
  void loop(void);
  // brightness, for now
  Inp_uint32* brightnessIn = new Inp_uint32("brightness");
};

#endif
#endif

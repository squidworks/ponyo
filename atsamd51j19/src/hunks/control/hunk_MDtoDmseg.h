/*
hunks/control/hunk_MDtoDmseg.h

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#ifndef MDTODMSEG_H_
#define MDTODMSEG_H_

#include "build_config.h"

#ifdef BUILD_INCLUDES_HUNK_MDTODMSEG

#include <arduino.h>
#include "hunks/hunks.h"
#include "transports/net_MDmseg.h"
#include "transports/net_mseg.h"
#include "states/state_uint32.h"

class MDtoDmseg : public Hunk {
private:
  // input
  float _p0[3];
  float _p1[3];
  float _t, _v0, _a;
  // gen
  float _d[3];
  uint32_t _tb, _spu;
  // shipments
  Outp_mseg* _msegs[3];
public:
  MDtoDmseg();
  void init(void);
  void loop(void);
  // in segment, out discretes
  Inp_MDmseg* MDmsegIn = new Inp_MDmseg("motionSegment");
  Outp_mseg* msegXOut = new Outp_mseg("dmSegX");
  Outp_mseg* msegYOut = new Outp_mseg("dmSegY");
  Outp_mseg* msegZOut = new Outp_mseg("dmSegZ");
  // check all,
  boolean allClear(void);
  // what's the timebase?
  // State_uint32* timeBase = new State_uint32("ticks/s", 6000000);
  // State_uint32* stepsPerUnit = new State_uint32("steps/u", 200);
};

#endif
#endif

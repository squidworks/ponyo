/*
hunks/control/hunk_saturnjog.h

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#ifndef SATURNJOG_H_
#define SATURNJOG_H_

#include <arduino.h>
#include "transports/net_boolean.h"
#include "transports/net_int32.h"
#include "states/state_boolean.h"

class SaturnJog : public Hunk {
private:
  // getNextDesire();

public:
  SaturnJog();
  //
  void init(void);
  void loop(void);
  // to test, let's start with xpositive pressure, xnegative ... each dof will have p, n sides
  Inp_boolean* xppIn = new Inp_boolean("xPositive");
  Inp_boolean* xpnIn = new Inp_boolean("xNegative");
  // output is one stream of increments:
  Outp_int32* xStream = new Outp_int32("xStream");
  // and the state: is this running ?
  State_boolean* isRunning = new State_boolean("isRunning", false);
  boolean stateChangeCallback_0(void) override;
  // ok,
};

#endif

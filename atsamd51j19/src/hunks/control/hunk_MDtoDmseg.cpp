/*
hunks/control/hunk_MDtoDmseg.cpp

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#include "hunk_MDtoDmseg.h"

#ifdef BUILD_INCLUDES_HUNK_MDTODMSEG

MDtoDmseg::MDtoDmseg(){
  type_ = "control/MDtoDmseg";
  numInputs = 1;
  inputs[0] = MDmsegIn;
  numOutputs = 3;
  outputs[0] = msegXOut;
  outputs[1] = msegYOut;
  outputs[2] = msegZOut;
  _msegs[0] = msegXOut;
  _msegs[1] = msegYOut;
  _msegs[2] = msegZOut;
  /*
  numStates = 2;
  states[0] = timeBase;
  states[1] = stepsPerUnit;
  */
}

/*

this should have some rules, commeasurate with our ability to step:
minspeed, maxspeed (steps/s) - this has to do with downstream time length,

*/

boolean MDtoDmseg::allClear(void){
  return (!(msegXOut->io()) && !(msegYOut->io()) && !(msegZOut->io()));
}

void MDtoDmseg::init(void){
  //
}

// #define BIG_MDSEG_DEBUG

void MDtoDmseg::loop(void){
  // properly, should buffer these moves,
  // and then, when all 3 are clear, ship
  if(MDmsegIn->io() && allClear()){
    // for each output, we need: duration (same all up(uint32)), steps (int32), interval_0 (uint32)
    // indeed, we donot need a: have distance to make, time to make it in, and a v0
    MDmsegIn->get(_p0, _p1, &_t, &_v0, &_a);
    // downstream steppers will recv
    // (1) duration of the move in s (float)
    // (2) the size of their move in units (float)
    // (3) their initial velocity, in units/s (float)
    // from this, they'll calculate and execute the constant acceleration necessary to make the complete steps in time

    // calculate per-axis move distance, and total
    float distance = 0;
    for(uint8_t i = 0; i < 3; i ++){
      _d[i] = _p1[i] - _p0[i];
      distance += _d[i] * _d[i];
    }
    distance = sqrt(distance);

    // debug... seems like we are missing some moves ? try calcing vf? log moves in js?
    #ifdef BIG_MDSEG_DEBUG
      adebug("------------------------------- MDtoD:");
      adebug("p1:\t" + String(_p1[0]) + ", " + String(_p1[1]) + ", " + String(_p1[2]));
      adebug("p0:\t" + String(_p0[0]) + ", " + String(_p0[1]) + ", " + String(_p0[2]));
      adebug("distance:\t" + String(distance));
      adebug("t:\t\t" + String(_t, 6));
      adebug("v0:\t" + String(_v0, 6));
      adebug("a:\t\t" + String(_a, 6));
    #endif

    // calculate per axis:
    for(uint8_t i = 0; i < 3; i ++){
      // how much of the move is this axis:
      float ratio = _d[i] / distance;
      float axisV0 = _v0 * ratio;
      // that it?
      _msegs[i]->put(_t, _d[i], axisV0);
    }
    #ifdef BIG_MDSEG_DEBUG
      delay(100);
    #endif
  }
}

#endif

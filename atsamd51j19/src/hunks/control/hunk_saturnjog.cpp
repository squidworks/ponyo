/*
hunks/control/hunk_saturnjog.cpp

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#include "hunk_saturnjog.h"

SaturnJog::SaturnJog(){
  type_ = "saturnjog";
  //
  numInputs = 2;
  inputs[0] = xppIn;
  inputs[1] = xpnIn;
  numOutputs = 1;
  outputs[0] = xStream;
  numStates = 1;
  states[0] = isRunning;
}

void SaturnJog::init(void){
  // always starts off:
  isRunning->set(false);
}

boolean SaturnJog::stateChangeCallback_0(void){
  // get from isRunning->swapValue();
  return false;
}

void SaturnJog::loop(void){
  // if we're running, and clear downstream, ship next desires
  // based on state we keep (as if all shipments happened immediately)
  // should we also ship some position information back ... as often as possible ?
  // trust that upstream flowcontrol won't f us ?
}

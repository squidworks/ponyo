/*
hunks/hunk_pin.cpp

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#include "hunk_pin.h"

Pin::Pin(){
  type_ = "pin";
}

void Pin::init(void){
  debug("PIN INIT");
  PIN_PORT.DIRSET.reg = PIN_BM;
  PIN_PORT.OUTCLR.reg = PIN_BM;
}

void Pin::loop(void){
  PIN_PORT.OUTTGL.reg = PIN_BM;
}

/*
hunks/hunk_int32_debugger.h

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#ifndef INT32_DEBUG_H_
#define INT32_DEBUG_H_

#include <arduino.h>
#include "hunks/hunks.h"
#include "transports/net_int32.h"

class Int32_Debugger : public Hunk {
private:
  //
public:
  Int32_Debugger();
  void init(void);
  void loop(void);
  Inp_int32* numIn = new Inp_int32("numin");
};

#endif

/*
hunks/hunk_link.h

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#ifndef LINK_H_
#define LINK_H_

#if(1)

#include <arduino.h>
#include "hunks/hunks.h"
#include "transports/multimessagedevice.h"
#include "transports/net_byteArray.h"
#include "transports/net_uint32.h"
#include "transports/net_int32.h"
#include "transports/net_boolean.h"
#include "transports/net_MDmseg.h"
#include "transports/net_mseg.h"
#include "states/state_string.h"
#include "states/state_boolean.h"
#include "states/state_uint16.h"

#define LINK_DATA_WIDTH 512
#define LINK_BIG_DATA_WIDTH 1024
#define LINK_BUFFER_LEN 2048

#define NKS_INSIDE_NAME 0
#define NKS_INSIDE_TYPE 1
#define NKS_OUTSIDE 2

// , mA (byteArray), sA (int32), mB (byteArray), sB (int32), mD (byteArray), sD (int32), mE (byteArray), sE (int32)
// , s (int32)

class Link : public Hunk {
private:
  // in and out buffers, probably
  unsigned char inBuffer_[LINK_BUFFER_LEN];
  // on bigboi, donot want to overrun
  unsigned char outBuffer_[LINK_BUFFER_LEN];
  // mmu? mmd.
  MultiMessageDevice* mmd_ = new MultiMessageDevice();
  // state for:
  boolean isOpening_ = false;
  // we track acks,
  boolean outputNeedsAck[MAX_TRANSPORT_MULTIPLEX];
  boolean inputIsClearUpstream[MAX_TRANSPORT_MULTIPLEX];

public:
  Link();
  // every1 inits
  void init(void);
  void loop(void);
  // state objs
  // State_byteArray*
  // inpts / outputs 0, 0,
  Inp_byteArray* dtIn = new Inp_byteArray("data", LINK_BIG_DATA_WIDTH);
  Outp_byteArray* dtOut = new Outp_byteArray("data", LINK_BIG_DATA_WIDTH);
  // ok, we startup with (default) this list ...
  State_boolean* isActive = new State_boolean("isActive", false);
  State_uint16* otherLink = new State_uint16("otherLink", 0);
  boolean stateChangeCallback_0(void) override;
  boolean stateChangeCallback_1(void) override;
  // and these,
  State_string* inputList = new State_string("inputList", "mgrMsgs (byteArray)", 512);// train (int32)", 1024);
  State_string* outputList = new State_string("outputList","mgrMsgs (byteArray)", 512);// train (int32)", 1024);
  // with callbacks...
  boolean stateChangeCallback_2(void) override;
  boolean stateChangeCallback_3(void) override;
  // mostly, for doing
  boolean makeInputsFrom(String list);
  boolean makeOutputsFrom(String list);
  // ok, to init
  boolean ack(uint8_t indice);
  void openup(boolean reqResponse);
  // boolean newInputList(boolean startup);
};

// unrelated fn?

#endif
#endif

/*
hunks/hunks.h

code modules for virtual dataflow environment 

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#ifndef HUNKS_H_
#define HUNKS_H_

// bc cyclic include... so, ptrs only
class Input;
class Output;
class State;

#include <arduino.h>
// hunk, or the interface ... is the object the manager
// will bump over
// mostly, just a list of pointers
// an a 'purely virtual' class
#include "transports/nets.h"
#include "states/states.h"


#define HUNK_HANDLER_A 0
#define HUNK_HANDLER_B 1
// donot expect > this many handlers, so
#define HUNK_HANDLER_NOHANDLER 244

#define HUNK_MAX_OUTPUTS 16
#define HUNK_MAX_INPUTS 16
#define HUNK_MAX_STATES 8

class Hunk{
private:

public:
  virtual ~Hunk() {};
  String name_;
  String type_;
  uint16_t indice_;
  // manager calls these
  virtual void init(void) = 0;
  virtual void loop(void) = 0;
  // everyone's got (pointers to) some
  Output* outputs[HUNK_MAX_OUTPUTS];
  uint16_t numOutputs = 0;
  // inputs
  Input* inputs[HUNK_MAX_INPUTS];
  uint16_t numInputs = 0;
  // no state yet
  // for state updates, want also virtual table (?) base -> typed -> instance (in hunk)
  State* states[HUNK_MAX_STATES];
  uint16_t numStates = 0;
  // one manager handle,
  boolean tryStateChange(uint8_t stInd, unsigned char* msg, uint16_t mp);
  // hunks can override these to get callback hooks ... yes, 16 repeated fns, I know...
  // call me when you have a hunk w/16 state elements, and no better solution for this
  virtual boolean stateChangeCallback_0(void);
  virtual boolean stateChangeCallback_1(void);
  virtual boolean stateChangeCallback_2(void);
  virtual boolean stateChangeCallback_3(void);
  virtual boolean stateChangeCallback_4(void);
  virtual boolean stateChangeCallback_5(void);
  virtual boolean stateChangeCallback_6(void);
  virtual boolean stateChangeCallback_7(void);
  // ah utility for ponyo,
  boolean setChildrenIndices();
  // interrupts ... default to no-op
  virtual void isr_handler_a(void){};
  virtual void isr_handler_b(void){};
};

#endif

/*
hunks/hunk_loadcell.cpp

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#include "hunk_loadcell.h"
#include "trunk.h"

// D5: PA16: DCA-0: B6
// D6: PA18: DCA-2: A5
#define LOADCELL_DOUT_PIN 5
#define LOADCELL_SCK_PIN 6
#define LOADCELL_OFFSET 50682624
#define LOADCELL_DIVIDER 5895655

Loadcell::Loadcell(){
  type_ = "loadcell";
  numInputs = 2;
  inputs[0] = takeTrig;
  inputs[1] = tareTrig;
  numOutputs = 1;
  outputs[0] = outp;
}

void Loadcell::init(void){
  cell.begin(LOADCELL_DOUT_PIN, LOADCELL_SCK_PIN);
  cell.set_scale(LOADCELL_DIVIDER);
  cell.set_offset(LOADCELL_OFFSET);
  cell.tare();
}

void Loadcell::loop(void){
  if(tareTrig->io()){
    tareTrig->get();
    cell.tare();
  }
  if(takeTrig->io() && !(outp->io())){
    takeTrig->get();
    // this readin is synchronous, and slow af !
    debug("loadcell reading:");
    int32_t val = cell.get_value(10);
    //debug(String(val));
    outp->put(val);
  }
}

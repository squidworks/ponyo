/*
hunks/hunk_loadcell.h

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#ifndef HUNK_LOADCELL_H_
#define HUNK_LOADCELL_H_

#include <arduino.h>
#include "hunks/hunks.h"
#include "transports/net_int32.h"
#include "transports/net_boolean.h"
#include "friends/HX711.h"

class Loadcell : public Hunk {
private:
  HX711 cell;
  // ?
public:
  Loadcell();
  //
  uint8_t cnt;
  void init(void);
  void loop(void);
  // io
  //Inp_int32* trig = new Inp_int32("trigger");
  Outp_int32* outp = new Outp_int32("24bit");
  Inp_boolean* takeTrig = new Inp_boolean("read");
  Inp_boolean* tareTrig = new Inp_boolean("tare");
  //
};

#endif

/*
hunks/comm/hunk_cobserial_a.cpp

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#include "hunk_cobserial_a.h"
#include "isrcpp.h"
// TX: PA22, SER3-0 or SER5-1.. tx is always on -0, so 3-0, peripheral C
// RX: PA20, SER3-2 or SER5-2.. rx can go anywhere, so its 3-2, peripheral D

#define COBA_SERCOM SERCOM1
#define COBA_SERCOM_CLK SERCOM1_GCLK_ID_CORE
#define COBA_COMPORT PORT->Group[0]
#define COBA_TXPIN 16 // x-0
#define COBA_RXPIN 18 // x-2
#define COBA_TXPERIPHERAL PERIPHERAL_C
#define COBA_RXPERIPHERAL PERIPHERAL_C
#define COBA_LIGHT_PIN 19 // status, x-3
#define COBA_LIGHT_PORT PORT->Group[0]
#define COBA_TE_PIN 17 // TE, x-1
#define COBA_TE_PORT PORT->Group[0]

COBSerial_A::COBSerial_A(){
  // hunk stuff,
  type_ = "comm/COBSerial_A";
  numInputs = 1;
  inputs[0] = inChars;
  numOutputs = 1;
  outputs[0] = outChars;
  // set our local vals up,
  com_usart = &(COBA_SERCOM->USART);
  com_port = &(COBA_COMPORT);
  // ok,
  com_tx_pin = COBA_TXPIN;
  com_tx_peripheral = COBA_TXPERIPHERAL;
  com_rx_pin = COBA_RXPIN;
  com_rx_peripheral = COBA_RXPERIPHERAL;
  // and
  gclknum_pick = 7;
  gclkid_core = COBA_SERCOM_CLK;
  // lights
  light_pin = COBA_LIGHT_PIN; // status
  light_port = &(COBA_LIGHT_PORT);
  te_pin = COBA_TE_PIN; // termination resitor en/disable
  te_port = &(COBA_TE_PORT);
}

void COBSerial_A::register_for_interrupts(void){
  isrcpp->register_for_ser1_rx(indice_, HUNK_HANDLER_A);
  isrcpp->register_for_ser1_tx(indice_, HUNK_HANDLER_B);
  NVIC_EnableIRQ(SERCOM1_2_IRQn);
  NVIC_EnableIRQ(SERCOM1_0_IRQn);
}

void COBSerial_A::unmask_clocks(void){
  MCLK->APBAMASK.bit.SERCOM1_ = 1;
}

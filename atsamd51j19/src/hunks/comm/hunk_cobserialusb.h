/*
hunks/comm/hunk_cobserialusb.h

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#ifndef COBSERIALUSB_H_
#define COBSERIALUSB_H_

#include <arduino.h>
#include "hunks/hunks.h"
#include "transports/net_byteArray.h"

#define COBS_BUFFER_LEN 1024

class COBS_USB : public Hunk{
private:
  uint8_t localbytesin_[COBS_BUFFER_LEN];
  size_t bp_ = 0;
  uint8_t localbytesdecobd_[COBS_BUFFER_LEN];
  uint8_t localbytesout_[COBS_BUFFER_LEN];
  uint16_t dbg_counter = 0;

public:
  COBS_USB();
  // every1 has 1
  void init(void);
  void loop(void);
  // io
  Inp_byteArray* inChars = new Inp_byteArray("data", COBS_BUFFER_LEN);
  Outp_byteArray* outChars = new Outp_byteArray("data", COBS_BUFFER_LEN);
};

#endif

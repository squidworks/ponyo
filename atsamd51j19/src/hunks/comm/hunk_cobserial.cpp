/*
hunks/comm/hunk_cobserial.cpp

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#include "hunk_cobserial.h"
// isr device,
#include "isrcpp.h"
#include "utils/cobs.h"

// for the uartport example from earlier work,
// https://gitlab.cba.mit.edu/jakeread/atkstepper17/tree/master/embedded/mkstepper17

#define SET_LIGHT_ON light_port->OUTCLR.reg = light_bm
#define WRITE_LIGHT_OFF light_port->OUTSET.reg = light_bm

#define SET_TXLIGHT_ON SET_LIGHT_ON
#define SET_TXLIGHT_OFF light_timesince_tx = 0
#define SET_RXLIGHT_ON SET_LIGHT_ON
#define SET_RXLIGHT_OFF light_timesince_rx = 0

COBSerial::COBSerial(){
  // children do this...
}

void COBSerial::init(void){
  // not the most important, but lights are first:
  light_bm = 1 << light_pin;
  te_bm = 1 << te_pin;
  // turn the light *off*
  light_port->DIRSET.reg = light_bm;
  light_port->OUTSET.reg = light_bm;
  // for now, all point to point, enable the termination resistor
  te_port->DIRSET.reg = te_bm;
  te_port->OUTSET.reg = te_bm;
  // our local business 1st,
  for(uint8_t i = 0; i < HCBSRJ45_NETBUFF_NUM; i ++){
    netBufLen_[i] = 0;
  }
  // setup some bms
  com_tx_bm = (uint32_t)(1 << com_tx_pin);
  com_rx_bm = (uint32_t)(1 << com_rx_pin);
  // virtual, children ...
  register_for_interrupts();
  // rx pin setup
  com_port->DIRCLR.reg = com_rx_bm;
  com_port->PINCFG[com_rx_pin].bit.PMUXEN = 1;
  if(com_rx_pin % 2){
    com_port->PMUX[com_rx_pin >> 1].reg |= PORT_PMUX_PMUXO(com_rx_peripheral);
  } else {
    com_port->PMUX[com_rx_pin >> 1].reg |= PORT_PMUX_PMUXE(com_rx_peripheral);
  }
  // tx
  com_port->DIRCLR.reg = com_tx_bm;
  com_port->PINCFG[com_tx_pin].bit.PMUXEN = 1;
  if(com_tx_pin % 2){
    com_port->PMUX[com_tx_pin >> 1].reg |= PORT_PMUX_PMUXO(com_tx_peripheral);
  } else {
    com_port->PMUX[com_tx_pin >> 1].reg |= PORT_PMUX_PMUXE(com_tx_peripheral);
  }
  // ok, clocks, first line au manuel
  unmask_clocks();
  GCLK->GENCTRL[gclknum_pick].reg = GCLK_GENCTRL_SRC(GCLK_GENCTRL_SRC_DFLL) | GCLK_GENCTRL_GENEN;
  while(GCLK->SYNCBUSY.reg & GCLK_SYNCBUSY_GENCTRL(gclknum_pick));
	GCLK->PCHCTRL[gclkid_core].reg = GCLK_PCHCTRL_CHEN | GCLK_PCHCTRL_GEN(gclknum_pick);
  // then, sercom
  while(com_usart->SYNCBUSY.bit.ENABLE);
  com_usart->CTRLA.bit.ENABLE = 0;
  while(com_usart->SYNCBUSY.bit.SWRST);
  com_usart->CTRLA.bit.SWRST = 1;
  while(com_usart->SYNCBUSY.bit.SWRST);
  while(com_usart->SYNCBUSY.bit.SWRST || com_usart->SYNCBUSY.bit.ENABLE);
  // ok, well
  com_usart->CTRLA.reg = SERCOM_USART_CTRLA_MODE(1) | SERCOM_USART_CTRLA_DORD | SERCOM_USART_CTRLA_RXPO(com_rx_po) | SERCOM_USART_CTRLA_TXPO(0);
  while(com_usart->SYNCBUSY.bit.CTRLB);
  com_usart->CTRLB.reg = SERCOM_USART_CTRLB_RXEN | SERCOM_USART_CTRLB_TXEN | SERCOM_USART_CTRLB_CHSIZE(0);
  /*
	BAUD = 65536*(1-S*(fBAUD/fref))
	where S is samples per bit, 16 for async uart
	where fBAUD is the rate that you want
	where fref is the peripheral clock from GCLK, in this case (and most) 48MHz (?)
	*/
  com_usart->BAUD.reg = COM_BAUD_VAL;
  // and finally, a kickoff
  while(com_usart->SYNCBUSY.bit.ENABLE);
  com_usart->CTRLA.bit.ENABLE = 1;
  // not yet, but:
  com_usart->INTENSET.bit.RXC = 1;
}

/*

ok, the kritical link ...

we pull off of our input, writing to an output buffer. we COB those, and tx them.
we only pull off of the input when we have a clear output buffer!

when we get a byte, we write it to our input buffer, which has to be a ringbuffer, pretty sure
if the byte is a zero, that's a packet, memcpy that to ... an mmd ?

*/

void COBSerial::loop(void){
  // turn off like apres 100 loops ? we have LOOP_TICKS_PER_BLINK - is in sys toplevel
  if((light_timesince_tx > LOOP_TICKS_PER_BLINK) && (light_timesince_rx > LOOP_TICKS_PER_BLINK)){
    // noop
  } else {
    light_timesince_tx ++;
    light_timesince_rx ++;
    // catch on transitions,
    if((light_timesince_tx > LOOP_TICKS_PER_BLINK) && (light_timesince_rx > LOOP_TICKS_PER_BLINK)) {
      WRITE_LIGHT_OFF;
    }
  }

  // hw test, for polling...
  //if(com_usart->INTFLAG.bit.DRE){
  //  com_usart->DATA.reg = 87;
  //};
  // or,

  if(netBufReadAt_ != netBufWriteTo_){
    // gotem
    if(!(outChars->io())){
      //we want to de-cob this str8 into the output, for speed
      uint16_t len = cobs_decode(netBuf_[netBufReadAt_], netBufLen_[netBufReadAt_], (uint8_t*)(outChars->chunk->data));
      debug("decob, len: " + String(len));
      //uint16_t len = netBufLen_[netBufReadAt_];
      if(len){
        /*
        for(uint8_t i = 0; i < len; i ++){
          adbs();
          adbn("bt-raw:\t" + String(netBuf_[netBufReadAt_][i]) + "\t");
          adbf(String(netBuf_[netBufReadAt_][i], BIN));
          //adbs();
          //adbn("bt-decob: ");
          //adbf(String(((uint8_t*)(outChars->chunk->data))[i]));
        } */
        // the -1 rm's the trailing zero,
        outChars->manualSetIo(len - 1);
      } else {
        error("RJ45 pulls 0 len on decob...");
      }
      // done with that, important to write its length back to zero,
      netBufLen_[netBufReadAt_] = 0;
      // clear,
      SET_RXLIGHT_OFF;
      // now we increment,
      netBufReadAt_ ++;
      if(netBufReadAt_ >= HCBSRJ45_NETBUFF_NUM){
        netBufReadAt_ = 0;
      }
    } else {
      // we're sweatin' but not panicking
    }
  }

  if(inChars->io() && bytesOutEndPtr_ == 0){
    // yes, we are using cycles for this
    SET_TXLIGHT_ON;
    // since it saves a shetload of memcpy, we encode str8 out of the input
    uint8_t wi = inChars->getNextWireIndice();
    bytesOutEndPtr_ = cobs_encode((uint8_t*)(inChars->connections[wi]->op->chunk->data), inChars->connections[wi]->op->chunk->len, bytesOutToPhy_);
    // that means we have to set these things au manuel:
    inChars->manualReadFrom(wi);
    bytesOutWritePtr_ = 0;
    com_usart->INTENSET.bit.DRE = 1;
    // bytesOutWritePtr_ = 0;
    // and set this up to send via the txint
    // com_usart->INTENSET.bit.DRE = 1;
    // lay 'em down, synchronous like
    /*
    for(uint16_t i = 0; i < bytesOutEndPtr_; i ++){
      while(!(com_usart->INTFLAG.bit.DRE));
      com_usart->DATA.reg = bytesOutToPhy_[i];
      //adbs();
      //adbn("out:\t" + String(bytesOutToPhy_[i]) + "\t");
      //adbf(String(bytesOutToPhy_[i], BIN));
    }
    bytesOutEndPtr_ = 0;
    */
    // or do,
  }
}

// this uses a lot of trust w/r/t the other link not f'ing us
// or maybe we don't have to rewrite these ?
void COBSerial::isr_handler_a(void){
  // always set back up,
  SET_RXLIGHT_ON;
  // cleared by reading out,
  uint8_t data = com_usart->DATA.reg;
  if(data == 0){
    // we gucc, write it & wrip it
    netBuf_[netBufWriteTo_][netBufLen_[netBufWriteTo_] ++] = 0;
    netBufWriteTo_ ++;
    if(netBufWriteTo_ >= HCBSRJ45_NETBUFF_NUM){
      netBufWriteTo_ = 0;
    }
  } else {
    netBuf_[netBufWriteTo_][netBufLen_[netBufWriteTo_] ++] = data;
  }
}

void COBSerial::isr_handler_b(void){
  // atm we never turn this on,
  com_usart->DATA.reg = bytesOutToPhy_[bytesOutWritePtr_]; //data;
  bytesOutWritePtr_ ++;
  if(bytesOutWritePtr_ >= bytesOutEndPtr_){
    com_usart->INTENCLR.reg = SERCOM_USART_INTENCLR_DRE;
    bytesOutEndPtr_ = 0;
    // off,
    SET_TXLIGHT_OFF;
  }
}

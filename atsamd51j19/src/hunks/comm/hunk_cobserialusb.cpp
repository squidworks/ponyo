/*
hunks/comm/hunk_cobserialusb.cpp

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#include "hunk_cobserialusb.h"
#include "trunk.h"
#include "utils/cobs.h"

COBS_USB::COBS_USB(){
  type_ = "comm/COBS_USB";
  // need to update these
  numInputs = 1;
  inputs[0] = inChars;

  numOutputs = 1;
  outputs[0] = outChars;
}

void COBS_USB::init(void){
  // relies on a statement in setup() to init serial... for the boot case
  /*
  if(!Serial){
    Serial.begin(9600);
    while(!Serial){
      ERRLIGHT_ON;
    };
    ERRLIGHT_OFF;
  }
  */
}

void COBS_USB::loop(void){
  // while the trunk lets us escape messages
  // all over the place,
  // this is (or should be) our only entry point
  // this is, also, perty slow... once per loop we check for 1 byte?
  // cmon folks
  if(!(outChars->io())){
    // ok, worth note-taking here:
    /*
    this was previously set up w/o the while(Serial.available()) loop,
    which is, by the way, still a slow implementation of what could be wickedquick via native usb / dma etc ! however !
    when I was sipping just one byte at a time, occasionally would see a zero appear in the middle of an (apparently)
    COBS'd message. I changed this to read all of the bytes available, until a zero appears
    this seems to have fixed the bug, but there's probably still something in here lurking...
    */
    if(Serial.available()){
      while(Serial.available()){
        localbytesin_[bp_] = Serial.read();
        // to check ...
        dbg_counter ++;
        if(localbytesin_[bp_] == 0){
          // de-cobs 1st !
          uint16_t dcl = cobs_decode(localbytesin_, bp_, localbytesdecobd_);
          outChars->put(localbytesdecobd_, dcl);
          // wrap buffer to start,
          bp_ = 0;
          break;
        } else {
          bp_ ++;
          if(bp_ >= COBS_BUFFER_LEN){
            error("COBS packet size larger than buffer");
            dbs();
            dbn("len: ");
            dbf(COBS_BUFFER_LEN);
            bp_ = 0;
          }
        }
      }
    }
  }

  if(inChars->io()){
    size_t len = inChars->get(localbytesin_);
    /*
    adbs();
    adbn("COBSerial len: ");
    adbf(String(len));
    */
    trunk->writeCOBS_USBPacket(localbytesin_, len);
  }
}

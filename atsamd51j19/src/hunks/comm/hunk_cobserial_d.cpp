/*
hunks/comm/hunk_cobserial_d.cpp

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#include "hunk_cobserial_d.h"

#ifdef BUILD_INCLUDES_HUNK_COBSERIALD

#include "isrcpp.h"

//D: SER4 PB12 -
#define COBD_SERCOM SERCOM4
#define COBD_SERCOM_CLK SERCOM4_GCLK_ID_CORE
#define COBD_COMPORT PORT->Group[1]
#define COBD_TXPIN 12
#define COBD_RXPIN 14
#define COBD_TXPERIPHERAL PERIPHERAL_C
#define COBD_RXPERIPHERAL PERIPHERAL_C
#define COBD_LIGHT_PIN 4 // TX: green
#define COBD_LIGHT_PORT PORT->Group[1]
#define COBD_TE_PIN 9  // RX: yellow
#define COBD_TE_PORT PORT->Group[1]

COBSerial_D::COBSerial_D(){
  // hunk stuff,
  type_ = "comm/COBSerial_D";
  numInputs = 1;
  inputs[0] = inChars;
  numOutputs = 1;
  outputs[0] = outChars;
  // set our local vals up,
  com_usart = &(COBD_SERCOM->USART);
  com_port = &(COBD_COMPORT);
  // ok,
  com_tx_pin = COBD_TXPIN;
  com_tx_peripheral = COBD_TXPERIPHERAL;
  com_rx_pin = COBD_RXPIN;
  com_rx_peripheral = COBD_RXPERIPHERAL;
  // and
  gclknum_pick = 8;
  gclkid_core = COBD_SERCOM_CLK;
  // lights
  light_pin = COBD_LIGHT_PIN; // green (good to go!)
  light_port = &(COBD_LIGHT_PORT);
  te_pin = COBD_TE_PIN; // yellow (pls catch!)
  te_port = &(COBD_TE_PORT);
}

void COBSerial_D::register_for_interrupts(void){
  isrcpp->register_for_ser4_rx(indice_, HUNK_HANDLER_A);
  isrcpp->register_for_ser4_tx(indice_, HUNK_HANDLER_B);
  NVIC_EnableIRQ(SERCOM4_2_IRQn);
  NVIC_EnableIRQ(SERCOM4_0_IRQn);
}

void COBSerial_D::unmask_clocks(void){
  MCLK->APBDMASK.bit.SERCOM4_ = 1;
}

#endif

/*

// Ponyo Port D:
// TX: PA16, SER1-0, peripheral C
// RX: PA18, SER1-2, peripheral C

COBSerial_D::COBSerial_D(){
  // hunk stuff,
  type_ = "comm/COBSerial_D";
  numInputs = 1;
  inputs[0] = inChars;
  numOutputs = 1;
  outputs[0] = outChars;
  // set our local vals up,
  com_usart = &(SERCOM1->USART);
  com_port = &(PORT->Group[0]);
  // ok,
  com_tx_pin = 16;
  com_tx_peripheral = 2;
  com_rx_pin = 18;
  com_rx_peripheral = 2;
  // and
  gclknum_pick = 10;
  gclkid_core = SERCOM1_GCLK_ID_CORE;
  // lights
  light_pin = 2; // green (good to go!)
  light_port = &(PORT->Group[0]);
  te_pin = 3; // yellow (pls catch!)
  te_port = &(PORT->Group[0]);
}

void COBSerial_D::register_for_interrupts(void){
  // sys -> interrupts
  isrcpp->register_for_ser1_rx(indice_, HUNK_HANDLER_A);
  isrcpp->register_for_ser1_tx(indice_, HUNK_HANDLER_B);
  // and,
  NVIC_EnableIRQ(SERCOM1_2_IRQn);
  NVIC_EnableIRQ(SERCOM1_0_IRQn);
}

void COBSerial_D::unmask_clocks(void){
  MCLK->APBAMASK.bit.SERCOM1_ = 1;
}

*/

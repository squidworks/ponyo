/*
hunks/comm/hunk_cobserial_e.cpp

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#include "hunk_cobserial_e.h"

#ifdef BUILD_INCLUDES_HUNK_COBSERIALE

#include "isrcpp.h"

//E: SER0 PA04 -
#define COBE_SERCOM SERCOM0
#define COBE_SERCOM_CLK SERCOM0_GCLK_ID_CORE
#define COBE_COMPORT PORT->Group[0]
#define COBE_TXPIN 4 // x-0
#define COBE_RXPIN 6 // x-2
#define COBE_TXPERIPHERAL PERIPHERAL_D
#define COBE_RXPERIPHERAL PERIPHERAL_D
#define COBE_LIGHT_PIN 7 // x-3
#define COBE_LIGHT_PORT PORT->Group[0]
#define COBE_TE_PIN 5  // x-1
#define COBE_TE_PORT PORT->Group[0]

COBSerial_E::COBSerial_E(){
  // hunk stuff,
  type_ = "comm/COBSerial_E";
  numInputs = 1;
  inputs[0] = inChars;
  numOutputs = 1;
  outputs[0] = outChars;
  // set our local vals up,
  com_usart = &(COBE_SERCOM->USART);
  com_port = &(COBE_COMPORT);
  // ok,
  com_tx_pin = COBE_TXPIN;
  com_tx_peripheral = COBE_TXPERIPHERAL;
  com_rx_pin = COBE_RXPIN;
  com_rx_peripheral = COBE_RXPERIPHERAL;
  // and
  gclknum_pick = 8;
  gclkid_core = COBE_SERCOM_CLK;
  // lights
  light_pin = COBE_LIGHT_PIN; // green (good to go!)
  light_port = &(COBE_LIGHT_PORT);
  te_pin = COBE_TE_PIN; // yellow (pls catch!)
  te_port = &(COBE_TE_PORT);
}

void COBSerial_E::register_for_interrupts(void){
  isrcpp->register_for_ser0_rx(indice_, HUNK_HANDLER_A);
  isrcpp->register_for_ser0_tx(indice_, HUNK_HANDLER_B);
  NVIC_EnableIRQ(SERCOM0_2_IRQn);
  NVIC_EnableIRQ(SERCOM0_0_IRQn);
}

void COBSerial_E::unmask_clocks(void){
  MCLK->APBAMASK.bit.SERCOM0_ = 1;
}

#endif

/*

// Ponyo Port E:
// TX: PB02, SER5-0, peripheral D
// RX: PB00, SER5-2, peripheral D

COBSerial_E::COBSerial_E(){
  // hunk stuff,
  type_ = "comm/COBSerial_E";
  numInputs = 1;
  inputs[0] = inChars;
  numOutputs = 1;
  outputs[0] = outChars;
  // set our local vals up,
  com_usart = &(SERCOM5->USART);
  com_port = &(PORT->Group[1]);
  // ok,
  com_tx_pin = 2;
  com_tx_peripheral = 3;
  com_rx_pin = 0;
  com_rx_peripheral = 3;
  // and
  gclknum_pick = 11;
  gclkid_core = SERCOM5_GCLK_ID_CORE;
  // lights
  light_pin = 8; // green (good to go!)
  light_port = &(PORT->Group[1]);
  te_pin = 5; // yellow (pls catch!)
  te_port = &(PORT->Group[1]);
}

void COBSerial_E::register_for_interrupts(void){
  // sys -> interrupts
  isrcpp->register_for_ser5_rx(indice_, HUNK_HANDLER_A);
  isrcpp->register_for_ser5_tx(indice_, HUNK_HANDLER_B);
  // and,
  NVIC_EnableIRQ(SERCOM5_2_IRQn);
  NVIC_EnableIRQ(SERCOM5_0_IRQn);
}

void COBSerial_E::unmask_clocks(void){
  MCLK->APBDMASK.bit.SERCOM5_ = 1;
}

*/

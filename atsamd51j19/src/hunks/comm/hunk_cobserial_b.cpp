/*
hunks/comm/hunk_cobserial_b.cpp

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#include "hunk_cobserial_b.h"

#ifdef BUILD_INCLUDES_HUNK_COBSERIALB

#include "isrcpp.h"

// B: SER3 PA20 -
#define COBB_SERCOM SERCOM3
#define COBB_SERCOM_CLK SERCOM3_GCLK_ID_CORE
#define COBB_COMPORT PORT->Group[0]
#define COBB_TXPIN 22 // x-0
#define COBB_RXPIN 20 // x-2
#define COBB_TXPERIPHERAL PERIPHERAL_D
#define COBB_RXPERIPHERAL PERIPHERAL_D
#define COBB_LIGHT_PIN 21 // x-3
#define COBB_LIGHT_PORT PORT->Group[0]
#define COBB_TE_PIN 23  // x-1
#define COBB_TE_PORT PORT->Group[0]

COBSerial_B::COBSerial_B(){
  // hunk stuff,
  type_ = "comm/COBSerial_B";
  numInputs = 1;
  inputs[0] = inChars;
  numOutputs = 1;
  outputs[0] = outChars;
  // set our local vals up,
  com_usart = &(COBB_SERCOM->USART);
  com_port = &(COBB_COMPORT);
  // ok,
  com_tx_pin = COBB_TXPIN;
  com_tx_peripheral = COBB_TXPERIPHERAL;
  com_rx_pin = COBB_RXPIN;
  com_rx_peripheral = COBB_RXPERIPHERAL;
  // and
  gclknum_pick = 8;
  gclkid_core = COBB_SERCOM_CLK;
  // lights
  light_pin = COBB_LIGHT_PIN; // green (good to go!)
  light_port = &(COBB_LIGHT_PORT);
  te_pin = COBB_TE_PIN; // yellow (pls catch!)
  te_port = &(COBB_TE_PORT);
}

void COBSerial_B::register_for_interrupts(void){
  isrcpp->register_for_ser3_rx(indice_, HUNK_HANDLER_A);
  isrcpp->register_for_ser3_tx(indice_, HUNK_HANDLER_B);
  NVIC_EnableIRQ(SERCOM3_2_IRQn);
  NVIC_EnableIRQ(SERCOM3_0_IRQn);
}

void COBSerial_B::unmask_clocks(void){
  MCLK->APBBMASK.bit.SERCOM3_ = 1;
}

#endif

/*

I hope you never have to go back to this board, but here it is:

COBSerial_B::COBSerial_B(){
  // hunk stuff,
  type_ = "comm/COBSerial_B";
  numInputs = 1;
  inputs[0] = inChars;
  numOutputs = 1;
  outputs[0] = outChars;
  // set our local vals up,
  com_usart = &(SERCOM4->USART);
  com_port = &(PORT->Group[1]);
  // ok,
  com_tx_pin = 12;
  com_tx_peripheral = 2;
  com_rx_pin = 14;
  com_rx_peripheral = 2;
  // and
  gclknum_pick = 8;
  gclkid_core = SERCOM4_GCLK_ID_CORE;
  // lights
  light_pin = 4; // green (good to go!)
  light_port = &(PORT->Group[0]);
  te_pin = 9; // yellow (pls catch!)
  te_port = &(PORT->Group[1]);
}

void COBSerial_B::register_for_interrupts(void){
  // sys -> interrupts
  isrcpp->register_for_ser4_rx(indice_, HUNK_HANDLER_A);
  isrcpp->register_for_ser4_tx(indice_, HUNK_HANDLER_B);
  // and,
  NVIC_EnableIRQ(SERCOM4_2_IRQn);
  NVIC_EnableIRQ(SERCOM4_0_IRQn);
}

void COBSerial_B::unmask_clocks(void){
  MCLK->APBDMASK.bit.SERCOM4_ = 1;
}

*/

/*
hunks/comm/hunk_cobserial_c.cpp

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#include "hunk_cobserial_c.h"

#ifdef BUILD_INCLUDES_HUNK_COBSERIALC

#include "isrcpp.h"

//C: SER2 PA08 -
#define COBC_SERCOM SERCOM2
#define COBC_SERCOM_CLK SERCOM2_GCLK_ID_CORE
#define COBC_COMPORT PORT->Group[0]
#define COBC_TXPIN 9 // x-0
#define COBC_RXPIN 10 // x-2
#define COBC_TXPERIPHERAL PERIPHERAL_D
#define COBC_RXPERIPHERAL PERIPHERAL_D
#define COBC_LIGHT_PIN 11 // x-3
#define COBC_LIGHT_PORT PORT->Group[0]
#define COBC_TE_PIN 8  // x-1
#define COBC_TE_PORT PORT->Group[0]

COBSerial_C::COBSerial_C(){
  // hunk stuff,
  type_ = "comm/COBSerial_C";
  numInputs = 1;
  inputs[0] = inChars;
  numOutputs = 1;
  outputs[0] = outChars;
  // set our local vals up,
  com_usart = &(COBC_SERCOM->USART);
  com_port = &(COBC_COMPORT);
  // ok,
  com_tx_pin = COBC_TXPIN;
  com_tx_peripheral = COBC_TXPERIPHERAL;
  com_rx_pin = COBC_RXPIN;
  com_rx_peripheral = COBC_RXPERIPHERAL;
  // and
  gclknum_pick = 8;
  gclkid_core = COBC_SERCOM_CLK;
  // lights
  light_pin = COBC_LIGHT_PIN; // green (good to go!)
  light_port = &(COBC_LIGHT_PORT);
  te_pin = COBC_TE_PIN; // yellow (pls catch!)
  te_port = &(COBC_TE_PORT);
}

void COBSerial_C::register_for_interrupts(void){
  isrcpp->register_for_ser2_rx(indice_, HUNK_HANDLER_A);
  isrcpp->register_for_ser2_tx(indice_, HUNK_HANDLER_B);
  NVIC_EnableIRQ(SERCOM2_2_IRQn);
  NVIC_EnableIRQ(SERCOM2_0_IRQn);
}

void COBSerial_C::unmask_clocks(void){
  MCLK->APBBMASK.bit.SERCOM2_ = 1;
}

#endif

/*
// Ponyo Port C:
// TX: PA08, SER0-0, peripheral C
// RX: PA10, SER0-2, peripheral C

COBSerial_C::COBSerial_C(){
  // hunk stuff,
  type_ = "comm/COBSerial_C";
  numInputs = 1;
  inputs[0] = inChars;
  numOutputs = 1;
  outputs[0] = outChars;
  // set our local vals up,
  com_usart = &(SERCOM0->USART);
  com_port = &(PORT->Group[0]);
  // ok,
  com_tx_pin = 8;
  com_tx_peripheral = 2;
  com_rx_pin = 10;
  com_rx_peripheral = 2;
  // and
  gclknum_pick = 9;
  gclkid_core = SERCOM0_GCLK_ID_CORE;
  // lights
  light_pin = 6; // green (good to go!)
  light_port = &(PORT->Group[0]);
  te_pin = 5; // yellow (pls catch!)
  te_port = &(PORT->Group[0]);
}

void COBSerial_C::register_for_interrupts(void){
  // sys -> interrupts
  isrcpp->register_for_ser0_rx(indice_, HUNK_HANDLER_A);
  isrcpp->register_for_ser0_tx(indice_, HUNK_HANDLER_C);
  // and,
  NVIC_EnableIRQ(SERCOM0_2_IRQn);
  NVIC_EnableIRQ(SERCOM0_0_IRQn);
}

void COBSerial_C::unmask_clocks(void){
  MCLK->APBAMASK.bit.SERCOM0_ = 1;
}
*/

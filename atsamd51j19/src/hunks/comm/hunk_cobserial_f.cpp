/*
hunks/comm/hunk_cobserial_f.cpp

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#include "hunk_cobserial_f.h"

#ifdef BUILD_INCLUDES_HUNK_COBSERIALF

#include "isrcpp.h"

//E: SER5 PB00 -
#define COBF_SERCOM SERCOM5
#define COBF_SERCOM_CLK SERCOM5_GCLK_ID_CORE
#define COBF_COMPORT PORT->Group[1]
#define COBF_TXPIN 2 // x-0
#define COBF_RXPIN 0 // x-2
#define COBF_TXPERIPHERAL PERIPHERAL_D
#define COBF_RXPERIPHERAL PERIPHERAL_D
#define COBF_LIGHT_PIN 1 // x-3
#define COBF_LIGHT_PORT PORT->Group[1]
#define COBF_TE_PIN 3  // x-1
#define COBF_TE_PORT PORT->Group[1]

COBSerial_F::COBSerial_F(){
  // hunk stuff,
  type_ = "comm/COBSerial_F";
  numInputs = 1;
  inputs[0] = inChars;
  numOutputs = 1;
  outputs[0] = outChars;
  // set our local vals up,
  com_usart = &(COBF_SERCOM->USART);
  com_port = &(COBF_COMPORT);
  // ok,
  com_tx_pin = COBF_TXPIN;
  com_tx_peripheral = COBF_TXPERIPHERAL;
  com_rx_pin = COBF_RXPIN;
  com_rx_peripheral = COBF_RXPERIPHERAL;
  // and
  gclknum_pick = 9;
  gclkid_core = COBF_SERCOM_CLK;
  // lights
  light_pin = COBF_LIGHT_PIN; // green (good to go!)
  light_port = &(COBF_LIGHT_PORT);
  te_pin = COBF_TE_PIN; // yellow (pls catch!)
  te_port = &(COBF_TE_PORT);
}

void COBSerial_F::register_for_interrupts(void){
  // are interrupts pin-registered ?
  isrcpp->register_for_ser5_rx(indice_, HUNK_HANDLER_A); //_on_one
  isrcpp->register_for_ser5_tx(indice_, HUNK_HANDLER_B);
  NVIC_EnableIRQ(SERCOM5_2_IRQn);
  NVIC_EnableIRQ(SERCOM5_0_IRQn);
}

void COBSerial_F::unmask_clocks(void){
  MCLK->APBDMASK.bit.SERCOM5_ = 1;
}

#endif

/*
// Ponyo Port E:
// TX: PA12, SER2-0, peripheral C
// RX: PA13, SER2-1, peripheral C

#warning this for new router,

#if 0

COBSerial_F::COBSerial_F(){
  // hunk stuff,
  type_ = "comm/COBSerial_F";
  numInputs = 1;
  inputs[0] = inChars;
  numOutputs = 1;
  outputs[0] = outChars;
  // set our local vals up,
  com_usart = &(SERCOM2->USART);
  com_port = &(PORT->Group[0]);
  // ok,
  #warning this is untested, and you need to mod some of the pinmuxing, we think: RX here is on SER2-1 (not SERx-2 as others)
  com_tx_pin = 12;
  com_tx_peripheral = 2;
  com_rx_pin = 13;
  com_rx_peripheral = 2;
  // and
  gclknum_pick = 12;
  gclkid_core = SERCOM0_GCLK_ID_CORE;
  // lights
  light_pin = 9; // green (good to go!)
  light_port = &(PORT->Group[0]);
  te_pin = 11; // yellow (pls catch!)
  te_port = &(PORT->Group[0]);
}

void COBSerial_F::register_for_interrupts(void){
  // sys -> interrupts
  isrcpp->register_for_ser2_rx(indice_, HUNK_HANDLER_A);
  isrcpp->register_for_ser2_tx(indice_, HUNK_HANDLER_B);
  // and,
  #warning is this now SERCOM2_1_IRQn ? seems likely ... altho ...
  #warning also to finish: complete adventure in isrcpp as well as in names etc
  NVIC_EnableIRQ(SERCOM2_2_IRQn);
  NVIC_EnableIRQ(SERCOM2_0_IRQn);
}

void COBSerial_F::unmask_clocks(void){
  MCLK->APBDMASK.bit.SERCOM5_ = 1;
}

#endif
*/

/*
hunks/comm/hunk_cobserial_d.h

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#ifndef HUNK_COBSERIALRJ45_D_H_
#define HUNK_COBSERIALRJ45_D_H_

#include "build_config.h"

#ifdef BUILD_INCLUDES_HUNK_COBSERIALD

#include "hunk_cobserial.h"

class COBSerial_D : public COBSerial {
private:

public:
  COBSerial_D();
  // learn about virtual items again? change needs to be for...
  // have to call these au manuel,
  void register_for_interrupts(void) override;
  void unmask_clocks(void) override;
};

#endif
#endif

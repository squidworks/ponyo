/*
hunks/comm/hunk_cobserial.h

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#ifndef HUNK_COBSERIALRJ45_H_
#define HUNK_COBSERIALRJ45_H_

#include <arduino.h>
#include "debugpins.h"
#include "build_config.h"
#include "hunks/hunks.h"
#include "transports/net_byteArray.h"
#include "utils/cobs.h"

// baud bb baud
// 63019 for a very safe 115200
// 54351 for a go-karting 512000
// 43690 for a trotting pace of 1MHz
// 21845 for the E30 2MHz
// 0 for max-speed 3MHz
#define COM_BAUD_VAL 0

#define HCBSRJ45_BUFFERLEN 1024
#define HCBSRJ45_NETBUFF_PAD 512
#define HCBSRJ45_NETBUFF_NUM 8

class COBSerial : public Hunk {
private:

public:
  COBSerial();
  void init(void);
  void loop(void);
  Inp_byteArray* inChars = new Inp_byteArray("data", HCBSRJ45_BUFFERLEN);
  Outp_byteArray* outChars = new Outp_byteArray("data", HCBSRJ45_BUFFERLEN);
  // we override these from the hunk, they're identical per our children
  void isr_handler_a(void) override;
  void isr_handler_b(void) override;
  // ideally these would be protected, but
  // these are our hardware addresses,
  SercomUsart* com_usart;
  PortGroup* com_port;
  // pins
  uint32_t com_tx_pin;
  uint32_t com_tx_bm;
  uint32_t com_tx_peripheral;
  uint32_t com_rx_pin;
  uint8_t com_rx_po = 2;
  uint32_t com_rx_bm;
  uint32_t com_rx_peripheral;
  // clock bs
  uint32_t gclknum_pick;
  uint32_t gclkid_core;
  // status light: track time since TX and RX events,
  uint32_t light_timesince_tx = LOOP_TICKS_PER_BLINK + 1;
  uint32_t light_timesince_rx = LOOP_TICKS_PER_BLINK + 1;
  // actuate one LED ... 
  uint32_t light_pin = 4;
  uint32_t light_bm;
  PortGroup* light_port = &(PORT->Group[1]);
  uint16_t light_time = LOOP_TICKS_PER_BLINK;
  // termination enable, sometimes software defined (for busses)
  uint32_t te_pin = 4;
  uint32_t te_bm;
  PortGroup* te_port = &(PORT->Group[1]);
  // we keep contiguous chunks of 1kb,
  uint8_t netBuf_[HCBSRJ45_NETBUFF_NUM][HCBSRJ45_NETBUFF_PAD];
  uint16_t netBufLen_[HCBSRJ45_NETBUFF_NUM];
  volatile uint8_t netBufReadAt_ = 0;
  volatile uint8_t netBufWriteTo_ = 0;
  // ptrs for our buffer of outgoing bytes, which have been COB'd
  uint8_t bytesOutToPhy_[HCBSRJ45_BUFFERLEN];
  volatile uint16_t bytesOutEndPtr_ = 0;
  volatile uint16_t bytesOutWritePtr_ = 0;
  // just 4 us,
  virtual void register_for_interrupts(void) = 0;
  virtual void unmask_clocks(void) = 0;
};

#endif

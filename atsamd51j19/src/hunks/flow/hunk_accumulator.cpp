/*
hunks/flow/hunk_accumulator.cpp

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#include "hunk_accumulator.h"
#include "trunk.h"

// TODO: should really dish float vals, and use accumulator type

Accumulator::Accumulator(){
  type_ = "flow/accumulator";
  numInputs = 1;
  inputs[0] = invar;
  numOutputs = 1;
  outputs[0] = outvar;
  numStates = 1;
  states[0] = tapsize;
}

void Accumulator::init(void){
  val_ = 0;
  count_ = 0;
  hold_ = 0;
  hasHold_ = false;
}

void Accumulator::loop(void){
  if(invar->io()){
    val_ += invar->get();
    count_ ++;
    if(count_ > tapsize->value()){
      hold_ = val_;
      hasHold_ = true;
      val_ = 0;
    }
  }
  if(!(outvar->io()) && hasHold_){
    outvar->put(hold_);
    hold_ = 0;
    hasHold_ = false;
  }
}

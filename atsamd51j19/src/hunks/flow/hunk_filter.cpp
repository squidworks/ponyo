/*
hunks/flow/hunk_filter.cpp

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#include "hunk_filter.h"
#include "trunk.h"

// TODO: should really dish float vals, and use accumulator type

Filter::Filter(){
  type_ = "flow/filter";
  numInputs = 1;
  inputs[0] = invar;
  numOutputs = 1;
  outputs[0] = outvar;
  numStates = 1;
  states[0] = tapsize;
}

void Filter::init(void){
  val_ = 0;
  count_ = 0;
  hold_ = 0;
  hasHold_ = false;
}

void Filter::loop(void){
  if(invar->io()){
    val_ += invar->get();
    count_ ++;
    if(count_ > tapsize->value()){
      hold_ = val_ / count_;
      hasHold_ = true;
    }
  }
  if(!(outvar->io()) && hasHold_){
    outvar->put(hold_);
    hold_ = 0;
    hasHold_ = false;
  }
}

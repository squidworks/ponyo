/*
hunks/flow/hunk_accumulator.h

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#ifndef HUNK_ACCUMULATOR_H_
#define HUNK_ACCUMULATOR_H_

#include <arduino.h>
#include "hunks/hunks.h"
#include "transports/net_uint32.h"
#include "states/state_uint16.h"

class Accumulator : public Hunk {
private:
  //
public:
  Accumulator();
  uint32_t val_;
  uint16_t count_;
  uint32_t hold_;
  boolean hasHold_;
  //
  void init(void);
  void loop(void);
  //
  Inp_uint32* invar = new Inp_uint32("stream");
  Outp_uint32* outvar = new Outp_uint32("filtered");
  State_uint16* tapsize = new State_uint16("tapsize", 16);
};

#endif

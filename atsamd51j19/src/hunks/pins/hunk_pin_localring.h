/*
hunks/pins/hunk_pin_localring.h

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#ifndef HUNK_LOCALRING_H_
#define HUNK_LOCALRING_H_

#include <arduino.h>
#include "hunks/hunks.h"

class LocalRing : public Hunk {
private:
public:
  LocalRing();
  //
  void init(void);
  void loop(void);
};

#endif

/*
hunks/pins/hunk_pin_pa12_output.cpp

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#include "hunk_pin_pa12_output.h"

#define PIN_NUM 12
#define PIN_BM (uint32_t)(1 << PIN_NUM)
#define PIN_PORT PORT->Group[0]

PA12_Output::PA12_Output(){
  type_ = "pins/pa12_output";
  // physical input is software output
  numInputs = 1;
  inputs[0] = inp;
}

void PA12_Output::init(void){
  // set otp
  PIN_PORT.DIRSET.reg = PIN_BM;
}

void PA12_Output::loop(void){
  if(inp->io()){
    if(inp->get()){
      PIN_PORT.OUTSET.reg = PIN_BM;
    } else {
      PIN_PORT.OUTCLR.reg = PIN_BM;
    }
  }
}

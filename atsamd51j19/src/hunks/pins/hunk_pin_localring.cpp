/*
hunks/pins/hunk_pin_localring.cpp

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#include "hunk_pin_localring.h"

// a ring test local to this hunk, using the loop framework

#define PIN_OUTPUT_NUM 12
#define PIN_OUTPUT_BM (uint32_t)(1 << PIN_OUTPUT_NUM)
#define PIN_OUTPUT_PORT PORT->Group[0]

#define PIN_INPUT_NUM 10
#define PIN_INPUT_BM (uint32_t)(1 << PIN_INPUT_NUM)
#define PIN_INPUT_PORT PORT->Group[0]

LocalRing::LocalRing(){
  type_ = "pins/localring";
}

void LocalRing::init(void){
  PIN_OUTPUT_PORT.DIRSET.reg = PIN_OUTPUT_BM;
  PIN_OUTPUT_PORT.OUTSET.reg = PIN_OUTPUT_BM;
  // set inp
  PIN_INPUT_PORT.DIRCLR.reg = PIN_INPUT_BM;
  PIN_INPUT_PORT.PINCFG[PIN_INPUT_NUM].reg = PORT_PINCFG_INEN;
}

void LocalRing::loop(void){
  if(PIN_INPUT_PORT.IN.reg & PIN_INPUT_BM){
    PIN_OUTPUT_PORT.OUTCLR.reg = PIN_OUTPUT_BM;
  } else {
    PIN_OUTPUT_PORT.OUTSET.reg = PIN_OUTPUT_BM;
  }
}

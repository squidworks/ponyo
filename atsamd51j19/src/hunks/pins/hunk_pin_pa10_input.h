/*
hunks/pins/hunk_pin_pa10_input.h

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#ifndef HUNK_PIN_PA10_INPUT_H_
#define HUNK_PIN_PA10_INPUT_H_

#include <arduino.h>
#include "hunks/hunks.h"
#include "transports/net_boolean.h"

class PA10_Input : public Hunk {
private:
public:
  PA10_Input();
  //
  void init(void);
  void loop(void);
  // pressure kids
  Outp_boolean* out = new Outp_boolean("state");
};

#endif

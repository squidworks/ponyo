/*
hunks/pins/hunk_pin_pa10_input.cpp

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#include "hunk_pin_pa10_input.h"

#define PIN_NUM 10
#define PIN_BM (uint32_t)(1 << PIN_NUM)
#define PIN_PORT PORT->Group[0]

PA10_Input::PA10_Input(){
  type_ = "pins/pa10_input";
  // physical input is software output
  numOutputs = 1;
  outputs[0] = out;
}

void PA10_Input::init(void){
  // setup as input
  PIN_PORT.DIRCLR.reg = PIN_BM;
  PIN_PORT.PINCFG[PIN_NUM].reg = PORT_PINCFG_INEN;
}

void PA10_Input::loop(void){
  if(!(out->io())){
    if(PIN_PORT.IN.reg & PIN_BM){
      out->put(true);
    } else {
      out->put(false);
    }
  }
}

/*
hunks/pins/hunk_pin_pa12_output.h

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#ifndef HUNK_PIN_PA12_OUTPUT_H_
#define HUNK_PIN_PA12_OUTPUT_H_

#include <arduino.h>
#include "hunks/hunks.h"
#include "transports/net_boolean.h"

class PA12_Output : public Hunk {
private:
public:
  PA12_Output();
  //
  void init(void);
  void loop(void);
  //
  Inp_boolean* inp = new Inp_boolean("write");
};

#endif

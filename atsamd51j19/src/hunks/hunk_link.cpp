/*
hunks/hunk_link.cpp

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#include "hunk_link.h"
#include "debugpins.h"
#include "trunk.h"
#include "../transports/typeset.h"
#include "manager.h"

#if(1)

Link::Link(){
  type_ = "link";
  // upd8
  // TODO do these w/ a fn ?
  numInputs = 1;
  inputs[0] = dtIn;
  numOutputs = 1;
  outputs[0] = dtOut;
  // statuuuuuses (actually includes 'isActive (boolean), otherLink (uint16)')
  numStates = 4;
  states[0] = isActive;
  states[1] = otherLink;
  states[2] = inputList;
  states[3] = outputList;
  // can't have stray acks
  for(uint8_t i = 0; i < MAX_TRANSPORT_MULTIPLEX; i ++){
    outputNeedsAck[i] = false;
    inputIsClearUpstream[i] = true;
  }
}

// need to override these also, returning false: thou shalt not change
boolean Link::stateChangeCallback_0(void){
  return false;
}
boolean Link::stateChangeCallback_1(void){
  return false;
}

boolean Link::ack(uint8_t indice){
  // do ack and return true; or if outbuf full,
  // ack is real simple,
  unsigned char msg[2] = {indice, LK_ACK};
  if(dtOut->io() && mmd_->getNumWaiting() == 0){
    mmd_->put(msg, 2);
  } else {
    dtOut->put(msg, 2);
  }
  return true;
}

void Link::openup(boolean reqResponse){
  // would initiate opening,
  if(isOpening_){
    // do nothing, state says we have already sent one ...
  } else {
    uint16_t dptr = 0;
    // set and move: dptr ++
    outBuffer_[dptr ++] = LK_HELLO;
    // write our indice as a uint16,
    //dbs();
    //dbn("link indice_: ");
    //dbf(indice_);

    writeUint16(outBuffer_, &dptr, indice_);
    if(reqResponse){
      writeBoolean(outBuffer_, &dptr, true);
    } else {
      writeBoolean(outBuffer_, &dptr, false);
    }
    isOpening_ = true;
    // putting on open,
    //dbs();
    //dbn("link put to open, reqResponse: ");
    //dbf(reqResponse);
    // ok, f this but
    /*
    for(uint8_t d = 0; d < dptr; d ++){
      dbs();
      dbn("dbyte: ");
      dbf(outBuffer_[d]);
    }
    */
    // and put, using this direct-if-possible:
    // so ... watch for the mmd to appear down the road,
    if(dtOut->io() && mmd_->getNumWaiting() == 0){
      mmd_->put(outBuffer_, dptr);
    } else {
      dtOut->put(outBuffer_, dptr);
    }
  }
}

void cleanupStringList(String** list, uint8_t num){
  for(uint8_t i = 0; i < num; i ++){
    delete list[i];
  }
}

void getTypeAndNameKeys(String* source, uint8_t* numInSet, String** names, String** types, size_t maxnum){
  // walk it?
  debug("link type and name keys: source below:");
  debug(*source);
  size_t lp = 0;
  size_t nt = 0;
  size_t nn = 0;
  // assuming we start at the 1st letter of a name,
  uint8_t state = NKS_INSIDE_NAME;
  for(size_t c = 0; c < source->length(); c ++){
    char cr = source->charAt(c);
    switch(state){
      case NKS_OUTSIDE:
        if(isAlpha(cr)){
          state = NKS_INSIDE_NAME;
          lp = c;
        } else if (cr == '('){
          state = NKS_INSIDE_TYPE;
          lp = c + 1;
        }
        break;
      case NKS_INSIDE_NAME:
        // anything that closes a name:
        if(cr == ' ' || cr == '('){
          //debug("tryna make sub for name:");
          //dbs();
          //dbn("'" + source->substring(lp, c));
          //dbf("'");
          names[nn] = new String(source->substring(lp, c));
          nn ++;
          state = NKS_OUTSIDE;
        }
        break;
      case NKS_INSIDE_TYPE:
        if(cr == ')'){
          //debug("tryna make sub for type:");
          //dbs();
          //dbn("'" + source->substring(lp, c));
          //dbf("'");
          types[nt] = new String(source->substring(lp, c));
          nt ++;
          state = NKS_OUTSIDE;
        }
      default:
        // ?
        break;
    }
  } // end for-chars loop
  if(nn == nt){
    *numInSet = nn;
  } else {
    // this also an error condition,
    error("num names != num types");
    adebug("source string for error is: ");
    adebug(*source);
    cleanupStringList(names, nn);
    cleanupStringList(types, nt);
    *numInSet = 0;
  }
}

// yonder mess,
Input* makeInputFrom(String type, String name, uint8_t position, uint16_t pind){
  if(type == "byteArray"){
    // comin in hot with the adhoc fix
    if(position == 0 && pind == 1){
      return new Inp_byteArray(name, LINK_BIG_DATA_WIDTH);
    } else {
      return new Inp_byteArray(name, LINK_DATA_WIDTH);
    }
  } else if (type == "uint32"){
    return new Inp_uint32(name);
  } else if (type == "int32"){
    return new Inp_int32(name);
  } else if (type == "boolean"){
    return new Inp_boolean(name);
  } else if (type == "MDmseg"){
    return new Inp_MDmseg(name);
  } else if (type == "mseg"){
    return new Inp_mseg(name);
  } else {
    error("! input type not present in build");
    return nullptr;
  }
}

Output* makeOutputFrom(String type, String name, uint8_t position, uint16_t pind){
  if(type == "byteArray"){
    if(position == 0 && pind == 1){
      return new Outp_byteArray(name, LINK_BIG_DATA_WIDTH);
    } else {
      return new Outp_byteArray(name, LINK_DATA_WIDTH);
    }
  } else if (type == "uint32"){
    return new Outp_uint32(name);
  } else if (type == "int32"){
    return new Outp_int32(name);
  } else if (type == "boolean"){
    return new Outp_boolean(name);
  } else if (type == "MDmseg"){
    return new Outp_MDmseg(name);
  } else if (type == "mseg"){
    return new Outp_mseg(name);
  } else {
    error("! output type not present in build");
    return nullptr;
  }
}


boolean Link::stateChangeCallback_2(void){
  // do startup type first, see if we can set by sending state down on
  // setup ... this requires setting up proper program, and program loading ...
  // that's whatever's up in cuttlefish, not saving ponyo contexts
  debug("! reach inputlist change !");
  // for now, in this limit, we'll commit only to adding to our set
  // so we can read in a list, then walk our current outputs ...
  // if they aren't in new positions, or don't match, throw em
  // else we do new things... then send evaluate
  // that makes this mucho easier,
  String* names[16];
  String* types[16];
  uint8_t numSet = 0;
  String ipString = inputList->swapValue();
  debug("prep to swap for: " + ipString);
  getTypeAndNameKeys(&ipString, &numSet, names, types, 16);
  // checkup, recall our 0th i/o are data bytes
  for(uint8_t s = 0; s < numSet; s ++){
    uint8_t ip = s + 1;
    Input* addnl = nullptr;
    if(ip >= numInputs){
      debug("ip-add, name: |" + *(names[s]) + "|, type: |" + *(types[s]) + "|");
      addnl = makeInputFrom(*(types[s]), *(names[s]), s, indice_);
      if(!addnl){
        error("could not allocate an input of type: " + *(types[s]));
        return false;
      } else {
        inputs[ip] = addnl;
        numInputs ++;
      }
    } else {
        // used to have chunks in inputs, could do this,
        // polymorphism / this link boondoggle needs a rethink, so just going to hack it here for now
        /* if((*(types[s]) != inputs[ip]->chunk->type) || (*(names[s]) != inputs[ip]->name)){ */
        if((*(names[s]) != inputs[ip]->name)){
        debug("not covering rewrite of existing link types!");
        debug("type req: " + *(types[s]));
        debug("type at: " + inputs[ip]->typeName);
        debug("name req: " + *(names[s]));
        debug("name at: " + inputs[ip]->name);
      }
    }
  }
  // before u complete, don't forget to
  cleanupStringList(names, numSet);
  cleanupStringList(types, numSet);
  // tell the mgr to evaluate us, !alert! haven't set yet when we eval, that it?
  inputList->set(ipString);
  ponyo->evaluateHunk(indice_);
  return true;
}

boolean Link::stateChangeCallback_3(void){
debug("! reach outputlist change !");
  String* names[16];
  String* types[16];
  uint8_t numSet = 0;
  String opString = outputList->swapValue();
  debug("prep to swap for: " + opString);
  getTypeAndNameKeys(&opString, &numSet, names, types, 16);
  // checkup, recall our 0th i/o are data bytes
  for(uint8_t s = 0; s < numSet; s ++){
    uint8_t op = s + 1;
    Output* addnl = nullptr;
    if(op >= numOutputs){
      debug("op-add, name: |" + *(names[s]) + "|, type: |" + *(types[s]) + "|");
      addnl = makeOutputFrom(*(types[s]), *(names[s]), s, indice_);
      if(!addnl){
        error("could not allocate an output of type: " + *(types[s]));
        return false;
      } else {
        outputs[op] = addnl;
        numOutputs ++;
      }
    } else {
      if((*(types[s]) != outputs[op]->chunk->type) || (*(names[s]) != outputs[op]->name)){
        error("not covering rewrite of existing link types!");
        debug("type req: " + *(types[s]));
        debug("type at: " + outputs[op]->chunk->type);
        debug("name req: " + *(names[s]));
        debug("name at: " + outputs[op]->name);
      }
    }
  }
  // before u complete, don't forget to
  cleanupStringList(names, numSet);
  cleanupStringList(types, numSet);
  // tell the mgr to evaluate us, !alert! haven't set yet when we eval, that it?
  outputList->set(opString);
  ponyo->evaluateHunk(indice_);
  return true;
}

boolean Link::makeInputsFrom(String list){
  String* names[16];
  String* types[16];
  uint8_t numSet = 0;
  String ipString = inputList->value();
  getTypeAndNameKeys(&ipString, &numSet, names, types, 16);
  // checkup,
  for(uint8_t s = 0; s < numSet; s ++){
    debug("ip-set, name: |" + *(names[s]) + "|, type: |" + *(types[s]) + "|");
    /// sooo... we've gotta hotfix this,
    Input* here = makeInputFrom(*(types[s]), *(names[s]), s, indice_);
    if(!here){
      error("could not allocate an input of type: " + *(types[s]));
      return false;
    } else {
      inputs[s + 1] = here;
    }
  }
  numInputs = numSet + 1;
  // before u complete, don't forget to
  cleanupStringList(names, numSet);
  cleanupStringList(types, numSet);
  return true;
}

boolean Link::makeOutputsFrom(String list){
  String* names[16];
  String* types[16];
  uint8_t numSet = 0;
  String opString = outputList->value();
  getTypeAndNameKeys(&opString, &numSet, names, types, 16);
  // checkup,
  for(uint8_t s = 0; s < numSet; s ++){
    debug("op-set, name: |" + *(names[s]) + "|, type: |" + *(types[s]) + "|");
    /// sooo...
    Output* here = makeOutputFrom(*(types[s]), *(names[s]), s, indice_);
    if(!here){
      error("could not allocate an input of type: " + *(types[s]));
      return false;
    } else {
      outputs[s + 1] = here;
    }
  }
  numOutputs = numSet + 1;
  // before u complete, don't forget to
  cleanupStringList(names, numSet);
  cleanupStringList(types, numSet);
  return true;
}

void Link::init(void){
  // we r static at the moment, so let's just program this au manuel as
  // it would be like,
  debug("INIT LINK");
  // we're interested in ...
  if(!makeInputsFrom(inputList->value())){
    error("link cannot make inputs from this init!");
  }
  if(!makeOutputsFrom(outputList->value())){
    error("link cannot make outputs from this init!")
  }
  setChildrenIndices();
  debug("LINK COMPLETE INIT");
}

void Link::loop(void){

  // (1) check if we have incoming,
  if(dtIn->io()){
    // copy in ? que ? let's just log then think about speed
    uint16_t len = dtIn->get(inBuffer_);
    //dbs();
    //dbn("LK: link got msg of len ");
    //dbf(len);
    // ah msg-reading-ptr,
    uint16_t dptr = 0;
    // ok,
    switch(inBuffer_[0]){
      case LK_HELLO:
        //debug("LK: link recv hello, trying to handle...");
        // this always means..
        dptr ++;
        // ok, if not active yet, now we are
        if(!isActive->value()){
          isActive->set(true);
        }
        // no longer opening (awaiting an openup reply)
        isOpening_ = false;
        // (1) write this for now...
        otherLink->set(readUint16(inBuffer_, &dptr));
        // if otherside wants a reply, we oblige
        if(readBoolean(inBuffer_, &dptr)){
          openup(false);
        }
        break;
      default:
        uint16_t pnum = inBuffer_[0];
        // check if ack
        if(inBuffer_[1] == LK_ACK){
          if (pnum >= numInputs){
            error("LK: link recv ack for out of bounds input");
            break;
          }
          if(inputIsClearUpstream[pnum] != false){
            ERRLIGHT_ON;
            error("LK: link recv ack for port not waiting");
            adbs();
            adbn("link name is " + name_);
            adbf();
            // important to debug this,
            for(uint16_t i = 0; i < len; i ++){
              adbs();
              adbn("byte: ");
              adbf(inBuffer_[i]);
            }
            break;
          }
          inputIsClearUpstream[pnum] = true;
        } else {
          if (pnum >= numOutputs){
            ERRLIGHT_ON;
            error("LK: link recv msg for out of bounds port, entire msg to follow:");
            adbs();
            adbn("link name is " + name_);
            adbf();
            // important to debug this,
            for(uint16_t i = 0; i < len; i ++){
              adbs();
              adbn("byte: ");
              adbf(inBuffer_[i]);
            }
            break;
          }
          if(!outputs[pnum]->io()){
          // rather than type-ing out etc, we should
          // put the bytes right in ?
          // first we would check type and get size,
          //dbs();
          //dbn("LK: tCheck for op: ");
          //dbf(outputs[pnum]->name);
          //dbs();
          //dbn("LK: op has key: ");
          //dbf(outputs[pnum]->typekey);
          // check proper type, and get length / width
          uint16_t dstart = 1;
          uint16_t dlength = 0;
          if(typeCheck(inBuffer_, outputs[pnum]->chunk, &dstart, &dlength)){
            // that's right lads we r type blind here,
            // good faith on typeCheck (not blind)
            //dbs();
            //dbn("LK: data port -> op for len: ");
            //dbf(dlength);
            // op is clear, so go ahead and write in manually, then set written state (io)
            memcpy(outputs[pnum]->chunk->data, &inBuffer_[dstart], dlength);
            outputs[pnum]->manualSetIo(dlength);
            // i.e. the other side needs to hear back that this is clear,
            outputNeedsAck[pnum] = true;
          } else {
            error("link recv msg with bad type or len spec, msg:");
            // important to debug this,
            for(uint16_t i = 0; i < len; i ++){
              adbs();
              adbn("byte: ");
              adbf(inBuffer_[i]);
            }
          }
        } else {
          error("link recv msg for occupied port, msg:");
          // important to debug this,
          for(uint16_t i = 0; i < len; i ++){
            adbs();
            adbn("byte: ");
            adbf(inBuffer_[i]);
          }
        }
        } // end not-ack case
        break;
    } // end zero-switch
    // clear it, let's check that we can transport to here
  } // end dtIn.io

  // (2) check if we can put to outside world,
  if(!(dtOut->io()) && mmd_->getNumWaiting() > 0){
    // the mmd will call the output's put ...
    mmd_->releaseTo(dtOut);
  }

  // (3) check if we can ack, if data has been consumed
  for(uint8_t o = 1; o < numOutputs; o ++){
    if(outputNeedsAck[o] && !outputs[o]->io()){
      outputNeedsAck[o] = false;
      if(!ack(o)){
        error("LK: err on ack at link");
        adbs();
        adbn("for port: ");
        adbf(o);
      }
    }
  }

  // (4) check if we can put any inputs on the line,
  for(uint8_t i = 1; i < numInputs; i ++){
    if(inputIsClearUpstream[i] && inputs[i]->io()){
      // direct call the state,
      if(isActive->value()){
        // ok, we can't ->get() it bc we have Inputs* not Inp_types*
        // we can write this msg in,
        // it gets routed:
        outBuffer_[0] = i;
        uint16_t mlen = 1;
        // it gets a type, len (if needed) and the bytes
        // actually, our c bytes are straightforward,
        // we already track len and typekey, so this is simple... ish,
        uint8_t wi = inputs[i]->getNextWireIndice();
        typeWrite(outBuffer_, &mlen, inputs[i]->connections[wi]->op->chunk);
        // that's it, now push it to the mmd
        mmd_->put(outBuffer_, mlen);
        // clear that,
        inputs[i]->manualReadFrom(wi);
        // wait 4 ack,
        inputIsClearUpstream[i] = false;
      } else {
        // try to say hi,
        openup(true);
      }
    } else {
      // input io but not clear upstream,
    }
  }

} // end link loop


#endif

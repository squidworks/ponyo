/*
hunks/hunks.cpp

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#include "hunks.h"

boolean Hunk::setChildrenIndices(void){
  dbs();
  dbn("SETTING INDICES: ");
  dbf(name_);
  dbs();
  dbn("indice is: ");
  dbf(indice_);
  for(uint8_t i = 0; i < numOutputs; i ++){
    outputs[i]->indice = i;
    outputs[i]->parentIndice = indice_;
  }
  for(uint8_t i = 0; i < numInputs; i ++){
    inputs[i]->indice = i;
    inputs[i]->parentIndice = indice_;
  }
  for(uint8_t i = 0; i < numStates; i ++){
    states[i]->indice = i;
    states[i]->parentIndice = indice_;
  }
  // should do
  return true;
}

boolean Hunk::tryStateChange(uint8_t stInd, unsigned char* msg, uint16_t mp){
  // ok, do all of the state work in the hunk, fine,
  if(stInd > numStates){
    return false;
  }
  if(msg[mp] == states[stInd]->chunk->key){
    // key is ok, traverse ptr,
    mp ++;
    // and wrip to swap holder,
    uint16_t len = 0;
    if(states[stInd]->chunk->isVarLen){
      len = (msg[mp + 1] << 8) | msg[mp];
      mp += 2;
      // to hack, we put ah zero at the end of the data as well, and set
      states[stInd]->swap->len = len;
      memcpy(states[stInd]->swap->data, &msg[mp], len);
      // well!
      void* eop = states[stInd]->swap->data;
      *((char*)eop + len) = 0;
    } else {
      len = states[stInd]->chunk->len;
      memcpy(states[stInd]->swap->data, &msg[mp], len);
    }
    // data's in the swap now, so we can offer this volley of virtual callbacks
    // each can be overridden, by default they just whip from swap -> chunk
    switch(stInd){
      case 0:
        return stateChangeCallback_0();
        break;
      case 1:
        return stateChangeCallback_1();
        break;
      case 2:
        return stateChangeCallback_2();
        break;
      case 3:
        return stateChangeCallback_3();
        break;
      case 4:
        return stateChangeCallback_4();
        break;
      case 5:
        return stateChangeCallback_5();
        break;
      case 6:
        return stateChangeCallback_6();
        break;
      case 7:
        return stateChangeCallback_7();
        break;
      default:
        error("state change for state > 4, switch is not complete!");
        return false;
    }
  } else {
    error("bad key for state change");
    return false;
  }
}

// by default, these just do...
// but are setup here to be repl' by virtual fns, for callbacks

boolean Hunk::stateChangeCallback_0(void){
  adebug("set by default");
  memcpy(states[0]->chunk->data, states[0]->swap->data, states[0]->swap->len);
  return true;
}

boolean Hunk::stateChangeCallback_1(void){
  adebug("set by default");
  memcpy(states[1]->chunk->data, states[1]->swap->data, states[1]->swap->len);
  return true;
}

boolean Hunk::stateChangeCallback_2(void){
  adebug("set by default");
  memcpy(states[2]->chunk->data, states[2]->swap->data, states[2]->swap->len);
  return true;
}

boolean Hunk::stateChangeCallback_3(void){
  adebug("set by default");
  memcpy(states[3]->chunk->data, states[3]->swap->data, states[3]->swap->len);
  return true;
}

boolean Hunk::stateChangeCallback_4(void){
  adebug("set by default");
  memcpy(states[4]->chunk->data, states[4]->swap->data, states[4]->swap->len);
  return true;
}

boolean Hunk::stateChangeCallback_5(void){
  adebug("set by default");
  memcpy(states[5]->chunk->data, states[5]->swap->data, states[5]->swap->len);
  return true;
}

boolean Hunk::stateChangeCallback_6(void){
  adebug("set by default");
  memcpy(states[6]->chunk->data, states[6]->swap->data, states[6]->swap->len);
  return true;
}

boolean Hunk::stateChangeCallback_7(void){
  adebug("set by default");
  memcpy(states[7]->chunk->data, states[7]->swap->data, states[7]->swap->len);
  return true;
}

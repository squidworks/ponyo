/*
hunks/hunk_int32_debugger.cpp

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#include "hunk_int32_debugger.h"

Int32_Debugger::Int32_Debugger(){
  type_ = "int32_debugger";
  numInputs = 1;
  inputs[0] = numIn;
}

void Int32_Debugger::init(void){
  //
}

void Int32_Debugger::loop(void){
  if(numIn->io()){
    int32_t num = numIn->get();
  }
}

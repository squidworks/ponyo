/*
hunks/hunk_pin.h

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#ifndef PIN_H_
#define PIN_H_

#include <arduino.h>
#include "hunks/hunks.h"

#define PIN_NUM 3
#define PIN_BM (uint32_t)(1 << PIN_NUM)
#define PIN_PORT PORT->Group[0]

class Pin : public Hunk {
private:
  // ??
public:
  Pin();
  void init(void);
  void loop(void);
};

#endif

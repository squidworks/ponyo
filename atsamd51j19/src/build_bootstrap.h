/*
build_bootstrap.h

configure startup dataflow patches for ponyo boards

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#ifndef CIRCUIT_H_
#define CIRCUIT_H_

/*

devices are unique,
here we write the boostrap programs, and the build configurations for these devices

*/

/*
// to macro hunk setup,
Hunk* hnk = allocateHunkByType(type);
// or
Hunk* hnk = new HunkTypeConstructor();
// give it a name, and an indice...
hnk->name_ = "some_new_name";
hnk->indice_ = p->hunkCount_;
p->hunks[hunkCount_] = hnk;
p->hunkCount_ ++;
// could set state objects now:
// hnk->states[2].value = x;
// then we do indices,
hnk->setChildrenIndices();
// and kick it hello
hnk->init();
// hnk adding complete,
*/

/*
// for ah hunk to compile / add to the system:
at hunklist.h
#include HUNK_STRING

String hunklist[len]{
  HUNK_STRING,
}

in manager.cpp

Hunk* allocateHunkByType(String type){
  [...]
  else if (type == "HUNK_STRING"){
    return new HUNK_FUNCTION()
  }
}

the name switch doesn't have to live in manager,

*/

#include "build_config.h"
#include "hunks/hunks.h"
#include "manager.h"

// ------------------------------------------------------- //
// ------------------ DEFAULT HUNK LIST  ----------------- //
// ------------------------------------------------------- //

#include "hunks/hunk_link.h"
#include "hunks/comm/hunk_cobserialusb.h"
#include "hunks/comm/hunk_cobserial_a.h"
#include "hunks/control/hunk_saturnjog.h"
#include "hunks/math/hunk_adder.h"
#include "hunks/math/hunk_booleaninversion.h"
#include "hunks/flow/hunk_filter.h"
#include "hunks/flow/hunk_accumulator.h"
#include "hunks/driver/hunk_errlight.h"
#include "hunks/driver/hunk_spindlePWM.h"

#include "hunks/pins/hunk_pin_localring.h"
#include "hunks/pins/hunk_pin_pa10_input.h"
#include "hunks/pins/hunk_pin_pa12_output.h"

// ------------------------------------------------------- //
// ------------------ OPTION HUNK LIST  ------------------ //
// ------------------------------------------------------- //

#ifdef BUILD_INCLUDES_HUNK_COBSERIALB
  #include "hunks/comm/hunk_cobserial_b.h"
#endif

#ifdef BUILD_INCLUDES_HUNK_COBSERIALC
  #include "hunks/comm/hunk_cobserial_c.h"
#endif

#ifdef BUILD_INCLUDES_HUNK_COBSERIALD
  #include "hunks/comm/hunk_cobserial_d.h"
#endif

#ifdef BUILD_INCLUDES_HUNK_COBSERIALE
  #include "hunks/comm/hunk_cobserial_e.h"
#endif

#ifdef BUILD_INCLUDES_HUNK_COBSERIALF
  #include "hunks/comm/hunk_cobserial_f.h"
#endif

#ifdef BUILD_INCLUDES_HUNK_DRIVER_SPI_TLC5940
  #include "hunks/driver/spi_tlc5940.h"
#endif

#ifdef BUILD_INCLUDES_HUNK_STEPPER
  #include "hunks/driver/hunk_stepper.h"
#endif

#ifdef BUILD_INCLUDES_HUNK_MDTODMSEG
  #include "hunks/control/hunk_MDtoDmseg.h"
#endif

#define HUNK_LIST_LENGTH 64

String hunklist[HUNK_LIST_LENGTH];
uint16_t hlc;

Hunk* allocateHunkByType(String type){
  // I *dont* have a good answer for this yet,
  // I'll run this code many times, it's inefficient, that's fine for now !
  // if you are a cpp expert, tweet @ me for a solution
  hlc = 0;
  Hunk* ret = nullptr;
  hunklist[hlc++] = "link";
  if (type == "link"){
    ret = new Link();
  }
  hunklist[hlc++] = "comm/COBS_USB";
  if (type == "comm/COBS_USB") {
    ret = new COBS_USB();
  }
  hunklist[hlc++] = "control/saturnjog";
  if (type == "control/saturnjog") {
    ret = new SaturnJog();
  }
  hunklist[hlc++] = "math/adder";
  if (type == "math/adder") {
    ret = new Adder();
  }
  hunklist[hlc++] = "math/booleaninversion";
  if (type == "math/booleaninversion") {
    ret = new BooleanInversion();
  }
  hunklist[hlc++] = "flow/filter";
  if (type == "flow/filter") {
    ret = new Filter();
  }
  hunklist[hlc++] = "flow/accumulator";
  if (type == "flow/accumulator") {
    ret = new Accumulator();
  }
  hunklist[hlc++] = "driver/errlight";
  if (type == "driver/errlight"){
    ret = new Hunk_ErrLight();
  }
  hunklist[hlc++] = "driver/spindlePWM";
  if(type == "driver/spindlePWM"){
    ret = new Hunk_SpindlePWM();
  }
  hunklist[hlc++] = "pins/localring";
  if(type == "pins/localring"){
    ret = new LocalRing();
  }
  hunklist[hlc++] = "pins/pa10_input";
  if(type == "pins/pa10_input"){
    ret = new PA10_Input();
  }
  hunklist[hlc++] = "pins/pa12_output";
  if(type == "pins/pa12_output"){
    ret = new PA12_Output();
  }
  hunklist[hlc++] = "comm/COBSerial_A";
  if (type == "comm/COBSerial_A") {
    ret = new COBSerial_A();
  }
  #ifdef BUILD_INCLUDES_HUNK_COBSERIALB
    hunklist[hlc++] = "comm/COBSerial_B";
    if (type == "comm/COBSerial_B"){
      ret = new COBSerial_B();
    }
  #endif
  #ifdef BUILD_INCLUDES_HUNK_COBSERIALC
    hunklist[hlc++] = "comm/COBSerial_C";
    if (type == "comm/COBSerial_C"){
      ret = new COBSerial_C();
    }
  #endif
  #ifdef BUILD_INCLUDES_HUNK_COBSERIALD
    hunklist[hlc++] = "comm/COBSerial_D";
    if (type == "comm/COBSerial_D"){
      ret = new COBSerial_D();
    }
  #endif
  #ifdef BUILD_INCLUDES_HUNK_COBSERIALE
    hunklist[hlc++] = "comm/COBSerial_E";
    if (type == "comm/COBSerial_E"){
      ret = new COBSerial_E();
    }
  #endif
  #ifdef BUILD_INCLUDES_HUNK_COBSERIALF
    hunklist[hlc++] = "comm/COBSerial_F";
    if (type == "comm/COBSerial_F"){
      ret = new COBSerial_F();
    }
  #endif
  #ifdef BUILD_INCLUDES_HUNK_STEPPER
    hunklist[hlc++] = "driver/stepper";
    if (type == "driver/stepper"){
      ret = new Stepper();
    }
  #endif
  #ifdef BUILD_INCLUDES_HUNK_DRIVER_SPI_TLC5940
    hunklist[hlc++] = "driver/spi_tlc5940";
    if (type == "driver/spi_tlc5940"){
      ret = new SPI_TLC5940();
    }
  #endif
  #ifdef BUILD_INCLUDES_HUNK_MDTODMSEG
    hunklist[hlc++] = "control/MDtoDmseg";
    if (type == "control/MDtoDmseg"){
      ret = new MDtoDmseg();
    }
  #endif
  return ret;
}

// ------------------------------------------------------- //
// ---------------------- BOOTSTRAPS --------------------- //
// ------------------------------------------------------- //

// ok, here is ah board definition:
// since we define a global 'bootstrap' fn,
// we should be blocked from defining multiples of these

// to start a test, I'll see how I might just include the stepper when this is
// defined

#ifdef BOARD_IS_BASIC
  void bootstrap(Manager* p){
    // and bootstrap,
    p->addHunk("link");
    #ifdef BOOT_USB
      p->addHunk("comm/COBS_USB");
    #else
      p->addHunk("comm/COBSerial_A");
    #endif
    // (0) manager to (1) link's 1th
    p->addLink(0, 0, 1, 1);
    p->addLink(1, 1, 0, 0);
    // (1) link's 0th to cobserial (2) 0th
    p->addLink(1, 0, 2, 0);
    p->addLink(2, 0, 1, 0);
  }
#endif

#ifdef BOARD_IS_STEPPER

  Stepper stepper; // = new Stepper();

  void TC0_Handler(void){
    //PORT->Group[0].OUTSET.reg = (uint32_t)(1 << 20);
    stepper.isr_handler_a();
    //PORT->Group[0].OUTCLR.reg = (uint32_t)(1 << 20);
  }

  void bootstrap(Manager* p){
    // and bootstrap,
    Hunk* lnk = p->addHunk("link");
    #ifdef BOOT_USB
      p->addHunk("comm/COBS_USB");
    #else
      p->addHunk("comm/COBSerial_A");
    #endif
    // (0) manager to (1) link's 1th
    p->addLink(0, 0, 1, 1);
    // (1) link to (0) manager's 0th
    p->addLink(1, 1, 0, 0);
    // (1) link's 0th to cobserial (2) 0th
    p->addLink(1, 0, 2, 0);
    p->addLink(2, 0, 1, 0);
    // more plus
    stepper.name_ = "step_driver";
    p->includeHunk(&stepper);
    /*
    // and also,
    // p->addHunk("control/MDtoDmseg");
    // let's write the link we want...
    State* swapOption = new State_string("outputList", "mgrMsgs (byteArray), mdseg (mseg)", 512);
    lnk->states[3]->swap = swapOption->chunk;
    lnk->stateChangeCallback_3();
    // ok, hookup:
    // abandoning this... some kind of error during the refresh, front door
    // reset from cuttlefish works best... big shrug
    p->addLink(1, 2, 3, 0);
    */
  }
#endif

#ifdef BOARD_IS_ROUTER
  //
  void bootstrap(Manager* p){
    // and bootstrap,
    p->addHunk("link");
    #ifdef BOOT_USB
      p->addHunk("comm/COBS_USB");
    #else
      p->addHunk("comm/COBSerial_A");
    #endif
    // (0) manager to (1) link's 1th
    p->addLink(0, 0, 1, 1);
    p->addLink(1, 1, 0, 0);
    // (1) link's 0th to cobserial (2) 0th
    p->addLink(1, 0, 2, 0);
    p->addLink(2, 0, 1, 0);
  }
#endif

#ifdef BOARD_IS_DLPCORE

  // assuming I'm going to want some interrupts for this build:

  SPI_TLC5940 tlcdriver;

  void bootstrap(Manager* p){
    // and bootstrap,
    Hunk* lnk = p->addHunk("link");
    // *whistles*
    State* swapOption = new State_string("outputList", "mgrMsgs (byteArray), brightness (uint32)", 512);
    lnk->states[3]->swap = swapOption->chunk;
    lnk->stateChangeCallback_3();
    /*
    String inputs = String("mgrMsgs (byteArray), typeTwo (uint32)");
    size_t len = inputs.length();
    inputs.getBytes((unsigned char*)(lnk->states[2]->swap->data), len);
    (unsigned char*)(lnk->states[2]->swap->data)[len + 1] = '\0';
    */
    #ifdef BOOT_USB
      p->addHunk("comm/COBS_USB");
    #else
      p->addHunk("comm/COBSerial_A");
    #endif
    // (0) manager to (1) link's 1th
    p->addLink(0, 0, 1, 1);
    p->addLink(1, 1, 0, 0);
    // (1) link's 0th to cobserial (2) 0th
    p->addLink(1, 0, 2, 0);
    p->addLink(2, 0, 1, 0);
    // add our driver, and hookup...
    tlcdriver.name_ = "led_driver";
    p->includeHunk(&tlcdriver);
    p->addLink(1, 2, 3, 0);
    // more plus
  }
#endif

#ifdef BOARD_IS_SPINDLE
  void bootstrap(Manager* p){
    // and bootstrap,
    p->addHunk("link");
    #ifdef BOOT_USB
      p->addHunk("comm/COBS_USB");
    #else
      p->addHunk("comm/COBSerial_A");
    #endif
    // (0) manager to (1) link's 1th
    p->addLink(0, 0, 1, 1);
    p->addLink(1, 1, 0, 0);
    // (1) link's 0th to cobserial (2) 0th
    p->addLink(1, 0, 2, 0);
    p->addLink(2, 0, 1, 0);
    // ok,
    p->addHunk("driver/spindlePWM");
  }
#endif

// here's a problem: if it's boot-usb, but we'll also
// want to have cobs_a available ... ? how to handle interrupts, then ?
// basically have to dynamic allocate.
// regardless, this doesn't have to be the master-blaster yet, just make some
// machines jog ...

/*
#include "hunks/hunk_loadcell.h"
*/

#endif // end CIRCUIT_H_

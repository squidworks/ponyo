/*
trunk.cpp

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#include "trunk.h"

// do we ever ?

Trunk* Trunk::instance = 0;

Trunk* Trunk::getInstance(void){
  if(instance == 0){
    instance = new Trunk();
  }
  return instance;
}

Trunk::Trunk(){
  // aint no thang
}

// ok,
/*
we can escape this way:
COBSerial, proper-like, will always direct call
writeCOBSerialPacket,
if we use ->println, or debug() (which is just...)
then, whenever we call write with \n char,
we can stick a flag in front of the cobs packet
*/
size_t Trunk::write(uint8_t bt){
  if(bt == '\n'){
    // 254 key says that this is a debug message
    // so that in the upstream link we can cast it as a
    // string for the humans
    errbuf_[0] = LK_LLDEBUG;
    for(uint16_t i = 0; i < ps_; i ++){
      errbuf_[i + 1] = buf_[i];
    }
    writeCOBS_USBPacket(errbuf_, ps_);
    ps_ = 0;
  } else {
    buf_[ps_] = bt;
    ps_ ++;
  }
  return 1;
}

boolean Trunk::writeCOBS_USBPacket(uint8_t* buf, size_t len){
  // halting if
  // some chance you have to understand Serial.write()
  // to get where these errors are showing up
  // also keep in mind that delay still lives strong
  if(len > TRUNK_OUTBUF_LEN - 3){
    // not allowed; hard bail (unsafe!)
    digitalWrite(13, HIGH);
    error("writing COBS packet is too large, bailing");
    return false;
  } else {
    // cobs encode it
    size_t resl = cobs_encode(buf, len, cbuf_);
    // ship it, adding a trailing zero ? (do in cobs?)
    Serial.write(cbuf_, resl);
    //...
    return true;
  }
}

Trunk* trunk = Trunk::getInstance();

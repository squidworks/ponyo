/*
build_config.h

configure build options for ponyo boards

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#ifndef BUILD_CONFIG_H_
#define BUILD_CONFIG_H_

/*

this should be where we go to configure a system,

*/

// trash bucket:
// #define IS_OLD_STEPPER

//#define DEBUG_MSGS

// some ms between loops, to chill things out
#define LOOP_DELAY 0
#define LOOP_TICKS_PER_BLINK 8192

#define USE_DEBUGPIN_A
#define USE_DEBUGPIN_B

// START: do bootstrap steppers -> connected, ok
// then bootstrap router -> ok, hello all three,
// now try stepping ... where borked ?
#define BOOT_USB

// pick 'yer board
//#define BOARD_IS_BASIC
#define BOARD_IS_STEPPER
//#define BOARD_IS_ROUTER
//#define BOARD_IS_SPINDLE
//#define BOARD_IS_DLPCORE

#ifdef BOARD_IS_STEPPER
  #define BUILD_INCLUDES_HUNK_STEPPER
  #define BUILD_INCLUDES_HUNK_MDTODMSEG
#endif

#ifdef BOARD_IS_ROUTER
  #define BUILD_INCLUDES_HUNK_COBSERIALB // ok
  #define BUILD_INCLUDES_HUNK_COBSERIALC // ok
  #define BUILD_INCLUDES_HUNK_COBSERIALD // ok
  #define BUILD_INCLUDES_HUNK_COBSERIALE // ok
  #define BUILD_INCLUDES_HUNK_COBSERIALF // ok
  #define BUILD_INCLUDES_HUNK_MDTODMSEG
#endif

#ifdef BOARD_IS_DLPCORE
  #define BUILD_INCLUDES_HUNK_DRIVER_SPI_TLC5940
#endif

#endif

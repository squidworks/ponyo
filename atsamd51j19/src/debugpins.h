/*
debugpins.h

low level interface to debug outputs

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#ifndef DEBUGPINS_H_
#define DEBUGPINS_H_

#include "build_config.h"

// ------------------------------------------------------- //
// ---------------------- DEBUGPIN ----------------------- //
// ------------------------------------------------------- //

// doesn't actually belong here:

#define PERIPHERAL_A 0
#define PERIPHERAL_B 1
#define PERIPHERAL_C 2
#define PERIPHERAL_D 3
#define PERIPHERAL_E 4
#define PERIPHERAL_F 5
#define PERIPHERAL_G 6
#define PERIPHERAL_H 7
#define PERIPHERAL_I 8
#define PERIPHERAL_K 9
#define PERIPHERAL_L 10
#define PERIPHERAL_M 11
#define PERIPHERAL_N 12

// ------------------------------------------------------- //
// ---------------------- BASEBOARD ---------------------- //
// ------------------------------------------------------- //

#define CLKLIGHT_PIN 30
#define CLKLIGHT_PORT PORT->Group[1]
#define ERRLIGHT_PIN 27
#define ERRLIGHT_PORT PORT->Group[0]

#define CLKLIGHT_BM (uint32_t)(1 << CLKLIGHT_PIN)
#define CLKLIGHT_SETUP CLKLIGHT_PORT.DIRSET.reg = CLKLIGHT_BM
#define CLKLIGHT_ON CLKLIGHT_PORT.OUTCLR.reg = CLKLIGHT_BM
#define CLKLIGHT_OFF CLKLIGHT_PORT.OUTSET.reg = CLKLIGHT_BM
#define CLKLIGHT_TOGGLE CLKLIGHT_PORT.OUTTGL.reg = CLKLIGHT_BM

#define ERRLIGHT_BM (uint32_t)(1 << ERRLIGHT_PIN)
#define ERRLIGHT_SETUP ERRLIGHT_PORT.DIRSET.reg = ERRLIGHT_BM
#define ERRLIGHT_ON ERRLIGHT_PORT.OUTCLR.reg = ERRLIGHT_BM
#define ERRLIGHT_OFF ERRLIGHT_PORT.OUTSET.reg = ERRLIGHT_BM
#define ERRLIGHT_TOGGLE ERRLIGHT_PORT.OUTTGL.reg = ERRLIGHT_BM

// debug ports:

// ------------------------------------------------------- //
// ---------------------- DEBUGPIN ----------------------- //
// ------------------------------------------------------- //

#ifdef USE_DEBUGPIN_A
  #define DEBUGPIN_A_PORT PORT->Group[0]
  #define DEBUGPIN_A_PIN 6
  #define DEBUGPIN_A_BM (uint32_t)(1 << DEBUGPIN_A_PIN)
  #define DEBUGPIN_A_SETUP DEBUGPIN_A_PORT.DIRSET.reg = DEBUGPIN_A_BM
  #define DEBUGPIN_A_SET DEBUGPIN_A_PORT.OUTSET.reg = DEBUGPIN_A_BM
  #define DEBUGPIN_A_CLEAR DEBUGPIN_A_PORT.OUTCLR.reg = DEBUGPIN_A_BM
  #define DEBUGPIN_A_TOGGLE DEBUGPIN_A_PORT.OUTTGL.reg = DEBUGPIN_A_BM
#else
  #define DEBUGPIN_A_PORT
  #define DEBUGPIN_A_PIN
  #define DEBUGPIN_A_BM
  #define DEBUGPIN_A_SETUP
  #define DEBUGPIN_A_SET
  #define DEBUGPIN_A_CLEAR
  #define DEBUGPIN_A_TOGGLE
#endif

#ifdef USE_DEBUGPIN_B
  #define DEBUG_PIN_B_PORT PORT->Group[1]
  #define DEBUG_PIN_B_PIN 9
  #define DEBUG_PIN_B_BM (uint32_t)(1 << DEBUG_PIN_B_PIN)
  #define DEBUG_PIN_B_SETUP DEBUG_PIN_B_PORT.DIRSET.reg = DEBUG_PIN_B_BM
  #define DEBUG_PIN_B_SET DEBUG_PIN_B_PORT.OUTSET.reg = DEBUG_PIN_B_BM
  #define DEBUG_PIN_B_CLEAR DEBUG_PIN_B_PORT.OUTCLR.reg = DEBUG_PIN_B_BM
#else
  #define DEBUG_PIN_B_PORT
  #define DEBUG_PIN_B_PIN
  #define DEBUG_PIN_B_BM
  #define DEBUG_PIN_B_SETUP
  #define DEBUG_PIN_B_SET
  #define DEBUG_PIN_B_CLEAR
#endif

#endif

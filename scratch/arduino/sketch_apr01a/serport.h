#ifndef SERPORT_H_
#define SERPORT_H_

#include <arduino.h>
#include "hunks.h"

class SerPort : public Hunk{
  // the Serial object is friggen arduino global wow 
  uint16_t count;
  
  public:
  String name = "serport";
  void init(void);
  void loop(void);
};


#endif


#ifndef HUNKS_H_
#define HUNKS_H_

#include <arduino.h>
// hunk, or the interface ... is the object the manager
// will bump over 
// mostly, just a list of pointers
// an a 'purely virtual' class 
#include "output.h"

class Hunk{
  public:
  String name;
  String id; 
  virtual void init(void) = 0;
  virtual void loop(void) = 0;
  Output* outputs[16];
};

#endif

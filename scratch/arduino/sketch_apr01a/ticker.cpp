#include "ticker.h"

void Ticker::init(void) {
  count = 0;
  pinMode(13, OUTPUT);
}

void Ticker::loop(void) {
  count ++;
  if(count == 0){
    digitalWrite(13, !digitalRead(13));
  }
}


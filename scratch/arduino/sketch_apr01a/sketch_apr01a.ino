#include "hunks.h"
#include "ticker.h"
#include "serport.h"

// we keep these, and that
Hunk* hnks[256];
uint16_t hnkCount = 0;

void addHunk(String name) {
  Hunk* hnk = nullptr;
  if (name == "ticker") {
    hnk = new Ticker();
  } else if (name == "serial") {
    hnk = new SerPort();
  } else {
    // err
  }
  if (hnk) {
    hnk->init();
    hnks[hnkCount] = hnk;
    hnkCount ++;
  } else {
    // err
  }
}

// tasks for the 'manager'
// -> add a hunk
// -> add a link (needs inputs, outputs, addlink, and transport

void setup() {
  // put your setup code here, to run once:
  addHunk("ticker");
  addHunk("serial");
}

void loop() {
  /*
    digitalWrite(13, HIGH);
    delay(1000);
    digitalWrite(13, LOW);
    delay(1000);
  */
  // network transport ...
  for (uint16_t h = 0; h < hnkCount; h ++) {
    // roll over all outputs
    uint16_t oCnt = hnks[h]->getNumOutputs();
    for (uint16_t o = 0; o < oCnt; o ++) {
      // first check if there are any connections to it
      uint8_t nc = hnks[h]->outputs[o]->getNumConnections();
      // first, do we even have conns ?
      if (nc) {
        // if it's occupied
        if (hnks[h]->outputs[o]->io) {
          // and it hasn't been posted
          if (!(hnks[h]->outputs[o]->wasPosted)) {
            // begin clearance check
            bool clear = true;
            for (uint8_t i = 0; i < hnks[h]->outputs[o]->getNumConnections(); i ++) {
              if (hnks[h]->outputs[o]->connection[i].io) {
                // this input hasn't been consumed
                clear = false;
              }
            }
            // now we know if they are all clear, or not
            if (clear) {
              // all r cleared, we can move the new data in
              for (uint8_t i = 0; i < hnks[h]->outputs[o]->getNumConnections(); i ++) {
                hnks[h]->outputs[o]->connection[i]->put(hnks[h]->outputs[o].data);
              }
              hnks[h]->outputs[o]->wasPosted = true;
            }
          } else {
            // output was posted, check if we can clear it
            bool clear = true;
            for (uint8_t i = 0; i < hnks[h]->outputs[o]->getNumConnections(); i ++) {
              if (hnks[h]->outputs[o]->connection[i].io) {
                // this input hasn't been consumed
                clear = false;
              }
            }
            if (clear) {
              hnks[h]->outputs[o]->clear();
            }
          } // end output posted / not posted state check
        } // end if-output-io
      } else {
        // anything posted gets wiped into the aether
      }

    }// end loop over outputs
  }
  // and then loops
  for (uint16_t h = 0; h < hnkCount; h ++) {
    hnks[h]->loop();
  }
}

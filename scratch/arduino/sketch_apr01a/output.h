#ifndef OUTPUT_H_
#define OUTPUT_H_ 

#include <arduino.h> 
#include "input.h"

/* jake learns about templates ? */

class Output{
  public:
  // naming 
  String name;
  String type;
  // state
  boolean io;
  boolean wp;
  // actions 
  boolean put(int);
  boolean clear(void);
  Input* connections[16];
}

#endif 

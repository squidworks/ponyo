#ifndef INPUT_H_
#define INPUT_H_

#include <arduino.h>

/* templates ? get() returns type ...  */ 

class Input{
  public:
  // naming
  String name;
  String type;
  // state
  boolean io;
  // actions
  int get();
}

#endif

#ifndef TICKER_H_
#define TICKER_H_

#include <arduino.h>
#include "hunks.h"

class Ticker : public Hunk{
  uint16_t count;
  
  public:
  String name = "ticker";
  void init(void);
  void loop(void);
};

#endif

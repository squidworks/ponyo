/*
 * TickerLED.cpp
 *
 * Created: 4/1/2019 12:44:58 PM
 *  Author: Jake
 */ 

#include "TickerLED.h"

void Ticker::init(void){
	count = 0;
	PORTA.DIRSET = PIN0_bm;
}

void Ticker::loop(void){
	count ++;
	if(count == 0){
		PORTA.OUTTGL = PIN0_bm;
	}
}
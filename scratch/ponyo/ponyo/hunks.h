/*
 * hunks.h
 *
 * Created: 3/30/2019 1:10:17 PM
 *  Author: Jake
 */ 

#ifndef HUNKS_H_
#define HUNKS_H_

#include <avr/io.h>

class Hunk {
	// internal variables...
	// each has these f'ns
	uint16_t time;
	
	public:
	//Array inputs;
	//Array outputs;
	//Array state;
	void init (void);
	void loop (void);
};

#endif /* HUNKS_H_ */
/*
 * ponyo.cpp
 *
 * Created: 3/30/2019 12:45:33 PM
 * Author : Jake
 */ 

#include <avr/io.h>
#include <string.h>
#include "hunks.h"
#include "TickerLED.h"

// I'm pretty sure system stuff we can just go about normally
void clock_init(void){
	OSC.XOSCCTRL = OSC_XOSCSEL_XTAL_256CLK_gc | OSC_FRQRANGE_12TO16_gc; // select external source
	OSC.CTRL = OSC_XOSCEN_bm; // enable external source
	while(!(OSC.STATUS & OSC_XOSCRDY_bm)); // wait for external
	OSC.PLLCTRL =  OSC_PLLSRC_XOSC_gc | 3; // select external osc for pll, do pll = source * 3
	OSC.CTRL |= OSC_PLLEN_bm; // enable PLL
	while (!(OSC.STATUS & OSC_PLLRDY_bm)); // wait for PLL to be ready
	CCP = CCP_IOREG_gc; // enable protected register change
	CLK.CTRL = CLK_SCLKSEL_PLL_gc; // switch to PLL for main clock
}

Ticker ticker;

int main(void)
{
	clock_init();
	ticker.init();
	
    while (1) 
    {
		// not the one...
		ticker.loop();
    }
}


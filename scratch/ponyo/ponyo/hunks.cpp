/*
 * hunks.cpp
 *
 * Created: 3/30/2019 12:48:12 PM
 *  Author: Jake
 */ 

#include "hunks.h"

void Hunk::init(void){
	time = 0;
	// PIN0 is clk, PIN1 is err 
	PORTA.DIRSET = PIN0_bm | PIN1_bm;
}

void Hunk::loop(void){
	time ++;
	// or do some LED shiiiit
	if(time == 0){
		PORTA.OUTTGL = PIN0_bm;
		time = 0;
	}
}

/*
 * TickerLED.h
 *
 * Created: 4/1/2019 12:43:59 PM
 *  Author: Jake
 */ 


#ifndef TICKERLED_H_
#define TICKERLED_H_

#include <avr/io.h>

class Ticker {
	uint16_t count;
	
	public:
	void init(void);
	void loop(void);
};



#endif /* TICKERLED_H_ */
/* barebones \n delimited terminal tool for hello worlding ponyo */

const SerialPort = require('serialport')
const Delimiter = require('@serialport/parser-delimiter')

const ReadLine = require('readline')

/*
------------------------------------------------------
SERIALPORT
------------------------------------------------------
*/

let theport = null
let comname = ''

function findSerialPort() {
  let found = false
  SerialPort.list(function(err, ports){
    ports.forEach(function(serialport){
      if(serialport.productId === "8022"){
        comname = serialport.comName
        console.log('found 8022, opening')
        openPort()
      }
    })
  })
}

const dtbuf = Buffer.alloc(512, 'a')

function openPort() {
  theport = new SerialPort(comname, {
    baudRate: 9600
  })
  theport.on('open', ()=>{
    theport.on('error', (err)=>{
      console.log('port err', err)
      process.exit()
    })
    const parser = theport.pipe(new Delimiter({delimiter: '\n'}))
    parser.on('data', (data) => {
      console.log('line in:', data.toString())
    })
  })
}

/*
------------------------------------------------------
HUMAN
------------------------------------------------------
*/

const rl = ReadLine.createInterface({
  input: process.stdin,
  output: process.stdout
})

rl.on('line', (line) => {
  let outp = line.concat('\n')
  let buf = Buffer.from(outp)
  if(theport !== null){
    console.log('writing', buf)
    theport.write(buf)
  } else {
    console.log('port is null, not writing')
  }
})

findSerialPort()

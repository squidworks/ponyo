/* barebones COBS delimited terminal tool for hello worlding ponyo */

const SerialPort = require('serialport')
const Delimiter = require('@serialport/parser-delimiter')

const ReadLine = require('readline')

/*
------------------------------------------------------
SERIALPORT
------------------------------------------------------
*/

let theport = null
let comname = ''

function findSerialPort() {
  let found = false
  SerialPort.list(function(err, ports){
    ports.forEach(function(serialport){
      console.log(`port: ${serialport.productId}`)
      if(serialport.productId === "8031"){
        comname = serialport.comName
        console.log('found 8031, opening')
        openPort()
      }
    })
  })
}

const dtbuf = Buffer.alloc(512, 'a')

function openPort() {
  theport = new SerialPort(comname, {
    baudRate: 9600
  })
  theport.on('open', ()=>{
    theport.on('error', (err)=>{
      console.log('port err', err)
      process.exit()
    })
    /*
    theport.on('data', (data)=>{
      console.log("data:", data.toString());
    })
    */
    const parser = theport.pipe(new Delimiter({delimiter: [0]}))
    parser.on('data', (buf) => {
      // lazy buf -> array, and decode
      let arr = new Array()
      for(let i = 0; i < buf.length; i ++){
        arr[i] = buf[i];
      }
      // ... delimiter pulls that zero out, so put it back in for COBS decode
      arr.push(0)
      let decoded = cobsDecode(arr)
      if(decoded[decoded.length-1] === 10){
        //console.log('plucking line ending')
        decoded = decoded.slice(0, decoded.length-1)
      }
      // lldebug -> else
      if(decoded[0] === 252){
        // more type fucherois
        //console.log("the bytes...", decoded)
        // au manuel, for clarity
        let str = ""
        for(let i = 1; i < decoded.length; i ++){
          if(decoded[i] < 32){
            console.log("WARN: char", decoded[i], "is not a char")
          }
          str += String.fromCharCode(decoded[i])
        }
        console.log("LLMSG:\t", str)
      } else {
        console.log("Recv bytes, decoded:\t", decoded)
      }

      /*
      // HERE: grab by 254 flag (rming tailing newline), else draw something else,
      if(buf[0] == 254){
        let str = buf.toString()
        console.log('DBG:\t', str.substring(1, str.length-1))
      } else {
        let str = buf.toString()
        console.log('key:\t', buf[0], 'remaining:\t', str.substring(1))
      }
      */
    })
    //*/
  })
}

/*
------------------------------------------------------
HUMAN
------------------------------------------------------
*/

const rl = ReadLine.createInterface({
  input: process.stdin,
  output: process.stdout
})

rl.on('line', (line) => {
  // TODO NOW cobs this piece
  if(theport !== null){
    let buf = []
    for(let i = 0; i < line.length; i ++){
      buf[i] = line.charCodeAt(i)
    }
    console.log('encoding', buf)
    cobsEncode(buf)
    console.log('encoded, writing', buf)
    theport.write(buf)
  } else {
    console.log('port is null, not writing')
  }
})

function cobsEncode(arr){
  // make leading zeroe
  arr.splice(0,0,0)
  // trailing zero
  arr.push(0)
  // find zeroes in pack
  for (let i = 0; i < arr.length - 1; i ++) {
      if (arr[i] === 0) {
          // have a zero (won't get to the last one, b/c length-1)
          // find the next zero, insert distance to
          for (let j = 1; j < arr.length - i; j ++){
              if(arr[j + i] === 0){
                  arr[i] = j
                  break
              }
          }
      }
  }
}

function cobsDecode(arr){
  //console.log("predec:\t", arr)
  let dcd = new Array();
  let decrem = arr[0];
  let dcp = 0;
  for(let i = 1; i < arr.length; i ++){
    decrem --
    if(decrem === 0){
      if(arr[i] === 0){
        if(i === arr.length - 1){
          //console.log('breaking OK')
        } else {
          console.log('-------------------------> problematic break')
        }
        break
      } else {
        dcd[dcp] = 0
        decrem = arr[i]
      }
    } else {
      dcd[dcp] = arr[i]
    }
    dcp ++
  }
  //console.log("decoded:\t", dcd)
  return dcd
}

findSerialPort()
